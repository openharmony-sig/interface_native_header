/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup CodecBase
 * @{
 *
 * @brief CodecBase模块提供用于音视频封装、解封装、编解码基础功能的变量、属性以及函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 */

/**
 * @file native_avcodec_base.h
 *
 * @brief 声明用于音视频封装、解封装、编解码基础功能的Native API。
 *
 * @library libnative_media_codecbase.so
 * @since 9
 */

#ifndef NATIVE_AVCODEC_BASE_H
#define NATIVE_AVCODEC_BASE_H

#include <stdint.h>
#include <stdio.h>
#include "native_avbuffer.h"
#include "native_avmemory.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 为图形接口定义native层对象。
 * @since 9
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 为音视频编解码接口定义native层对象。
 * @since 9
 */
typedef struct OH_AVCodec OH_AVCodec;

/**
 * @brief 当OH_AVCodec实例运行出错时，会调用来上报具体的错误信息的函数指针。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param errorCode 特定错误代码。
 * @param userData 用户执行回调所依赖的数据。
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnError)(OH_AVCodec *codec, int32_t errorCode, void *userData);

/**
 * @brief 当解码输入码流分辨率或者编码输出码流的分辨率发生变化时，将调用此函数指针报告新的流描述信息。需要注意的是，
 * OH_AVFormat指针的生命周期只有在函数指针被调用时才有效，调用结束后禁止继续访问。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param format 新输出流描述信息。
 * @param userData 用户执行回调所依赖的数据。
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnStreamChanged)(OH_AVCodec *codec, OH_AVFormat *format, void *userData);

/**
 * @brief 当OH_AVCodec在运行过程中需要新的输入数据时，将调用此函数指针，并携带可用的缓冲区来填充新的输入数据。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param index 与新可用的输入缓冲区相对应的索引。
 * @param data 新的可用输入缓冲区。
 * @param userData 用户执行回调所依赖的数据。
 * @deprecated since 11
 * @useinstead OH_AVCodecOnNeedInputBuffer
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnNeedInputData)(OH_AVCodec *codec, uint32_t index, OH_AVMemory *data, void *userData);

/**
 * @brief 当OH_AVCodec运行过程中生成新的输出数据时，将调用此函数指针，并携带包含新输出数据的缓冲区。
 * 需要注意的是，OH_AVCodecBufferAttr指针的生命周期仅在调用函数指针时有效，这将禁止调用结束后继续访问。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param index 与新输出缓冲区对应的索引。
 * @param data 包含新输出数据的缓冲区。
 * @param attr 新输出缓冲区的说明，请参见{@link OH_AVCodecBufferAttr}。
 * @param userData 用户执行回调所依赖的数据。
 * @deprecated since 11
 * @useinstead OH_AVCodecOnNewOutputBuffer
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnNewOutputData)(OH_AVCodec *codec, uint32_t index, OH_AVMemory *data,
                                          OH_AVCodecBufferAttr *attr, void *userData);

/**
 * @brief 当OH_AVCodec在运行过程中需要新的输入数据时，将调用此函数指针，并携带可用的缓冲区来填充新的输入数据。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param index 与新可用的输入缓冲区相对应的索引。
 * @param buffer 新的可用输入缓冲区。
 * @param userData 用户执行回调所依赖的数据。
 * @since 11
 */
typedef void (*OH_AVCodecOnNeedInputBuffer)(OH_AVCodec *codec, uint32_t index, OH_AVBuffer *buffer, void *userData);

/**
 * @brief 当OH_AVCodec运行过程中生成新的输出数据时，将调用此函数指针，并携带包含新输出数据的缓冲区。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec OH_AVCodec实例。
 * @param index 与新输出缓冲区对应的索引。
 * @param buffer 包含新输出数据的缓冲区。
 * @param userData 用户执行回调所依赖的数据。
 * @since 11
 */
typedef void (*OH_AVCodecOnNewOutputBuffer)(OH_AVCodec *codec, uint32_t index, OH_AVBuffer *buffer, void *userData);

/**
 * @brief OH_AVCodec中所有异步回调函数指针的集合。将该结构体的实例注册到OH_AVCodec实例中，
 * 并处理回调上报的信息，以保证OH_AVCodec的正常运行。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param onError 监控编解码器操作错误，请参见{@link OH_AVCodecOnError}。
 * @param onStreamChanged 监控编解码器流变化，请参见{@link OH_AVCodecOnStreamChanged}。
 * @param onNeedInputData 监控编解码器需要输入数据，请参见@link OH_AVCodecOnNeedInputData}。
 * @param onNeedOutputData 监控编解码器已生成输出数据，请参见{@link OH_AVCodecOnNewOutputData}。
 * @deprecated since 11
 * @useinstead OH_AVCodecCallback
 * @since 9
 * @version 1.0
 */
typedef struct OH_AVCodecAsyncCallback {
    OH_AVCodecOnError onError;
    OH_AVCodecOnStreamChanged onStreamChanged;
    OH_AVCodecOnNeedInputData onNeedInputData;
    OH_AVCodecOnNewOutputData onNeedOutputData;
} OH_AVCodecAsyncCallback;

/**
 * @brief OH_AVCodec中所有异步回调函数指针的集合。将该结构体的实例注册到OH_AVCodec实例中，
 * 并处理回调上报的信息，以保证OH_AVCodec的正常运行。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param onError 监控编解码器操作错误，请参见{@link OH_AVCodecOnError}。
 * @param onStreamChanged 监控编解码器流变化，请参见{@link OH_AVCodecOnStreamChanged}。
 * @param onNeedInputBuffer 监控编解码器需要输入数据，请参见{@link OH_AVCodecOnNeedInputBuffer}。
 * @param onNewOutputBuffer 监控编解码器已生成输出数据，请参见{@link OH_AVCodecOnNewOutputBuffer}。
 * @since 11
 */
typedef struct OH_AVCodecCallback {
    OH_AVCodecOnError onError;
    OH_AVCodecOnStreamChanged onStreamChanged;
    OH_AVCodecOnNeedInputBuffer onNeedInputBuffer;
    OH_AVCodecOnNewOutputBuffer onNewOutputBuffer;
} OH_AVCodecCallback;

/**
 * @brief 函数指针定义，用于提供获取用户自定义媒体数据的能力。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param data 要填充的缓冲区。
 * @param length 要读取的数据长度。
 * @param pos 从偏移量位置读取。
 * @return 读取到缓冲区的数据的实际长度。
 * @since 12
*/
typedef int32_t (*OH_AVDataSourceReadAt)(OH_AVBuffer *data, int32_t length, int64_t pos);

/**
 * @brief 用户自定义数据源。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
*/
typedef struct OH_AVDataSource {
    /**
     * @brief 数据源的总大小。
     * @syscap SystemCapability.Multimedia.Media.CodecBase
     * @since 12
     */
    int64_t size;
    /**
     * @brief 数据源的数据回调。
     * @syscap SystemCapability.Multimedia.Media.CodecBase
     * @since 12
     */
    OH_AVDataSourceReadAt readAt;
} OH_AVDataSource;

/**
 * @brief 枚举音频和视频编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
/** AVC(H.264)视频编解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_AVC;
/** AAC音频编解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_AAC;

/**
 * @brief 枚举音频和视频编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
/** FLAC音频编解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_FLAC;
/** VORBIS音频解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_VORBIS;
/** MP3音频解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_MPEG;
/** HEVC(H.265)视频编解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_HEVC;

/**
 * @brief 枚举封装的视频类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @deprecated since 11
 * @since 10
 */
 /** MPEG4视频编码的MIME类型，仅用于封装MPEG4视频码流使用。 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_MPEG4;

/**
 * @brief 视频MPEG4 Part2编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_MPEG4_PART2;

/**
 * @brief 视频MPEG2编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_MPEG2;

/**
 * @brief 枚举封装的图片类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
 /** JPG图片编码的MIME类型，仅用于封装JPG封面时使用。 */
extern const char *OH_AVCODEC_MIMETYPE_IMAGE_JPG;
 /** PNG图片编码的MIME类型，仅用于封装PNG封面时使用。 */
extern const char *OH_AVCODEC_MIMETYPE_IMAGE_PNG;
 /** BMP图片编码的MIME类型，仅用于封装BMP封面时使用。 */
extern const char *OH_AVCODEC_MIMETYPE_IMAGE_BMP;

/**
 * @brief 枚举音频编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 11
 */
 /** Audio Vivid音频解码器的MIME类型。（目前本规格未开放） */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_VIVID;
 /** AMR_NB音频解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_AMR_NB;
 /** AMR_WB音频解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_AMR_WB;
 /** OPUS音频编解码器的MIME类型。（目前本规格未开放） */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_OPUS;
 /** G711MU音频编解码器的MIME类型。 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_G711MU;

/**
 * @brief VVC视频编解码器的MIME类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_VVC;
/**
 * @brief APE音频解码器的MIME类型。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_APE;
/**
 * @brief SRT字幕解封装器的MIME类型。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_AVCODEC_MIMETYPE_SUBTITLE_SRT;
/**
 * @brief WEBVTT字幕解封装器的MIME类型。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_AVCODEC_MIMETYPE_SUBTITLE_WEBVTT;

/**
 * @brief RAW音频码流的MIME类型。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_RAW;

/**
 * @brief 表示surfacebuffer时间戳的键。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @deprecated since 14
 * @since 9
 * @version 1.0
 */
/** 表示surfacebuffer时间戳的键，值类型为int64_t。 */
extern const char *OH_ED_KEY_TIME_STAMP;
/** 表示surfacebuffer流结束符的键，值类型为int32_t。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @deprecated since 14
*/
extern const char *OH_ED_KEY_EOS;

/**
 * @brief 提供统一的键，用于存储媒体描述。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
/** 轨道媒体类型的键，值类型为int32_t，请参见{@link OH_MediaType}。 */
extern const char *OH_MD_KEY_TRACK_TYPE;
/** 编解码器MIME类型的键，值类型为string。 */
extern const char *OH_MD_KEY_CODEC_MIME;
/** 媒体文件持续时间的键，值类型为int64_t。 */
extern const char *OH_MD_KEY_DURATION;
/** 比特率的键，值类型为int64_t。 */
extern const char *OH_MD_KEY_BITRATE;
/** 设置解码输入码流大小最大值的键，值类型为uint32_t。 */
extern const char *OH_MD_KEY_MAX_INPUT_SIZE;
/** 视频宽度的键，值类型为uint32_t。 */
extern const char *OH_MD_KEY_WIDTH;
/** 视频高度键，值类型为uint32_t。 */
extern const char *OH_MD_KEY_HEIGHT;
/** 视频像素格式的键，值类型为int32_t，请参见{@link OH_AVPixelFormat}。 */
extern const char *OH_MD_KEY_PIXEL_FORMAT;
/** 音频原始格式的键，值类型为int32_t，请参见{@link AudioSampleFormat}。 */
extern const char *OH_MD_KEY_AUDIO_SAMPLE_FORMAT;
/** 视频帧率的键，值类型为double。 */
extern const char *OH_MD_KEY_FRAME_RATE;
/** 视频编码码率模式，值类型为int32_t，请参见{@link OH_VideoEncodeBitrateMode}。 */
extern const char *OH_MD_KEY_VIDEO_ENCODE_BITRATE_MODE;
/** 编码档次，值类型为int32_t，请参见{@link OH_AVCProfile, OH_HEVCProfile, OH_AACProfile}。 */
extern const char *OH_MD_KEY_PROFILE;
/** 音频通道计数键，值类型为uint32_t。 */
extern const char *OH_MD_KEY_AUD_CHANNEL_COUNT;
/** 音频采样率键，值类型为int32_t。 */
extern const char *OH_MD_KEY_AUD_SAMPLE_RATE;
/* 关键帧间隔的键，值类型为int32_t，单位为毫秒。负值表示只有第一帧是关键帧，零表示所有帧都是关键帧。 */
extern const char *OH_MD_KEY_I_FRAME_INTERVAL;
/** surface旋转角度的键。值类型为int32_t：应为{0, 90, 180, 270}，默认值为0。 */
extern const char *OH_MD_KEY_ROTATION;

/**
 * @brief 提供统一的键，用于存储媒体描述。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
/** 视频YUV值域标志的键，值类型为int32_t，true表示full range，false表示limited range。 */
extern const char *OH_MD_KEY_RANGE_FLAG;
/** 视频色域的键，值类型为int32_t，请参见{@link OH_ColorPrimary}，遵循H.273标准Table2。 */
extern const char *OH_MD_KEY_COLOR_PRIMARIES;
/** 视频传递函数的键，值类型为int32_t，请参见{@link OH_TransferCharacteristic}，遵循H.273标准Table3。 */
extern const char *OH_MD_KEY_TRANSFER_CHARACTERISTICS;
/** 视频矩阵系数的键，值类型为int32_t，请参见{@link OH_MatrixCoefficient}，遵循H.273标准Table4。 */
extern const char *OH_MD_KEY_MATRIX_COEFFICIENTS;
/** 请求立即编码I帧的键。值类型为int32_t。 */
extern const char *OH_MD_KEY_REQUEST_I_FRAME;
/** 所需编码质量的键。值类型为int32_t，此键仅适用于配置在恒定质量模式下的编码器。 */
extern const char *OH_MD_KEY_QUALITY;
/** 编解码器特定数据的键，视频中表示传递SPS/PPS，音频中表示传递extraData。值类型为uint8_t*。*/
extern const char *OH_MD_KEY_CODEC_CONFIG;
/** 媒体文件标题的键，值类型为string。 */
extern const char *OH_MD_KEY_TITLE;
/** 媒体文件艺术家的键，值类型为string。 */
extern const char *OH_MD_KEY_ARTIST;
/** 专辑的媒体文件的键，值类型为string。 */
extern const char *OH_MD_KEY_ALBUM;
/** 专辑艺术家的键，值类型为string。 */
extern const char *OH_MD_KEY_ALBUM_ARTIST;
/** 媒体文件日期的键，值类型为string。 */
extern const char *OH_MD_KEY_DATE;
/** 媒体文件注释的键，值类型为string。 */
extern const char *OH_MD_KEY_COMMENT;
/** 媒体文件类型的键，值类型为string。 */
extern const char *OH_MD_KEY_GENRE;
/** 媒体文件版权的键，值类型为string。 */
extern const char *OH_MD_KEY_COPYRIGHT;
/** 媒体文件语言的键，值类型为string。 */
extern const char *OH_MD_KEY_LANGUAGE;
/** 媒体文件描述的键，值类型为string。 */
extern const char *OH_MD_KEY_DESCRIPTION;
/** 媒体文件歌词的键，值类型为string。 */
extern const char *OH_MD_KEY_LYRICS;
/** 媒体文件轨道数量的键，值类型为int32_t。 */
extern const char *OH_MD_KEY_TRACK_COUNT;
/** 所需编码通道布局的键。值类型为int64_t，此键仅适用于编码器。 */
extern const char *OH_MD_KEY_CHANNEL_LAYOUT;
/** 每个编码样本位数的键，值类型为int32_t，支持flac编码器，请参见{@link OH_BitsPerSample}。 */
extern const char *OH_MD_KEY_BITS_PER_CODED_SAMPLE;
/** aac格式的键，aac格式分为ADTS格式和LATM格式。值类型为int32_t，0表示LATM格式，1表示ADTS格式。aac解码器支持。*/
extern const char *OH_MD_KEY_AAC_IS_ADTS;
/** aac sbr模式的键，值类型为int32_t，aac编码器支持。 */
extern const char *OH_MD_KEY_SBR;
/** flac兼容性等级的键，值类型为int32_t。仅在音频编码使用。 */
extern const char *OH_MD_KEY_COMPLIANCE_LEVEL;
/** vorbis标识头的键，值类型为uint8_t*，仅vorbis解码器支持。 */
extern const char *OH_MD_KEY_IDENTIFICATION_HEADER;
/** vorbis设置头的键，值类型为uint8_t*，仅vorbis解码器支持。*/
extern const char *OH_MD_KEY_SETUP_HEADER;
/** 视频缩放模式，值类型为int32_t，请参见{@link OH_ScalingMode}。
 * @useinstead OH_NativeWindow_NativeWindowSetScalingModeV2
 * @deprecated since 14
*/
extern const char *OH_MD_KEY_SCALING_MODE;
/** 最大输入缓冲区个数的键，值类型为int32_t。 */
extern const char *OH_MD_MAX_INPUT_BUFFER_COUNT;
/** 最大输出缓冲区个数的键，值类型int32_t。 */
extern const char *OH_MD_MAX_OUTPUT_BUFFER_COUNT;

/**
 * @brief 提供统一的键，用于存储媒体描述。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 11
 */
/** 音频编解码压缩水平的键，值类型为int32_t。 */
extern const char *OH_MD_KEY_AUDIO_COMPRESSION_LEVEL;
/** 媒体文件中的视频轨是否为HDR Vivid的键，值类型为int32_t。 */
extern const char *OH_MD_KEY_VIDEO_IS_HDR_VIVID;
/** 音频对象数目的键. 值类型为int32_t，只有Audio Vivid解码使用。 */
extern const char *OH_MD_KEY_AUDIO_OBJECT_NUMBER;
/** Audio Vivid元数据的键，值类型为uint8_t*，只有Audio Vivid解码使用。 */
extern const char *OH_MD_KEY_AUDIO_VIVID_METADATA;

/**
 * @brief 在视频编码中获取长期参考帧的最大个数的键，值类型为int32_t。
 * 可以通过{@link OH_AVCapability_GetFeatureProperties}接口和枚举值{@link VIDEO_ENCODER_LONG_TERM_REFERENCE}来查询这个最大值。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_FEATURE_PROPERTY_KEY_VIDEO_ENCODER_MAX_LTR_FRAME_COUNT;
/**
 * @brief 使能分层编码的键，值类型为int32_t，1表示使能，0表示其它情况。
 * 使用前可以通过{@link OH_AVCapability_IsFeatureSupported}接口和枚举值{@link VIDEO_ENCODER_TEMPORAL_SCALABILITY}
 * 来查询当前视频编码器是否支持分层编码。
 * 该键是可选的且只用于视频编码，在configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_ENABLE_TEMPORAL_SCALABILITY;
/**
 * @brief 描述图片组基本层图片的间隔大小的键，值类型为int32_t，只在使能分层编码时生效。该键是可选的且只用于视频编码，在configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_TEMPORAL_GOP_SIZE;
/**
 * @brief 描述图片组内参考模式的键，值类型为int32_t，请参见{@link OH_TemporalGopReferenceMode}，只在使能分层编码时生效。
 * 该键是可选的且只用于视频编码，在configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_TEMPORAL_GOP_REFERENCE_MODE;
/**
 * @brief 描述长期参考帧个数的键，值类型为int32_t，必须在支持的值范围内使用。
 * 使用前可以通过{@link OH_AVCapability_GetFeatureProperties}接口和枚举值{@link VIDEO_ENCODER_LONG_TERM_REFERENCE}来查询支持的LTR数目。
 * 该键是可选的且只用于视频编码，在configure阶段使用。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_LTR_FRAME_COUNT;

/**
 * @brief 标记当前帧为长期参考帧的键，值类型为int32_t，1表示被标记，0表示其它情况。
 * 只在长期参考帧个数被配置后生效。
 * 该键是可选的且只用于视频编码输入轮转中，配置后立即生效。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_PER_FRAME_MARK_LTR;

/**
 * @brief 描述当前帧参考的长期参考帧帧号的键，值类型为int32_t。
 * 该键是可选的且只用于视频编码输入轮转中，配置后立即生效。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_PER_FRAME_USE_LTR;

/**
 * @brief 当前OH_AVBuffer中输出的码流对应的帧是否为长期参考帧的键，值类型为int32_t，1表示是LTR，0表示其它情况。
 * 该键是可选的且只用于视频编码输出轮转中。
 * 表示帧的属性。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_PER_FRAME_IS_LTR;

/**
 * @brief 描述帧的POC的键，值类型为int32_t。
 * 该键是可选的且只用于视频编码输出轮转中。
 * 表示帧的属性。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_PER_FRAME_POC;
/**
 * @brief 描述裁剪矩形顶部坐标(y)值的键，值类型为int32_t。 包含裁剪框顶部的行，行索引从0开始。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_CROP_TOP;
/**
 * @brief 描述裁剪矩形底部坐标(y)值的键，值类型为int32_t。 包含裁剪框底部的行，行索引从0开始。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_CROP_BOTTOM;
/**
 * @brief 描述裁剪矩形左坐标(x)值的键，值类型为int32_t。包含裁剪框最左边的列，列索引从0开始。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_CROP_LEFT;
/**
 * @brief 描述裁剪矩形右坐标(x)值的键，值类型为int32_t。包含裁剪框最右边的列，列索引从0开始。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_CROP_RIGHT;
/**
 * @brief 描述视频帧宽跨距的键，值类型为int32_t。宽跨距是像素的索引与正下方像素的索引之间的差。
 * 对于YUV420格式，宽跨距对应于Y平面，U和V平面的跨距可以根据颜色格式计算，但通常未定义，并且取决于设备和版本。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_STRIDE;
/**
 * @brief 描述视频帧高跨距的键，值类型为int32_t。高跨距是指从Y平面顶部到U平面顶部必须偏移的行数。本质上，U平面的偏移量是sliceHeight * stride。
 * U/V平面的高度可以根据颜色格式计算，尽管它通常是未定义的，并且取决于设备和版本。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_SLICE_HEIGHT;
/**
 * @brief 描述视频帧真实宽度的键，值类型为int32_t。
 * 视频解码时调用{@link OH_VideoDecoder_GetOutputDescription}接口，可以从其返回的OH_AVFormat中解析出宽度值。
 * 当解码输出码流变化时，也可从{@link OH_AVCodecOnStreamChanged}返回的OH_AVForamt实例中解析出宽度值。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_PIC_WIDTH;
/**
 * @brief 描述视频帧真实高度的键，值类型为int32_t。
 * 视频解码时调用{@link OH_VideoDecoder_GetOutputDescription}接口，可以从其返回的OH_AVFormat中解析出高度值。
 * 当解码输出码流变化时，也可从{@link OH_AVCodecOnStreamChanged}返回的OH_AVForamt实例中解析出高度值。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_PIC_HEIGHT;
/**
 * @brief 使能低时延视频编解码的键，值类型为int32_t，1表示使能，0表示其它情况。如果使能，则视频编码器或视频解码器持有的输入和输出数据不会超过编解码器标准所要求的数量。
 * 该键是可选的且只用于视频编码，在configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENABLE_LOW_LATENCY;
/**
 * @brief 描述视频编码器允许的最大量化参数的键，值类型为int32_t。
 * 在configure/setparameter阶段使用，或随帧立即生效。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_QP_MAX;
/**
 * @brief 描述视频编码器允许的最小量化参数的键，值类型为int32_t。
 * 在configure/setparameter阶段使用，或随帧立即生效。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_QP_MIN;
/**
 * @brief 描述视频帧平均量化参数的键，值类型为int32_t。
 * 表示当前帧编码块的平均qp值，随OH_AVBuffer输出。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_QP_AVERAGE;
/**
 * @brief 描述视频帧平方误差的键，值类型为double。
 * 表示当前帧编码块的MSE统计值，随OH_AVBuffer输出。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_MSE;
/**
 * @brief AVBuffer中携带的音视频或字幕的sample对应的解码时间戳的键，以微秒为单位，值类型为int64_t。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_DECODING_TIMESTAMP;
/**
 * @brief AVBuffer中携带的音视频或字幕的sample对应的持续时间的键，以微秒为单位，值类型为int64_t。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_BUFFER_DURATION;
/**
 * @brief 样本长宽比的键，值类型为double。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_SAR;
/**
 * @brief 媒体文件中第一帧起始位置开始时间的键，以微秒为单位，值类型为int64_t。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_START_TIME;
/**
 * @brief 轨道开始时间的键，以微秒为单位，值类型为int64_t。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_TRACK_START_TIME;
/**
 * @brief 设置视频解码器输出色彩空间的键，值类型为int32_t。
 * 支持的值为{@link OH_COLORSPACE_BT709_LIMIT}，请参见{@link OH_NativeBuffer_ColorSpace}。
 * 在视频解码调用{@link OH_VideoDecoder_Configure}接口时使用。
 * 如果支持色彩空间转换功能并配置了此键，则视频解码器会自动将HDR Vivid视频转码为色彩空间BT709的SDR视频。
 * 如果不支持色彩空间转换功能，则接口{@link OH_VideoDecoder_Configure}返回错误码{@link AV_ERR_VIDEO_UNSUPPORTED_COLOR_SPACE_CONVERSION}。
 * 如果输入视频不是HDR Vivid视频，则会通过回调函数{@link OH_AVCodecOnError}报告错误{@link AV_ERR_VIDEO_UNSUPPORTED_COLOR_SPACE_CONVERSION}
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
extern const char *OH_MD_KEY_VIDEO_DECODER_OUTPUT_COLOR_SPACE;
/**
 * @brief 解码器是否打开视频可变帧率功能的键，值类型为int32_t。1代表使能视频可变帧率功能，0代表不使能。
 * 该键只用于视频解码Surface模式，在Configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 15
 */
extern const char *OH_MD_KEY_VIDEO_DECODER_OUTPUT_ENABLE_VRR;
/**
 * @brief 媒体文件创建时间的元数据，值类型为字符串。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 14
 */
extern const char *OH_MD_KEY_CREATION_TIME;
/**
 * @brief 如果在上一帧提交给编码器之后没有新的帧可用，则会以毫秒为单位重复提交最后一帧，值类型为int32_t。
 * 该键只用于视频编码Surface模式，在Configure阶段使用。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_REPEAT_PREVIOUS_FRAME_AFTER;
/**
 * @brief 描述编码器在没有新的帧可用的情况下，可以对之前的帧进行重复编码的最大次数，值类型为int32_t。
 * 该键仅在接口{@link OH_MD_KEY_VIDEO_ENCODER_REPEAT_PREVIOUS_FRAME_AFTER}可用时生效，在Configure阶段使用
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
extern const char *OH_MD_KEY_VIDEO_ENCODER_REPEAT_PREVIOUS_MAX_COUNT;

/**
 * @brief 媒体类型。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_MediaType {
    /** 音频轨。*/
    MEDIA_TYPE_AUD = 0,
    /** 视频轨。 */
    MEDIA_TYPE_VID = 1,
    /**
     * 字幕轨。
     * @since 12
     */
    MEDIA_TYPE_SUBTITILE = 2,
} OH_MediaType;

/**
 * @brief AAC档次。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_AACProfile {
    /** AAC编码档次为Low Complexity级别。*/
    AAC_PROFILE_LC = 0,
    /**
     * AAC编码档次为High Efficiency级别。（目前本规格未开放）
     * @since 14
     */
    AAC_PROFILE_HE = 3,
    /**
     * AAC编码档次为High Efficiency v2级别。（目前本规格未开放）
     * @since 14
     */
    AAC_PROFILE_HE_V2 = 4,
} OH_AACProfile;

/**
 * @brief AVC档次。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVCProfile {
    /** AVC编码档次为基本档次。*/
    AVC_PROFILE_BASELINE = 0,
    /** AVC编码档次为高档次。*/
    AVC_PROFILE_HIGH = 4,
    /** AVC编码档次为主档次。*/
    AVC_PROFILE_MAIN = 8,
} OH_AVCProfile;

/**
 * @brief HEVC档次。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_HEVCProfile {
    /** HEVC编码档次为主档次。*/
    HEVC_PROFILE_MAIN = 0,
    /** HEVC编码档次为10bit主档次。*/
    HEVC_PROFILE_MAIN_10 = 1,
    /** HEVC编码档次为静止图像主档次。*/
    HEVC_PROFILE_MAIN_STILL = 2,
    /** HEVC编码档次为HDR10主档次。
     * @deprecated since 14
    */
    HEVC_PROFILE_MAIN_10_HDR10 = 3,
    /** HEVC编码档次为HDR10+主档次。
     * @deprecated since 14
    */
    HEVC_PROFILE_MAIN_10_HDR10_PLUS = 4,
} OH_HEVCProfile;

/**
 * @brief VVC档次。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 14
 */
typedef enum OH_VVCProfile {
    /** VVC编码档次为10bit主档次。 */
    VVC_PROFILE_MAIN_10 = 1,
    /** VVC编码档次为12bit主档次。 */
    VVC_PROFILE_MAIN_12 = 2,
    /** VVC编码档次为12bit帧内主档次。 */
    VVC_PROFILE_MAIN_12_INTRA = 10,
    /** VVC编码档次为多层编码10bit主档次。 */
    VVC_PROFILE_MULTI_MAIN_10 = 17,
    /** VVC编码档次为10bit全采样主档次。 */
    VVC_PROFILE_MAIN_10_444 = 33,
    /** VVC编码档次为12bit全采样主档次。 */
    VVC_PROFILE_MAIN_12_444 = 34,
    /** VVC编码档次为16bit全采样主档次。 */
    VVC_PROFILE_MAIN_16_444 = 36,
    /** VVC编码档次为12bit全采样帧内主档次。 */
    VVC_PROFILE_MAIN_12_444_INTRA = 42,
    /** VVC编码档次为16bit全采样帧内主档次。 */
    VVC_PROFILE_MAIN_16_444_INTRA = 44,
    /** VVC编码档次为多层编码10bit全采样主档次。 */
    VVC_PROFILE_MULTI_MAIN_10_444 = 49,
    /** VVC编码档次为10bit静止图像主档次。 */
    VVC_PROFILE_MAIN_10_STILL = 65,
    /** VVC编码档次为12bit静止图像主档次。 */
    VVC_PROFILE_MAIN_12_STILL = 66,
    /** VVC编码档次为10bit全采样静止图像主档次。 */
    VVC_PROFILE_MAIN_10_444_STILL = 97,
    /** VVC编码档次为12bit全采样静止图像主档次。 */
    VVC_PROFILE_MAIN_12_444_STILL = 98,
    /** VVC编码档次为16bit全采样静止图像主档次。 */
    VVC_PROFILE_MAIN_16_444_STILL = 100,
} OH_VVCProfile;

/**
 * @brief MPEG2档次。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
typedef enum OH_MPEG2Profile {
    /** 简单档次。 */
    MPEG2_PROFILE_SIMPLE = 0,
    /** 主档次。 */
    MPEG2_PROFILE_MAIN  = 1,
    /** 信噪比可分级档次。 */
    MPEG2_PROFILE_SNR  = 2,
    /** 空间可分级档次。 */
    MPEG2_PROFILE_SPATIAL = 3,
    /** 高级档次。 */
    MPEG2_PROFILE_HIGH = 4,
    /** 4:2:2档次。 */
    MPEG2_PROFILE_422 = 5,
} OH_MPEG2Profile;

/**
 * @brief MPEG4档次。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
typedef enum OH_MPEG4Profile {
    /** 简单档次。 */
    MPEG4_PROFILE_SIMPLE = 0,
    /** 简单可分级档次。 */
    MPEG4_PROFILE_SIMPLE_SCALABLE = 1,
    /** 核心档次。 */
    MPEG4_PROFILE_CORE = 2,
    /** 主档次。 */
    MPEG4_PROFILE_MAIN = 3,
    /** N位档次。 */
    MPEG4_PROFILE_NBIT  = 4,
    /** 混合档次。 */
    MPEG4_PROFILE_HYBRID = 5,
    /** 基本动画纹理档次。 */
    MPEG4_PROFILE_BASIC_ANIMATED_TEXTURE = 6,
    /** 可分级纹理档次。 */
    MPEG4_PROFILE_SCALABLE_TEXTURE = 7,
    /** 简单FA档次。 */
    MPEG4_PROFILE_SIMPLE_FA = 8,
    /** 高级实时简单档次。 */
    MPEG4_PROFILE_ADVANCED_REAL_TIME_SIMPLE = 9,
    /** 核心可分级档次。 */
    MPEG4_PROFILE_CORE_SCALABLE = 10,
    /** 高级编码效率档次。 */
    MPEG4_PROFILE_ADVANCED_CODING_EFFICIENCY = 11,
    /** 高级核心档次。 */
    MPEG4_PROFILE_ADVANCED_CORE = 12,
    /** 高级可分级纹理档次。 */
    MPEG4_PROFILE_ADVANCED_SCALABLE_TEXTURE = 13,
    /** 高级简单档次。 */
    MPEG4_PROFILE_ADVANCED_SIMPLE = 17,
} OH_MPEG4Profile;

/**
 * @brief 封装器支持的输出文件格式。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_AVOutputFormat {
    /** 输出文件格式默认值，默认为MP4格式。*/
    AV_OUTPUT_FORMAT_DEFAULT = 0,
    /** 输出文件格式为MP4格式。*/
    AV_OUTPUT_FORMAT_MPEG_4 = 2,
    /** 输出文件格式为M4A格式。*/
    AV_OUTPUT_FORMAT_M4A = 6,
	/**
     * 输出文件格式为AMR格式。
     * @since 12
     */
    AV_OUTPUT_FORMAT_AMR = 8,
	/**
     * 输出文件格式为MP3格式。
     * @since 12
     */
    AV_OUTPUT_FORMAT_MP3 = 9,
    /**
     * 输出文件格式为WAV格式。
     * @since 12
     */
    AV_OUTPUT_FORMAT_WAV = 10,
    /**
     * 输出文件格式为AAC格式。
     * @since 18
     */
    AV_OUTPUT_FORMAT_AAC = 11,
} OH_AVOutputFormat;

/**
 * @brief 跳转模式。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_AVSeekMode {
    /** 指定时间位置的下一关键帧。若时间点后没有I帧，该模式可能跳转失败。 */
    SEEK_MODE_NEXT_SYNC = 0,
    /** 指定时间位置的上一关键帧。 */
    SEEK_MODE_PREVIOUS_SYNC,
    /** 指定时间位置的最近关键帧。 */
    SEEK_MODE_CLOSEST_SYNC,
} OH_AVSeekMode;

/**
 * @brief 缩放模式。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @deprecated since 14
 * @useinstead OHScalingModeV2
 * @since 10
 */
typedef enum OH_ScalingMode {
    /** 根据窗口尺寸自适应调整图像大小。
     * @deprecated since 14
     * @useinstead OH_SCALING_MODE_SCALE_TO_WINDOW_V2
    */
    SCALING_MODE_SCALE_TO_WINDOW = 1,
    /** 根据窗口尺寸裁剪图像大小。
     * @deprecated since 14
     * @useinstead OH_SCALING_MODE_SCALE_CROP_V2
    */
    SCALING_MODE_SCALE_CROP = 2,
} OH_ScalingMode;

/**
 * @brief 每个编码样本的音频位数。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_BitsPerSample {
    /** 8位无符号整数采样。 */
    SAMPLE_U8 = 0,
    /** 16位有符号整数交样。 */
    SAMPLE_S16LE = 1,
    /** 24位有符号整数采样。 */
    SAMPLE_S24LE = 2,
    /** 32位有符号整数采样。 */
    SAMPLE_S32LE = 3,
    /** 32位浮点采样。 */
    SAMPLE_F32LE = 4,
    /** 8位无符号整数平面采样。 */
    SAMPLE_U8P = 5,
    /** 16位有符号整数平面采样。 */
    SAMPLE_S16P = 6,
    /** 24位有符号整数平面采样。 */
    SAMPLE_S24P = 7,
    /** 32位有符号整数平面采样。 */
    SAMPLE_S32P = 8,
    /** 32位浮点平面采样。 */
    SAMPLE_F32P = 9,
    /** 无效采样格式。 */
    INVALID_WIDTH = -1
} OH_BitsPerSample;

/**
 * @brief 色域。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_ColorPrimary {
    /** BT709色域。 */
    COLOR_PRIMARY_BT709 = 1,
    /** 未指定色域。 */
    COLOR_PRIMARY_UNSPECIFIED = 2,
    /** BT470_M色域。 */
    COLOR_PRIMARY_BT470_M = 4,
    /** BT601_625色域。 */
    COLOR_PRIMARY_BT601_625 = 5,
    /** BT601_525色域。 */
    COLOR_PRIMARY_BT601_525 = 6,
    /** SMPTE_ST240色域。 */
    COLOR_PRIMARY_SMPTE_ST240 = 7,
    /** GENERIC_FILM色域。 */
    COLOR_PRIMARY_GENERIC_FILM = 8,
    /** BT2020色域。 */
    COLOR_PRIMARY_BT2020 = 9,
    /** SMPTE_ST428色域。 */
    COLOR_PRIMARY_SMPTE_ST428 = 10,
    /** P3DCI色域。 */
    COLOR_PRIMARY_P3DCI = 11,
    /** P3D65色域。 */
    COLOR_PRIMARY_P3D65 = 12,
} OH_ColorPrimary;

/**
 * @brief 转移特性。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_TransferCharacteristic {
    /** BT709传递函数。 */
    TRANSFER_CHARACTERISTIC_BT709 = 1,
    /** 未指定传递函数。 */
    TRANSFER_CHARACTERISTIC_UNSPECIFIED = 2,
    /** GAMMA_2_2传递函数。 */
    TRANSFER_CHARACTERISTIC_GAMMA_2_2 = 4,
    /** GAMMA_2_8传递函数。 */
    TRANSFER_CHARACTERISTIC_GAMMA_2_8 = 5,
    /** BT601传递函数。 */
    TRANSFER_CHARACTERISTIC_BT601 = 6,
    /** SMPTE_ST240传递函数。 */
    TRANSFER_CHARACTERISTIC_SMPTE_ST240 = 7,
    /** LINEAR传递函数。 */
    TRANSFER_CHARACTERISTIC_LINEAR = 8,
    /** LOG传递函数。 */
    TRANSFER_CHARACTERISTIC_LOG = 9,
    /** LOG_SQRT传递函数。 */
    TRANSFER_CHARACTERISTIC_LOG_SQRT = 10,
    /** IEC_61966_2_4传递函数。 */
    TRANSFER_CHARACTERISTIC_IEC_61966_2_4 = 11,
    /** BT1361传递函数。 */
    TRANSFER_CHARACTERISTIC_BT1361 = 12,
    /** IEC_61966_2_1传递函数。 */
    TRANSFER_CHARACTERISTIC_IEC_61966_2_1 = 13,
    /** BT2020_10BIT传递函数。 */
    TRANSFER_CHARACTERISTIC_BT2020_10BIT = 14,
    /** BT2020_12BIT传递函数。 */
    TRANSFER_CHARACTERISTIC_BT2020_12BIT = 15,
    /** PQ传递函数。 */
    TRANSFER_CHARACTERISTIC_PQ = 16,
    /** SMPTE_ST428传递函数。 */
    TRANSFER_CHARACTERISTIC_SMPTE_ST428 = 17,
    /** HLG传递函数。 */
    TRANSFER_CHARACTERISTIC_HLG = 18,
} OH_TransferCharacteristic;

/**
 * @brief 矩阵系数。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_MatrixCoefficient {
    /** 单位矩阵。 */
    MATRIX_COEFFICIENT_IDENTITY = 0,
    /** BT709转换矩阵。 */
    MATRIX_COEFFICIENT_BT709 = 1,
    /** 未指定转换矩阵。 */
    MATRIX_COEFFICIENT_UNSPECIFIED = 2,
    /** FCC转换矩阵。 */
    MATRIX_COEFFICIENT_FCC = 4,
    /** BT601_625转换矩阵。 */
    MATRIX_COEFFICIENT_BT601_625 = 5,
    /** BT601_525转换矩阵。 */
    MATRIX_COEFFICIENT_BT601_525 = 6,
    /** SMPTE_ST240转换矩阵。 */
    MATRIX_COEFFICIENT_SMPTE_ST240 = 7,
    /** YCGCO转换矩阵。 */
    MATRIX_COEFFICIENT_YCGCO = 8,
    /** BT2020_NCL转换矩阵。 */
    MATRIX_COEFFICIENT_BT2020_NCL = 9,
    /** BT2020_CL转换矩阵。 */
    MATRIX_COEFFICIENT_BT2020_CL = 10,
    /** SMPTE_ST2085转换矩阵。 */
    MATRIX_COEFFICIENT_SMPTE_ST2085 = 11,
    /** CHROMATICITY_NCL转换矩阵。 */
    MATRIX_COEFFICIENT_CHROMATICITY_NCL = 12,
    /** CHROMATICITY_CL转换矩阵。 */
    MATRIX_COEFFICIENT_CHROMATICITY_CL = 13,
    /** ICTCP转换矩阵。 */
    MATRIX_COEFFICIENT_ICTCP = 14,
} OH_MatrixCoefficient;

/**
 * @brief AVC级别。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
typedef enum OH_AVCLevel {
	/** 级别1 */
    AVC_LEVEL_1 = 0,
	/** 级别1b */
    AVC_LEVEL_1b = 1,
	/** 级别1.1 */
    AVC_LEVEL_11 = 2,
	/** 级别1.2 */
    AVC_LEVEL_12 = 3,
	/** 级别1.3 */
    AVC_LEVEL_13 = 4,
	/** 级别2 */
    AVC_LEVEL_2 = 5,
	/** 级别2.1 */
    AVC_LEVEL_21 = 6,
	/** 级别2.2 */
    AVC_LEVEL_22 = 7,
	/** 级别3 */
    AVC_LEVEL_3 = 8,
	/** 级别3.1 */
    AVC_LEVEL_31 = 9,
	/** 级别3.2 */
    AVC_LEVEL_32 = 10,
	/** 级别4 */
    AVC_LEVEL_4 = 11,
	/** 级别4.1 */
    AVC_LEVEL_41 = 12,
	/** 级别4.2 */
    AVC_LEVEL_42 = 13,
	/** 级别5 */
    AVC_LEVEL_5 = 14,
	/** 级别5.1 */
    AVC_LEVEL_51 = 15,
    /** 级别5.2 */
    AVC_LEVEL_52 = 16,
	/** 级别6 */
    AVC_LEVEL_6 = 17,
	/** 级别6.1 */
    AVC_LEVEL_61 = 18,
    /** 级别6.2 */
    AVC_LEVEL_62 = 19,
} OH_AVCLevel;

/**
 * @brief HEVC级别。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
typedef enum OH_HEVCLevel {
	/** 级别1 */
    HEVC_LEVEL_1 = 0,
	/** 级别2 */
    HEVC_LEVEL_2 = 1,
	/** 级别2.1 */
    HEVC_LEVEL_21 = 2,
	/** 级别3 */
    HEVC_LEVEL_3 = 3,
	/** 级别3.1 */
    HEVC_LEVEL_31 = 4,
	/** 级别4 */
    HEVC_LEVEL_4 = 5,
	/** 级别4.1 */
    HEVC_LEVEL_41 = 6,
	/** 级别5 */
    HEVC_LEVEL_5 = 7,
	/** 级别5.1 */
    HEVC_LEVEL_51 = 8,
	/** 级别5.2 */
    HEVC_LEVEL_52 = 9,
	/** 级别6 */
    HEVC_LEVEL_6 = 10,
	/** 级别6.1 */
    HEVC_LEVEL_61 = 11,
	/** 级别6.2 */
    HEVC_LEVEL_62 = 12,
} OH_HEVCLevel;

/**
 * @brief VVC级别。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 14
 */
typedef enum OH_VVCLevel {
	/** 级别1.0 */
    VVC_LEVEL_1 = 16,
    /** 级别2.0 */
    VVC_LEVEL_2 = 32,
    /** 级别2.1 */
    VVC_LEVEL_21 = 35,
    /** 级别3.0 */
    VVC_LEVEL_3 = 48,
    /** 级别3.1 */
    VVC_LEVEL_31 = 51,
    /** 级别4.0 */
    VVC_LEVEL_4 = 64,
    /** 级别4.1 */
    VVC_LEVEL_41 = 67,
    /** 级别5.0 */
    VVC_LEVEL_5 = 80,
    /** 级别5.1 */
    VVC_LEVEL_51 = 83,
    /** 级别5.2 */
    VVC_LEVEL_52 = 86,
    /** 级别6.0 */
    VVC_LEVEL_6 = 96,
    /** 级别6.1 */
    VVC_LEVEL_61 = 99,
    /** 级别6.2 */
    VVC_LEVEL_62 = 102,
    /** 级别6.3 */
    VVC_LEVEL_63 = 105,
    /** 级别15.5 */
    VVC_LEVEL_155 = 255,
} OH_VVCLevel;

/**
 * @brief MPEG2级别。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
typedef enum OH_MPEG2Level {
    /** 低级别。 */
    MPEG2_LEVEL_LL  = 0,
    /** 主级别。 */
    MPEG2_LEVEL_ML  = 1,
    /** 高1440级别。 */
    MPEG2_LEVEL_H14 = 2,
    /** 高级别。 */
    MPEG2_LEVEL_HL  = 3,
}OH_MPEG2Level;

/**
 * @brief MPEG4级别。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 18
 */
typedef enum OH_MPEG4Level {
    /** 级别0 */
    MPEG4_LEVEL_0  = 0,
    /** 级别0B。  */
    MPEG4_LEVEL_0B = 1,
    /** 级别1。  */
    MPEG4_LEVEL_1  = 2,
    /** 级别2。  */
    MPEG4_LEVEL_2  = 3,
    /** 级别3。  */
    MPEG4_LEVEL_3  = 4,
    /** 级别3B。  */
    MPEG4_LEVEL_3B = 5,
    /** 级别4。 */
    MPEG4_LEVEL_4  = 6,
    /** 级别4A。 */
    MPEG4_LEVEL_4A = 7,
    /** 级别5。 */
    MPEG4_LEVEL_5  = 8,
    /** 级别6。 */
    MPEG4_LEVEL_6  = 9,
}OH_MPEG4Level;

/**
 * @brief 时域图片组参考模式。
 *
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 12
 */
typedef enum OH_TemporalGopReferenceMode {
    /** 参考最近的短期参考帧。 */
    ADJACENT_REFERENCE = 0,
    /** 参考最近的长期参考帧。 */
    JUMP_REFERENCE = 1,
    /** 均匀分层参考结构，在丢弃最高层级视频帧后，视频帧均匀分布。其中时域图片组个数必须为2的幂。 */
    UNIFORMLY_SCALED_REFERENCE = 2,
} OH_TemporalGopReferenceMode;

/**
 * @brief 编码器的比特率模式。从API14开始改变头文件的位置。
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 10
 */
typedef enum OH_BitrateMode {
    /* 恒定比特率模式。 */
    BITRATE_MODE_CBR = 0,
    /* 可变比特率模式。 */
    BITRATE_MODE_VBR = 1,
    /* 恒定质量模式。 */
    BITRATE_MODE_CQ = 2
} OH_BitrateMode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_BASE_H
/** @} */
