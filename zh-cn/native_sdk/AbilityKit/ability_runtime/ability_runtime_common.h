/*
* Copyright (C) 2024-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief 提供AbilityRuntime模块的错误码。
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

/**
 * @file ability_runtime_common.h
 *
 * @brief 提供AbilityRuntime模块的错误码。
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

#ifndef ABILITY_RUNTIME_COMMON_H
#define ABILITY_RUNTIME_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief AbilityRuntime模块的错误码的枚举。
 *
 * @since 13
 */
typedef enum {
    /** @error 操作成功。 */
    ABILITY_RUNTIME_ERROR_CODE_NO_ERROR = 0,
    /**
     * @error 权限校验失败。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_PERMISSION_DENIED = 201,
    /** @error 无效参数。 */
    ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID = 401,
    /**
     * @error 设备类型不支持。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_NOT_SUPPORTED = 801,
    /**
     * @error 指定的Ability名称不存在。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_NO_SUCH_ABILITY = 16000001,
    /**
     * @error 接口调用Ability类型错误。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_INCORRECT_ABILITY_TYPE = 16000002,
    /**
     * @error 众测应用到期。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_CROWDTEST_EXPIRED = 16000008,
    /**
     * @error Wukong模式，不允许启动/停止Ability。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_WUKONG_MODE = 16000009,
    /** @error 上下文不存在。 */
    ABILITY_RUNTIME_ERROR_CODE_CONTEXT_NOT_EXIST = 16000011,
    /**
     * @error 应用被管控。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_CONTROLLED = 16000012,
    /**
     * @error 应用被EDM管控。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_EDM_CONTROLLED = 16000013,
    /**
     * @error 限制API 11以上版本三方应用跳转。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_CROSS_APP = 16000018,
    /**
     * @error 内部错误。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_INTERNAL = 16000050,
    /**
     * @error 非顶层应用。
     * @since 15
     */
    ABILITY_RUNTIME_ERROR_CODE_NOT_TOP_ABILITY = 16000053,
    /**
     * @error 不允许设置窗口启动可见性。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_VISIBILITY_SETTING_DISABLED = 16000067,
    /**
     * @error 不支持应用分身和多实例。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_CODE_MULTI_APP_NOT_SUPPORTED = 16000072,
    /**
     * @error 无效多实例。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_CODE_INVALID_APP_INSTANCE_KEY = 16000076,
    /**
     * @error 应用多实例已达到上限。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_CODE_UPPER_LIMIT_REACHED = 16000077,
    /**
     * @error 不支持应用多实例。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_MULTI_INSTANCE_NOT_SUPPORTED = 16000078,
    /**
     * @error 不允许设置APP_INSTANCE_KEY。
     * @since 17
     */
    ABILITY_RUNTIME_ERROR_CODE_APP_INSTANCE_KEY_NOT_SUPPORTED = 16000079,
} AbilityRuntime_ErrorCode;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // ABILITY_RUNTIME_COMMON_H
