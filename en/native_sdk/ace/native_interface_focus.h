/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief Provides focus functionality of ArkUI on the native side, including focus transfer operations.
 *
 * @since 16
 */

/**
 * @file native_interface_focus.h
 *
 * @brief Declares the focus control APIs.
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 16
 */

#ifndef ARKUI_NATIVE_INTERFACE_FOCUS_H
#define ARKUI_NATIVE_INTERFACE_FOCUS_H

#include "napi/native_api.h"
#include "native_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Requests focus for a specific node.
 *
 * @param node Node to request focus for.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_FOCUS_NON_FOCUSABLE} if the node is not focusable.
 *         Returns {@link ARKUI_ERROR_CODE_FOCUS_NON_FOCUSABLE_ANCESTOR} if an ancestor of the node is not focusable.
 *         Returns {@link ARKUI_ERROR_CODE_FOCUS_NON_EXISTENT} if the node does not exist.
 * @since 16
 */
ArkUI_ErrorCode OH_ArkUI_FocusRequest(ArkUI_NodeHandle node);

/**
 * @brief Clears the focus to the root container node.
 *
 * @param uiContext Pointer to a UI instance.
 * @since 16
 */
void OH_ArkUI_FocusClear(ArkUI_ContextHandle uiContext);

/**
 * @brief Sets the focus activation state for the current page. When activated, the focused node displays a focus box.
 *
 * @param uiContext Pointer to a UI instance.
 * @param isActive Whether to enter or exit the focus active state.
 * @param isAutoInactive Whether to automatically exit the focus active state on touch or mouse down events:
 *                       "true": Automatically exit the focus active state.
 *                       "false": Maintain the current state until the corresponding setting API is called.
 * @since 16
 */
void OH_ArkUI_FocusActivate(ArkUI_ContextHandle uiContext, bool isActive, bool isAutoInactive);

/**
 * @brief Configures the focus transfer behavior when pages are switched.
 *
 * @param uiContext Pointer to a UI instance.
 * @param autoTransfer Whether to automatically transfer focus when pages are switched.
 * @since 16
 */
void OH_ArkUI_FocusSetAutoTransfer(ArkUI_ContextHandle uiContext, bool autoTransfer);

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_INTERFACE_FOCUS_H
