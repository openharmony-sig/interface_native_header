/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVSource
 * @{
 *
 * @brief AVSource模块提供用于构造媒体资源对象功能的函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @since 10
 */

/**
 * @file native_avsource.h
 *
 * @brief 声明用于音视频媒体数据解析的接口。
 * 
 * @library libnative_media_avsource.so
 * @since 10
 */

#ifndef NATIVE_AVSOURCE_H
#define NATIVE_AVSOURCE_H

#include <stdint.h>
#include "native_avcodec_base.h"
#include "native_averrors.h"
#include "native_avformat.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 为媒体资源接口定义native层对象。
 * @since 10
 */
typedef struct OH_AVSource OH_AVSource;

/**
 * @brief 为用户自定义数据源的资源对象创建OH_AVSource实例，可以通过调用OH_AVSource_Destroy接口释放实例。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param dataSource 用户自定义数据源。
 * @return 如果执行成功，则返回一个指向OH_AVSource实例的指针，否则返回NULL。\n
 * 可能的故障原因：
 * 1. dataSource为nullptr；
 * 2. dataSource->size == 0；
 * 3. 设置数据源失败；
 * 4. 内存不足；
 * 5. 解码器引擎为nullptr。
 * @since 12
*/
OH_AVSource *OH_AVSource_CreateWithDataSource(OH_AVDataSource *dataSource);

/**
 * @brief 为统一资源标识符对应的的资源对象创建OH_AVSource实例。，可以通过调用OH_AVSource_Destroy接口释放实例。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param uri 远程媒体资源的统一资源标识符。
 * @return 执行成功返回一个指向OH_AVSource实例的指针, 否则返回NULL。\n
 * 可能的故障原因：
 * 1. 网络异常；
 * 2. 资源无效；
 * 3. 文件格式不支持。
 * @since 10
*/
OH_AVSource *OH_AVSource_CreateWithURI(char *uri);

/**
 * @brief 为文件描述符对应的资源对象创建OH_AVSource实例。可以通过调用OH_AVSource_Destroy接口释放实例。
 * Note: 该接口如果传入offset不为文件起始位置，或size不为文件大小时，可能会因数据获取不完整导致
 * OH_AVSource创建失败、后续解封装失败等未定义错误。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param fd 数据资源的文件描述符。
 * @param offset 开始读取数据的位置。
 * @param size 文件的字节数大小。
 * @return 执行成功返回一个指向OH_AVSource实例的指针, 否则返回NULL。\n
 * 可能的故障原因：
 * 1. fd无效；
 * 2. 传入offset不是文件起始位置；
 * 3. size错误；
 * 4. 资源无效；
 * 5. 文件格式不支持。
 * @since 10
*/
OH_AVSource *OH_AVSource_CreateWithFD(int32_t fd, int64_t offset, int64_t size);

/**
 * @brief 销毁OH_AVSource实例并清理内部资源。
 * Note: 同一实例只能被销毁一次。销毁的实例在被重新创建之前不能再被使用。建议实例销毁成功后将指针置为NULL。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param source 指向OH_AVSource实例的指针。
 * @return 返回接口结果：\n
 *         {@link AV_ERR_OK}操作成功。\n
 *         {@link AV_ERR_INVALID_VAL}1. source指针无效，空指针；2. 非OH_AVSource实例。
 * @since 10
*/
OH_AVErrCode OH_AVSource_Destroy(OH_AVSource *source);

/**
 * @brief 获取媒体资源文件的基础信息。
 * 需要注意的是，指向的OH_AVFormat实例在生命周期结束时需调用者通过调用接口OH_AVFormat_Destroy释放。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param source 指向OH_AVSource实例的指针。
 * @return 执行成功返回文件的基础信息, 否则返回NULL。\n
 * 可能的故障原因：
 * 1. source指针无效，空指针；
 * 2. 非OH_AVSource实例；
 * 3. source没有初始化。
 * @since 10
*/
OH_AVFormat *OH_AVSource_GetSourceFormat(OH_AVSource *source);

/**
 * @brief 获取轨道的基础信息。
 * 需要注意的是，指向的OH_AVFormat实例在生命周期结束时需调用者通过调用接口OH_AVFormat_Destroy释放。
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param source 指向OH_AVSource实例的指针。
 * @param trackIndex 需要获取信息的轨道的索引。
 * @return 执行成功返回轨道的基础信息，否则返回NULL。\n
 * 可能的故障原因：
 * 1. source指针无效，空指针或非OH_AVSource实例；
 * 2. 轨道的索引超出范围；
 * 3. source没有初始化。
 * @since 10
*/
OH_AVFormat *OH_AVSource_GetTrackFormat(OH_AVSource *source, uint32_t trackIndex);

/**
 * @brief 获取自定义元数据的基础信息。
 *
 * 需要注意的是，指向的OH_AVFormat实例在生命周期结束时需调用者通过调用接口OH_AVFormat_Destroy释放。
 *
 * @syscap SystemCapability.Multimedia.Media.Spliter
 * @param source 指向OH_AVSource实例的指针。
 * @return 执行成功返回元数据的基础信息，否则返回NULL。\n
 * 可能的故障原因：
 * 1. source指针无效；
 * 2. 空指针或非OH_AVSource实例；
 * 3. source没有初始化。
 * @since 18
*/
OH_AVFormat *OH_AVSource_GetCustomMetadataFormat(OH_AVSource *source);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVSOURCE_H

/** @} */