/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file video_output.h
 *
 * @brief 声明录像输出概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_VIDEOOUTPUT_H
#define NATIVE_INCLUDE_CAMERA_VIDEOOUTPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 录像输出对象
 *
 * 可以使用{@link OH_CameraManager_CreateVideoOutput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_VideoOutput Camera_VideoOutput;

/**
 * @brief 在{@link VideoOutput_Callbacks}中被调用的录像输出帧开始回调。
 *
 * @param videoOutput 传递回调的{@link Camera_VideoOutput}。
 * @since 11
 */
typedef void (*OH_VideoOutput_OnFrameStart)(Camera_VideoOutput* videoOutput);

/**
 * @brief 在{@link VideoOutput_Callbacks}中被调用的录像输出帧结束回调。
 *
 * @param videoOutput 传递回调的{@link Camera_VideoOutput}。
 * @param frameCount 回调传递的帧计数。
 * @since 11
 */
typedef void (*OH_VideoOutput_OnFrameEnd)(Camera_VideoOutput* videoOutput, int32_t frameCount);

/**
 * @brief 在{@link VideoOutput_Callbacks}中被调用的录像输出错误回调。
 *
 * @param videoOutput 传递回调的{@link Camera_VideoOutput}。
 * @param errorCode 录像输出的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_VideoOutput_OnError)(Camera_VideoOutput* videoOutput, Camera_ErrorCode errorCode);

/**
 * @brief 用于录像输出的回调。
 *
 * @see OH_VideoOutput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct VideoOutput_Callbacks {
    /**
     * 录像输出帧启动事件。
     */
    OH_VideoOutput_OnFrameStart onFrameStart;

    /**
     * 录像输出帧结束事件。
     */
    OH_VideoOutput_OnFrameEnd onFrameEnd;

    /**
     * 录像输出错误事件。
     */
    OH_VideoOutput_OnError onError;
} VideoOutput_Callbacks;

/**
 * @brief 注册录像输出更改事件回调。
 *
 * @param videoOutput {@link Camera_VideoOutput}实例。
 * @param callback 要注册的{@link VideoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_VideoOutput_RegisterCallback(Camera_VideoOutput* videoOutput, VideoOutput_Callbacks* callback);

/**
 * @brief 注销录像输出更改事件回调。
 *
 * @param videoOutput {@link Camera_VideoOutput}实例。
 * @param callback 要注销的{@link VideoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_VideoOutput_UnregisterCallback(Camera_VideoOutput* videoOutput, VideoOutput_Callbacks* callback);

/**
 * @brief 开始录像输出。
 *
 * @param videoOutput 要启动的{@link Camera_VideoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_VideoOutput_Start(Camera_VideoOutput* videoOutput);

/**
 * @brief 停止录像输出。
 *
 * @param videoOutput 要停止的{@link Camera_VideoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_VideoOutput_Stop(Camera_VideoOutput* videoOutput);

/**
 * @brief 释放录像输出。
 *
 * @param videoOutput 要释放的{@link Camera_VideoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_VideoOutput_Release(Camera_VideoOutput* videoOutput);

/**
 * @brief 获取当前视频输出配置文件。
 *
 * @param videoOutput 传递当前视频输出配置文件的{@link Camera_VideoOutput}实例。
 * @param profile 如果方法调用成功，则将记录当前的{@link Camera_VideoProfile}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_GetActiveProfile(Camera_VideoOutput* videoOutput, Camera_VideoProfile** profile);

/**
 * @brief 删除视频配置文件实例。
 *
 * @param profile 要删除的{@link Camera_VideoProfile}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_DeleteProfile(Camera_VideoProfile* profile);

/**
 * @brief 获取支持的视频输出帧率列表。
 *
 * @param videoOutput 传递支持的视频输出帧率列表的{@link Camera_VideoOutput}实例。
 * @param frameRateRange 如果方法调用成功，则将记录支持的{@link Camera_FrameRateRange}列表。
 * @param size 如果方法调用成功，则将记录支持的{@link Camera_FrameRateRange}列表大小。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_GetSupportedFrameRates(Camera_VideoOutput* videoOutput,
    Camera_FrameRateRange** frameRateRange, uint32_t* size);

/**
 * @brief 删除帧率列表。
 *
 * @param videoOutput {@link Camera_VideoOutput}实例。
 * @param frameRateRange 要删除的{@link Camera_FrameRateRange}列表。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_DeleteFrameRates(Camera_VideoOutput* videoOutput,
    Camera_FrameRateRange* frameRateRange);

/**
 * @brief 设置视频输出帧率。
 *
 * @param videoOutput 要设置帧率的{@link Camera_VideoOutput}实例。
 * @param minFps 要设置的最小值。
 * @param maxFps 要设置的最大值。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_SetFrameRate(Camera_VideoOutput* videoOutput,
    int32_t minFps, int32_t maxFps);

/**
 * @brief 获取当前视频输出帧率。
 *
 * @param videoOutput 传递当前视频输出帧率的{@link Camera_VideoOutput}实例。
 * @param frameRateRange 如果方法调用成功，则将记录当前的{@link Camera_FrameRateRange}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_VideoOutput_GetActiveFrameRate(Camera_VideoOutput* videoOutput,
    Camera_FrameRateRange* frameRateRange);

/**
 * @brief 判断当前视频输出是否支持镜像。
 *
 * @param videoOutput 传递当前视频输出的{@link Camera_VideoOutput}实例。
 * @param isSupported 当前视频输出是否支持镜像。
 * @return {@link #CAMERA_OK}如果方法调用成功。
 *         {@link #CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 15
 */
Camera_ErrorCode OH_VideoOutput_IsMirrorSupported(Camera_VideoOutput* videoOutput, bool* isSupported);

/**
 * @brief 打开/关闭当前视频输出镜像功能。
 *
 * @param videoOutput 传递当前视频输出的{@link Camera_VideoOutput}实例。
 * @param mirrorMode TRUE表示打开镜像功能, FALSE表示关闭镜像功能。
 * @return {@link #CAMERA_OK}如果方法调用成功。
 *         {@link #CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 15
 */
Camera_ErrorCode OH_VideoOutput_EnableMirror(Camera_VideoOutput* videoOutput, bool mirrorMode);

/**
 * @brief 获取当前视频输出应当设置的旋转角度。
 *
 * @param videoOutput 传递当前视频输出的{@link Camera_VideoOutput}实例。
 * @param deviceDegree 设备目前相对于自然方向（充电口朝下）顺时针的旋转角度。
 * @param imageRotation 当前视频输出应当设置的旋转角度。
 * @return {@link #CAMERA_OK}如果方法调用成功。
 *         {@link #CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode  OH_VideoOutput_GetVideoRotation(Camera_VideoOutput* videoOutput, int deviceDegree,
    Camera_ImageRotation* imageRotation);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_VIDEOOUTPUT_H
/** @} */