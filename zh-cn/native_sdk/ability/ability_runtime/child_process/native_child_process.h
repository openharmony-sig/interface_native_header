/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_ABILITY_RUNTIME_C_NATIVE_CHILD_PROCESS_H
#define OHOS_ABILITY_RUNTIME_C_NATIVE_CHILD_PROCESS_H

#include "IPCKit/ipc_cparcel.h"

/**
 * @addtogroup ChildProcess
 * @{
 *
 * @brief 提供子进程的管理能力。
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 12
 */

/**
 * @file native_child_process.h
 *
 * @brief 支持创建Native子进程，并在父子进程间建立IPC通道。
 *
 * @library libchild_process.so
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 12
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义Native子进程模块错误码。
 * @since 12
 */
typedef enum Ability_NativeChildProcess_ErrCode {
    /**
     * @error 操作成功。
     */
    NCP_NO_ERROR = 0,

    /**
     * @error 无效参数。
     */
    NCP_ERR_INVALID_PARAM = 401,

    /**
     * @error 不支持创建Native子进程。
     */
    NCP_ERR_NOT_SUPPORTED = 801,

    /**
     * @error 内部错误。
     */
    NCP_ERR_INTERNAL = 16000050,

    /**
     * @error 在Native子进程的启动过程中不能再次创建新的子进程，可以等待当前子进程启动完成后再次尝试。
     */
    NCP_ERR_BUSY = 16010001,

    /**
     * @error 启动Native子进程超时。
     */
    NCP_ERR_TIMEOUT = 16010002,

    /**
     * @error 服务端出错。
     */
    NCP_ERR_SERVICE_ERROR = 16010003,

    /**
     * @error 多进程模式已关闭，不允许启动子进程。
     */
    NCP_ERR_MULTI_PROCESS_DISABLED = 16010004,

    /**
     * @error 不允许在子进程中再次创建进程。
     */
    NCP_ERR_ALREADY_IN_CHILD = 16010005,

    /**
     * @error 到达最大Native子进程数量限制，不能再创建子进程。
     */
    NCP_ERR_MAX_CHILD_PROCESSES_REACHED = 16010006,

    /**
     * @error 子进程加载动态库失败，文件不存在或者未实现对应的方法并导出。
     */
    NCP_ERR_LIB_LOADING_FAILED = 16010007,

    /**
     * @error 子进程调用动态库的OnConnect方法失败，可能返回了无效的IPC对象指针。
     */
    NCP_ERR_CONNECTION_FAILED = 16010008,
} Ability_NativeChildProcess_ErrCode;


/**
 * @brief 定义通知子进程启动结果的回调函数。
 *
 * @param errCode {@link NCP_NO_ERROR} - 创建子进程成功\n
 * {@link NCP_ERR_LIB_LOADING_FAILED} - 加载动态库文件失败或动态库中未实现必要的导出函数\n
 * {@link NCP_ERR_CONNECTION_FAILED} - 动态库中实现的OnConnect方法未返回有效的IPC Stub指针\n
 * 详见{@link Ability_NativeChildProcess_ErrCode}定义。
 * @param remoteProxy 子进程的IPC对象指针，出现异常时可能为nullptr；
 * 使用完毕后需要调用{@link OH_IPCRemoteProxy_Destory}方法释放。
 * @see OH_Ability_CreateNativeChildProcess
 * @see OH_IPCRemoteProxy_Destory
 * @since 12
 */
typedef void (*OH_Ability_OnNativeChildProcessStarted)(int errCode, OHIPCRemoteProxy *remoteProxy);

/**
 * @brief 创建子进程并加载参数中指定的动态链接库文件，进程启动结果通过回调参数异步通知，
 * 需注意回调通知为独立线程，回调函数实现需要注意线程同步，且不能执行高耗时操作避免长时间阻塞。
 *
 * 参数所指定的动态库必须实现并导出下列函数：\n
 *   1. OHIPCRemoteStub* NativeChildProcess_OnConnect()\n
 *   2. void NativeChildProcess_MainProc()\n
 *
 * 处理逻辑顺序如下列伪代码所示：\n
 *   主进程：\n
 *     1. OH_Ability_CreateNativeChildProcess(libName, onProcessStartedCallback)\n
 *   子进程：\n
 *     2. dlopen(libName)\n
 *     3. dlsym("NativeChildProcess_OnConnect")\n
 *     4. dlsym("NativeChildProcess_MainProc")\n
 *     5. ipcRemote = NativeChildProcess_OnConnect()\n
 *     6. NativeChildProcess_MainProc()\n
 *   主进程：\n
 *     7. onProcessStartedCallback(ipcRemote, errCode)\n
 *   子进程：\n
 *     8. 在NativeChildProcess_MainProc()函数返回后子进程退出。\n
 *
 * @param libName 子进程中加载的动态库文件名称，不能为nullptr。
 * @param onProcessStarted 通知子进程启动结果的回调函数指针，不能为nullptr，
 * 详见{@link OH_Ability_OnNativeChildProcessStarted}。
 * @return {@link NCP_NO_ERROR} - 调用成功，但子进程的实际启动结果由回调函数通知\n
 * {@link NCP_ERR_INVALID_PARAM} - 无效的动态库名称或者回调函数指针\n
 * {@link NCP_ERR_NOT_SUPPORTED} - 当前设备不支持创建Native子进程\n
 * {@link NCP_ERR_MULTI_PROCESS_DISABLED} - 当前设备已关闭多进程模式\n
 * {@link NCP_ERR_ALREADY_IN_CHILD} - 不允许在子进程中再次创建子进程\n
 * {@link NCP_ERR_MAX_CHILD_PROCESSES_REACHED} - 到达最大Native子进程数限制\n
 * 详见{@link Ability_NativeChildProcess_ErrCode}定义。
 * @see OH_Ability_OnNativeChildProcessStarted
 * @since 12
 */
int OH_Ability_CreateNativeChildProcess(const char* libName,
                                        OH_Ability_OnNativeChildProcessStarted onProcessStarted);


/**
 * @brief 子进程获取自身的启动参数。
 *
 * @return 返回指向当前子进程启动参数的指针。\n
 * 详见{@link NativeChildProcess_Args}定义。
 * @since 16
 */
NativeChildProcess_Args* OH_Ability_GetCurrentChildProcessArgs();
#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // OHOS_ABILITY_RUNTIME_C_NATIVE_CHILD_PROCESS_H
