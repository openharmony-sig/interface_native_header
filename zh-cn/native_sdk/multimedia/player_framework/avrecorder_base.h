/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup AVRecorder
 * @{
 *
 * @brief 提供请求录制能力的 API。
 *
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 * @}
 */
 /**
 * @file avrecorder_base.h
 *
 * @brief 定义了媒体 AVRecorder 的结构体和枚举。
 *
 * @kit MediaKit
 * @library libavrecorder.so
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @include <multimedia/player_framework/avrecorder_base.h>
 * @since 16
 */
 
#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVRECORDER_BASE_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVRECORDER_BASE_H

#include <string>
#include <stdint.h>
#include "multimedia/media_library/media_asset_base_capi.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 初始化AVRecorder。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder OH_AVRecorder;

/**
 * @brief AVRecorder的音频源类型。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_AudioSourceType {
    /** 默认音频源类型。 */
    AVRECORDER_DEFAULT = 0,
    /** 麦克风音频源类型。 */
    AVRECORDER_MIC = 1,
    /** 表示语音识别场景的音频源。 */
    AVRECORDER_VOICE_RECOGNITION = 2,
    /** 表示语音通话场景的音频源。 */
    AVRECORDER_VOICE_COMMUNICATION = 7,
    /** 表示短语音消息的音频源。 */
    AVRECORDER_VOICE_MESSAGE = 10,
    /** 表示相机录像的音频源。 */
    AVRECORDER_CAMCORDER = 13,
} OH_AVRecorder_AudioSourceType;

/**
 * @brief AVRecorder的视频源类型。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_VideoSourceType {
    /** 原始数据Surface。 */
    AVRECORDER_SURFACE_YUV = 0,
    /** ES数据Surface。 */
    AVRECORDER_SURFACE_ES = 1,
} OH_AVRecorder_VideoSourceType;

/**
 * @brief 枚举编码器 MIME 类型。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_CodecMimeType {
    /** H.264 编码器 MIME 类型。 */
    AVRECORDER_VIDEO_AVC = 2,
    /** AAC 编码器 MIME 类型。 */
    AVRECORDER_AUDIO_AAC = 3,
    /** mp3 编码器 MIME 类型。 */
    AVRECORDER_AUDIO_MP3 = 4,
    /** G711-mulaw 编码器 MIME 类型。 */
    AVRECORDER_AUDIO_G711MU = 5,
    /** MPEG4 编码器 MIME 类型。 */
    AVRECORDER_VIDEO_MPEG4 = 6,
    /** H.265 编码器 MIME 类型。 */
    AVRECORDER_VIDEO_HEVC = 8,
    /** AMR_NB 编解码器 MIME 类型。 */
    AVRECORDER_AUDIO_AMR_NB = 9,
    /** AMR_WB 编解码器 MIME 类型。 */
AVRECORDER_AUDIO_AMR_WB = 10,
} OH_AVRecorder_CodecMimeType;

/**
 * @brief 枚举容器格式类型（容器格式类型的缩写是 CFT）。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_ContainerFormatType {
    /** 视频容器格式类型 mp4。 */
    AVRECORDER_CFT_MPEG_4 = 2,
    /** 音频容器格式类型 m4a。 */
    AVRECORDER_CFT_MPEG_4A = 6,
    /** 音频容器格式类型 amr。 */
    AVRECORDER_CFT_AMR = 8,
    /** 音频容器格式类型 mp3。 */
    AVRECORDER_CFT_MP3 = 9,
    /** 音频容器格式类型 wav。 */
    AVRECORDER_CFT_WAV = 10,
} OH_AVRecorder_ContainerFormatType;

/**
 * @brief AVRecorder状态。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_State {
    /** 空闲状态。此时可以调用OH_AVRecorder_Prepare()方法设置录制参数，进入AVRECORDER_PREPARED状态。 */
    AVRECORDER_IDLE = 0,
    /** 准备状态。参数设置完成，此时可以调用OH_AVRecorder_Start()方法开始录制，进入AVRECORDER_STARTED状态。 */
    AVRECORDER_PREPARED = 1,
    /** 启动状态。正在录制，此时可以调用OH_AVRecorder_Pause()方法暂停录制，进入AVRECORDER_PAUSED状态。
    也可以调用OH_AVRecorder_Stop()方法结束录制，进入AVRECORDER_STOPPED状态。 */
    AVRECORDER_STARTED = 2,
    /** 暂停状态。此时可以调用OH_AVRecorder_Resume()方法继续录制，进入AVRECORDER_STARTED状态。
    也可以调用OH_AVRecorder_Stop()方法结束录制，进入AVRECORDER_STOPPED状态。 */
    AVRECORDER_PAUSED = 3,
    /** 停止状态。此时可以调用OH_AVRecorder_Prepare()方法设置录制参数，重新进入AVRECORDER_PREPARED状态。 */
    AVRECORDER_STOPPED = 4,
    /** 释放状态。录制资源释放，此时不能再进行任何操作。在任何其他状态下，均可以通过调用OH_AVRecorder_Release()方法进入AVRECORDER_RELEASED状态。 */
    AVRECORDER_RELEASED = 5,
    /** 错误状态。当AVRecorder实例发生不可逆错误，会转换至当前状态。切换至AVRECORDER_ERROR状态时会伴随OH_AVRecorder_OnError事件，该事件会上报详细错误原因。
    在AVRECORDER_ERROR状态时，用户需要调用OH_AVRecorder_Reset()方法重置AVRecorder实例，或者调用OH_AVRecorder_Release()方法释放资源。 */
    AVRECORDER_ERROR = 6,
} OH_AVRecorder_State;

/**
 * @brief AVRecorder状态变化的原因。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_StateChangeReason {
    /** 用户操作导致的状态变化。 */
    AVRECORDER_USER = 0,
    /** 后台操作导致的状态变化。 */
    AVRECORDER_BACKGROUND = 1,
} OH_AVRecorder_StateChangeReason;

/**
 * @brief 创建录制文件的模式。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef enum OH_AVRecorder_FileGenerationMode {
    /** 由应用自行在沙箱创建媒体文件。 */
    AVRECORDER_APP_CREATE = 0,
    /** 由系统创建媒体文件，当前仅在相机录制场景下生效，会忽略应用设置的url。 */
    AVRECORDER_AUTO_CREATE_CAMERA_SCENE = 1,
} OH_AVRecorder_FileGenerationMode;

/**
 * @brief 定义音视频录制的详细参数。
 * 可以通过参数设置选择只录制音频或只录制视频：
 * 1. 当 audioBitrate 或 audioChannels 为 0 时，不录制音频；
 * 2. 当 videoFrameWidth 或 videoFrameHeight 为 0 时，不录制视频。
 *
 * 各参数的范围请参见AVRecorderProfile。
 *
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_Profile {
    /** 音频比特率。 */
    int32_t audioBitrate;
    /** 音频通道数。 */
    int32_t audioChannels;
    /** 音频编码格式。 */
    OH_AVRecorder_CodecMimeType audioCodec;
    /** 音频采样率。 */
    int32_t audioSampleRate;
    /** 输出文件格式。 */
    OH_AVRecorder_ContainerFormatType fileFormat;
    /** 视频比特率。 */
    int32_t videoBitrate;
    /** 视频编码格式。 */
    OH_AVRecorder_CodecMimeType videoCodec;
    /** 视频宽度。 */
    int32_t videoFrameWidth;
    /** 视频高度。 */
    int32_t videoFrameHeight;
    /** 视频帧率。 */
    int32_t videoFrameRate;
    /** 是否录制 HDR 视频。 */
    bool isHdr;
    /** 是否启用时间缩放编码模式。 */
    bool enableTemporalScale;
} OH_AVRecorder_Profile;

/**
 * @brief 提供媒体资源的地理位置信息。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_Location {
    /** 地理位置的纬度。 */
    float latitude;
    /** 地理位置的经度。 */
    float longitude;
} OH_AVRecorder_Location;

/**
 * @brief 定义元数据的基本模板。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_MetadataTemplate {
    /** 元数据的键值。 */
    char *key;
    /** 元数据的内容。 */
    char *value;
} OH_AVRecorder_MetadataTemplate;

/**
 * @brief 设置元数据信息。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_Metadata {
    /** 媒体资源的类型或体裁。 */
    char *genre;
    /** 视频的旋转方向，单位为度。 */
    char *videoOrientation;
    /** 视频的地理位置信息。 */
    OH_AVRecorder_Location location;
    /** 从 moov.meta.list 读取的自定义参数键值映射。 */
    OH_AVRecorder_MetadataTemplate customInfo;
} OH_AVRecorder_Metadata;

/**
 * @brief 提供媒体AVRecorder的配置定义。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_Config {
    /** 录制音频源类型。 */
    OH_AVRecorder_AudioSourceType audioSourceType;
    /** 录制视频源类型。 */
    OH_AVRecorder_VideoSourceType videoSourceType;
    /** 包含音频和视频编码配置。 */
    OH_AVRecorder_Profile profile;
    /** 定义文件 URL，格式为fd://xx。 */
    char *url;
    /** 指定录制输出文件的生成模式。 */
    OH_AVRecorder_FileGenerationMode fileGenerationMode;
    /** 包含录制媒体的附加元数据。 */
    OH_AVRecorder_Metadata metadata;
} OH_AVRecorder_Config;

/**
 * @brief 表示一个类型的范围。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_Range {
    /** 范围的最小值。 */
    int32_t min;
    /** 范围的最大值。 */
    int32_t max;
} OH_AVRecorder_Range;

/**
 * @brief 提供编码器信息。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @since 16
 */
typedef struct OH_AVRecorder_EncoderInfo {
    /** 编码器MIME类型名称。 */
    OH_AVRecorder_CodecMimeType mimeType;
    /** 编码器类型，audio表示音频编码器，video表示视频编码器。 */
    char *type;
    /** 比特率，包含该编码器的最大和最小值。 */
    OH_AVRecorder_Range bitRate;
    /** 视频帧率，包含帧率的最大和最小值，仅视频编码器拥有。 */
    OH_AVRecorder_Range frameRate;
    /** 视频帧的宽度，包含宽度的最大和最小值，仅视频编码器拥有。 */
    OH_AVRecorder_Range width;
    /** 视频帧的高度，包含高度的最大和最小值，仅视频编码器拥有。 */
    OH_AVRecorder_Range height;
    /** 音频采集声道数，包含声道数的最大和最小值，仅音频编码器拥有。 */
    OH_AVRecorder_Range channels;
    /** 音频采样率列表，包含所有可以使用的音频采样率值，仅音频编码器拥有。 */
    int32_t *sampleRate;
    /** 音频采样率列表长度 */
    int32_t sampleRateLen;
} OH_AVRecorder_EncoderInfo;

/**
 * @brief 当录制状态发生变化时调用。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @param recorder OH_AVRecorder 实例的指针。
 * @param state 表示录制器状态。详情请参见 {@link OH_AVRecorder_State}。
 * @param reason 录制器状态变化的原因。详情请参见 {@link OH_AVRecorder_StateChangeReason}。
 * @param userData 用户特定数据的指针。
 * @since 16
 */
typedef void (*OH_AVRecorder_OnStateChange)(OH_AVRecorder *recorder,
    OH_AVRecorder_State state, OH_AVRecorder_StateChangeReason reason, void *userData);

/**
 * @brief 当录制过程中发生错误时调用。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @param recorder OH_AVRecorder 实例的指针。
 * @param errorCode 错误码。
 * @param errorMsg 错误信息。
 * @param userData 用户特定数据的指针。
 * @since 16
 */
typedef void (*OH_AVRecorder_OnError)(OH_AVRecorder *recorder, int32_t errorCode, const char *errorMsg,
    void *userData);

/**
 * @brief 当录制在 OH_AVRecorder_FileGenerationMode.AVRECORDER_AUTO_CREATE_CAMERA_SCENE 模式下时调用。
 * @syscap SystemCapability.Multimedia.Media.AVRecorder
 * @param recorder OH_AVRecorder 实例的指针。
 * @param asset OH_MediaAsset 实例的指针。
 * @param userData 用户特定数据的指针。
 * @since 16
 */
typedef void (*OH_AVRecorder_OnUri)(OH_AVRecorder *recorder, OH_MediaAsset *asset, void *userData);

#ifdef __cplusplus
}
#endif

#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVRECORDER_BASE_H
