/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief 提供应用启动参数数据结构AbilityRuntime_StartOptions以及设置和获取相关函数。
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 17
 */

/**
 * @file start_options.h
 *
 * @brief 提供应用启动参数数据结构AbilityRuntime_StartOptions以及设置和获取相关函数。
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 17
 */

#ifndef ABILITY_RUNTIME_START_OPTIONS_H
#define ABILITY_RUNTIME_START_OPTIONS_H

#include <stdint.h>
#include <stddef.h>
#include "ability_runtime_common.h"
#include "context_constant.h"
#include "multimedia/image_framework/image/pixelmap_native.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AbilityRuntime_StartOptions;
typedef struct AbilityRuntime_StartOptions AbilityRuntime_StartOptions;

/**
 * @brief 创建AbilityRuntime_StartOptions对象。
 *
 * @return 返回指针类型AbilityRuntime_StartOptions对象。
 *
 * @since 17
 */
AbilityRuntime_StartOptions* OH_AbilityRuntime_CreateStartOptions(void);

/**
 * @brief 销毁AbilityRuntime_StartOptions对象。
 *
 * @param startOptions 需要销毁的AbilityRuntime_StartOptions对象。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_DestroyStartOptions(AbilityRuntime_StartOptions **startOptions);

/**
 * @brief 设置启动Ability时的窗口模式。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowMode 启动Ability时的窗口模式。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者windowMode无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWindowMode(AbilityRuntime_StartOptions *startOptions,
    AbilityRuntime_WindowMode windowMode);

/**
 * @brief 获取启动Ability时的窗口模式。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowMode 启动Ability时的窗口模式。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWindowMode(AbilityRuntime_StartOptions *startOptions,
    AbilityRuntime_WindowMode &windowMode);

/**
 * @brief 设置启动Ability时窗口所在的屏幕ID。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param displayId 启动Ability时窗口所在的屏幕ID。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsDisplayId(AbilityRuntime_StartOptions *startOptions,
    int32_t displayId);

/**
 * @brief 获取启动Ability时窗口所在的屏幕ID。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param displayId 启动Ability时窗口所在的屏幕ID。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsDisplayId(AbilityRuntime_StartOptions *startOptions,
    int32_t &displayId);

/**
 * @brief 设置启动Ability时是否具有动画效果。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param withAnimation 启动Ability时是否具有动画效果。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWithAnimation(AbilityRuntime_StartOptions *startOptions,
    bool withAnimation);

/**
 * @brief 获取启动Ability时是否具有动画效果。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param withAnimation 启动Ability时是否具有动画效果。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWithAnimation(AbilityRuntime_StartOptions *startOptions,
    bool &withAnimation);

/**
 * @brief 设置启动Ability时的窗口左侧位置，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowLeft 启动Ability时的窗口左侧位置，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWindowLeft(AbilityRuntime_StartOptions *startOptions,
    int32_t windowLeft);

/**
 * @brief 获取启动Ability时的窗口左侧位置，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowLeft 启动Ability时的窗口左侧位置，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWindowLeft(AbilityRuntime_StartOptions *startOptions,
    int32_t &windowLeft);

/**
 * @brief 设置启动Ability时的窗口顶部位置，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowTop 启动Ability时的窗口顶部位置，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWindowTop(AbilityRuntime_StartOptions *startOptions,
    int32_t windowTop);

/**
 * @brief 获取启动Ability时的窗口顶部位置，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowTop 启动Ability时的窗口顶部位置，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWindowTop(AbilityRuntime_StartOptions *startOptions,
    int32_t &windowTop);

/**
 * @brief 设置启动Ability时的窗口高度，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowHeight 启动Ability时的窗口高度，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWindowHeight(AbilityRuntime_StartOptions *startOptions,
    int32_t windowHeight);

/**
 * @brief 获取启动Ability时的窗口高度，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowHeight 启动Ability时的窗口高度，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWindowHeight(AbilityRuntime_StartOptions *startOptions,
    int32_t &windowHeight);

/**
 * @brief 设置启动Ability时的窗口宽度，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowWidth 启动Ability时的窗口宽度，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsWindowWidth(AbilityRuntime_StartOptions *startOptions,
    int32_t windowWidth);

/**
 * @brief 获取启动Ability时的窗口宽度，单位为px。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param windowWidth 启动Ability时的窗口宽度，单位为px。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsWindowWidth(AbilityRuntime_StartOptions *startOptions,
    int32_t &windowWidth);

/**
 * @brief 设置启动Ability时的窗口启动图标。图片数据大小限制为600M。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param startWindowIcon 启动Ability时的窗口启动图标。图片数据大小限制为600M。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者startWindowIcon无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsStartWindowIcon(AbilityRuntime_StartOptions *startOptions,
    OH_PixelmapNative *startWindowIcon);

/**
 * @brief 获取启动Ability时的窗口启动图标。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param startWindowIcon 启动Ability时的窗口启动图标。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者startWindowIcon无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsStartWindowIcon(AbilityRuntime_StartOptions *startOptions,
    OH_PixelmapNative **startWindowIcon);

/**
 * @brief 设置启动Ability时的窗口背景颜色。如果未设置该字段，则默认采用module.json5配置文件中abilities标签的startWindowBackground字段的配置。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param startWindowBackgroundColor 启动Ability时的窗口背景颜色。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者startWindowBackgroundColor无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsStartWindowBackgroundColor(
    AbilityRuntime_StartOptions *startOptions, const char *startWindowBackgroundColor);

/**
 * @brief 获取启动Ability时的窗口背景颜色。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param startWindowBackgroundColor 启动Ability时的窗口背景颜色。
 * @param size The size of start window background color.
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者startWindowBackgroundColor无效。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_INTERNAL} 时，表示内部错误。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsStartWindowBackgroundColor(
    AbilityRuntime_StartOptions *startOptions, char **startWindowBackgroundColor, size_t &size);

/**
 * @brief 设置启动Ability时的组件所支持的窗口模式。如果未配置该字段，则默认采用该UIAbility对应的module.json5配置文件中abilities标签的supportWindowMode字段取值。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param supportedWindowModes 启动Ability时的组件所支持的窗口模式。
 * @param size 组件所支持的窗口模式大小。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者supportedWindowModes或者size无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsSupportedWindowModes(
    AbilityRuntime_StartOptions *startOptions, AbilityRuntime_SupportedWindowMode *supportedWindowModes,
    size_t size);

/**
 * @brief 获取启动Ability时的组件所支持的窗口模式。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param supportedWindowModes 启动Ability时的组件所支持的窗口模式。
 * @param size 组件所支持的窗口模式大小。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions或者supportedWindowModes无效。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_INTERNAL} 时，表示内部错误。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsSupportedWindowModes(
    AbilityRuntime_StartOptions *startOptions, AbilityRuntime_SupportedWindowMode **supportedWindowModes,
    size_t &size);

/**
 * @brief 设置启动Ability时的窗口最小宽度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param minWindowWidth 启动Ability时的窗口最小宽度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsMinWindowWidth(
    AbilityRuntime_StartOptions *startOptions, int32_t minWindowWidth);

/**
 * @brief 获取启动Ability时的窗口最小宽度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param minWindowWidth 启动Ability时的窗口最小宽度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsMinWindowWidth(
    AbilityRuntime_StartOptions *startOptions, int32_t &minWindowWidth);

/**
 * @brief 设置启动Ability时的窗口最大宽度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param maxWindowWidth 启动Ability时的窗口最大宽度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsMaxWindowWidth(
    AbilityRuntime_StartOptions *startOptions, int32_t maxWindowWidth);

/**
 * @brief 获取启动Ability时的窗口最大宽度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param maxWindowWidth 启动Ability时的窗口最大宽度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsMaxWindowWidth(
    AbilityRuntime_StartOptions *startOptions, int32_t &maxWindowWidth);

/**
 * @brief 设置启动Ability时的窗口最小高度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param minWindowHeight 启动Ability时的窗口最小高度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsMinWindowHeight(
    AbilityRuntime_StartOptions *startOptions, int32_t minWindowHeight);

/**
 * @brief 获取启动Ability时的窗口最小高度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param minWindowHeight 启动Ability时的窗口最小高度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsMinWindowHeight(
    AbilityRuntime_StartOptions *startOptions, int32_t &minWindowHeight);

/**
 * @brief 设置启动Ability时的窗口最大高度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param maxWindowHeight 启动Ability时的窗口最大高度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_SetStartOptionsMaxWindowHeight(
    AbilityRuntime_StartOptions *startOptions, int32_t maxWindowHeight);

/**
 * @brief 获取启动Ability时的窗口最大高度，单位为vp。
 *
 * @param startOptions AbilityRuntime_StartOptions对象。
 * @param maxWindowHeight 启动Ability时的窗口最大高度，单位为vp。
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 *         在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示startOptions无效。
 * @since 17
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_GetStartOptionsMaxWindowHeight(
    AbilityRuntime_StartOptions *startOptions, int32_t &maxWindowHeight);

#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // ABILITY_RUNTIME_START_OPTIONS_H