/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_MANAGEMENT_OH_FILE_URI_H
#define FILE_MANAGEMENT_OH_FILE_URI_H

/**
 * @addtogroup fileuri
 * @{
 *
 * @brief 文件统一资源标识符（File Uniform Resource Identifier）。\n
 * 支持fileuri与路径path的转换、有效性校验、以及指向的变换（指向的文件或路径）。\n
 * 该类主要用于 uri 格式验证和 uri 转换处理。
 * 且uri用于应用间文件分享场景，将应用沙箱路径按照固定关系转换为uri;\n
 * 调用者需保证所有接口入参的有效性，接口按照固定规则转换输出结果，并不检查其是否存在。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */

/**
 * @file oh_file_uri.h
 *
 * @brief 提供uri和路径path之间的相互转换，目录uri获取，以及uri的有效性校验的方法。
 * @library libohfileuri.so
 * @since 12
 */

#include "error_code.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 通过传入的路径path生成应用自己的uri；\n
 * 将path转uri时，路径中的中文及非数字字母的特殊字符将会被编译成对应的ASCII码，拼接在uri中。
 *
 * @param path 表示要转换的路径。
 * @param length 表示要转换路径的字节长度。
 * @param result 表示转换后的uri, 需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。\n
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因：\n
 *                  1. 参数path为空指针；\n
 *                  2. 参数result为空指针；\n
 *                  3. 输入的path长度与length不一致。\n
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误。转换后的uri长度为0会返回此错误。\n
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。\n
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetUriFromPath(const char *path, unsigned int length, char **result);

/**
 * @brief 将uri转换成对应的沙箱路径path。 \n
 * 1、uri转path过程中会将uri中存在的ASCII码进行解码后拼接在原处，非系统接口生成的uri中可能存在ASCII码解析范围之外的字符，
      导致字符串无法正常拼接；
 * 2、转换处理为系统约定的字符串替换规则（规则随系统演进可能会发生变化），转换过程中不进行路径校验操作，无法保证转换结果的一定可以访问。
 *
 * @param uri 表示要转换的uri。
 * @param length 表示要转换uri的字节长度。
 * @param result 表示转换后的路径path。需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。\n
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因：\n
 *                  1. 参数uri为空指针；\n
 *                  2. 参数result为空指针；\n
 *                  3. 输入的uri长度与length不一致。\n
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误。转换后的路径path长度为0会返回此错误。\n
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetPathFromUri(const char *uri, unsigned int length, char **result);

/**
 * @brief 获取所在路径uri。uri指向文件则返回所在路径的uri，uri指向目录则不处理直接返回原串；\n
 * uri指向的文件不存在或属性获取失败则返回空串。
 *
 * @param uri 表示要获取目录的uri的原始uri。
 * @param length  表示原始uri的字节长度。
 * @param result 表示获取到的目录uri，需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。\n
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因：\n
 *                  1. 参数uri为空指针；\n
 *                  2. 参数result为空指针；\n
 *                  3. 输入的uri长度与length不一致。\n
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误。获取到的目录uri长度为0会返回此错误。\n
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。\n
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetFullDirectoryUri(const char *uri, unsigned int length, char **result);

/**
 * @brief 判断传入的uri的格式是否正确。仅校验uri是否满足系统定义的格式规范，不校验uri的有效性。
 *
 * @param uri 表示需要校验的uri。
 * @param length 需要校验uri的字节长度。
 * @return 返回 true: 传入uri是有效的uri；false: 传入的uri是无效的uri。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
bool OH_FileUri_IsValidUri(const char *uri, unsigned int length);

/**
 * @brief 通过传入的uri获取到对应的文件名称。（如果文件名中存在ASCII码将会被解码处理后拼接在原处）。
 *
 * @param uri 传入的uri。
 * @param length 表示传入uri的字节长度。
 * @param result 表示转换后的名称。需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。\n
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因：\n
 *                  1. 参数uri为空指针；\n
 *                  2. 参数result为空指针；\n
 *                  3. 输入的uri长度与length不一致；\n
 *                  4. uri格式不正确。\n
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。\n
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 13
 */
FileManagement_ErrCode OH_FileUri_GetFileName(const char *uri, unsigned int length, char **result);
#ifdef __cplusplus
};
#endif
#endif // FILE_MANAGEMENT_OH_FILE_URI_H
