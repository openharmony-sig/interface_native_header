/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_IMAGE_FILTER_H
#define C_INCLUDE_DRAWING_IMAGE_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_image_filter.h
 *
 * @brief Declares the functions related to the image filter in the drawing module.
 *
 * File to include: native_drawing/drawing_image_filter.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_shader_effect.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_ImageFilter</b> object with a given blur type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sigmaX Deviation of the Gaussian blur to apply along the X axis.
 * @param sigmaY Deviation of the Gaussian blur to apply along the Y axis.
 * @param OH_Drawing_TileMode Tile mode of the image effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param input Pointer to the filter to which the image filter will be applied. If NULL is passed in,
 * the image filter is directly applied to the original image.
 * @return Returns the pointer to the {@link OH_Drawing_ImageFilter} object created.
 * If NULL is returned, the creation fails.
 * The possible failure cause is that no memory is available.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ImageFilter* OH_Drawing_ImageFilterCreateBlur(float sigmaX, float sigmaY, OH_Drawing_TileMode,
    OH_Drawing_ImageFilter* input);

/**
 * @brief Creates an <b>OH_Drawing_ImageFilter</b> object with a color filter effect.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>colorFilter</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param colorFilter Point to an {@link OH_Drawing_ColorFilter} object.
 * @param input Pointer to the filter to which the image filter will be applied.
 * If NULL is passed in, the image filter is directly applied to the original image.
 * @return Returns the pointer to the {@link OH_Drawing_ImageFilter} object created.
 * If NULL is returned, the creation fails.
 * The possible failure cause is that no memory is available or <b>colorFilter</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ImageFilter* OH_Drawing_ImageFilterCreateFromColorFilter(OH_Drawing_ColorFilter* colorFilter,
    OH_Drawing_ImageFilter* input);

/**
 * @brief Destroys an {@link OH_Drawing_ImageFilter} object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ImageFilter Pointer to the {@link OH_Drawing_ImageFilter} object obtained.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ImageFilterDestroy(OH_Drawing_ImageFilter*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
