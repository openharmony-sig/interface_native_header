/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HILOG_H
#define HIVIEWDFX_HILOG_H
/**
 * @addtogroup HiLog
 * @{
 * 
 * @brief HiLog模块实现日志打印功能。
 * 
 * 开发者可以通过使用这些接口实现日志相关功能，输出日志时可以指定日志类型、所属业务领域、日志TAG标识、日志级别等。
 *
 * @syscap SystemCapability.HiviewDFX.HiLog
 *
 * @since 8
 */

/**
 * @file log.h
 * 
 * @brief HiLog模块日志接口定义，通过这些接口实现日志打印相关功能。
 *
 * 用户输出日志时，先定义日志所属业务领域、日志TAG，然后按照类型、级别选择对应API，指定参数隐私标识输出日志内容。\n
 * 业务领域：指定日志所对应的业务领域，用户自定义使用，用于标识业务的子系统、模块。16进制整数，范围0x0~0xFFFF，超出范围则日志无法打印。\n
 * 日志TAG：字符串常量，用于标识调用所在的类或者业务。\n
 * 日志级别：DEBUG、INFO、WARN、ERROR、FATAL。\n
 * 参数格式：类printf的%方式，包括格式字符串（包括参数类型标识）和变参。\n
 * 隐私参数标识：在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。注意：每个参数未指定隐私标识时，缺省为隐私。\n
 *
 * 使用示例：\n
 * 定义业务领域、TAG：\n
 *     #define LOG_DOMAIN 0x0201\n
 *     #define LOG_TAG "MY_TAG"\n
 * 日志打印：\n
 *     HILOG_WARN({@link LOG_APP}, "Failed to visit %{private}s, reason:%{public}d.", url, errno);\n
 * 结果输出：\n
 *     05-06 15:01:06.870 1051 1051 W 0201/MY_TAG: Failed to visit <private>, reason:503.\n
 * 
 * @since 8
 */
#include <stdarg.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 日志所对应的业务领域，用于标识业务的子系统、模块。
 *
 * 16进制整数，有效范围为0x0~0xFFFF，超出范围则日志无法打印。\n
 * 
 * @since 8
 */
#ifndef LOG_DOMAIN
#define LOG_DOMAIN 0
#endif

/**
 * @brief 字符串常量，标识调用所在的类或者业务。tag最多为31字节，超出后会截断。不建议使用中文字符，可能出现乱码或者对齐问题。
 *
 * @since 8
 */
#ifndef LOG_TAG
#define LOG_TAG NULL
#endif

/**
 * @brief 日志类型。
 *
 * 该枚举类型用于定义应用开发者可以使用的日志类型。当前有应用日志LOG_APP。\n
 * 
 * @since 8
 */
enum LogType
{
      /** 应用日志 */
      LOG_APP = 0,  
}; 

/**
 * @brief 日志级别。
 *
 * 该枚举类型用于定义日志级别。各级别建议使用方式：\n
 * DEBUG：比INFO级别更详细的流程记录，通过该级别的日志可以更详细地分析业务流程和定位分析问题。DEBUG级别的日志在正式发布版本中默认不会被打印，只有在调试版本或打开调试开关的情况下才会打印。\n
 * INFO：用来记录业务关键流程节点，可以还原业务的主要运行过程；用来记录非正常情况信息，但这些情况都是可以预期的(如无网络信号、登录失败等)。这些日志都应该由该业务内处于支配地位的模块来记录，避免在多个被调用的模块或低级函数中重复记录。\n
 * WARN：发生了较为严重的非预期情况，但是对用户影响不大，程序可以自动恢复或通过简单的操作就可以恢复的问题。\n
 * ERROR：程序或功能发生了错误，该错误会影响功能的正常运行或用户的正常使用，可以恢复但恢复代价较高，如重置数据等。\n
 * FATAL：重大致命异常，表明程序或功能即将崩溃，故障无法恢复。\n
 * 
 * @since 8
 */
enum LogLevel            
{
      /** DEBUG日志级别，使用OH_LOG_DEBUG接口打印。 */
      LOG_DEBUG = 3,  
      /** INFO日志级别，使用OH_LOG_INFO接口打印。 */
      LOG_INFO = 4, 
      /** WARN日志级别，使用OH_LOG_WARN接口打印。 */
      LOG_WARN = 5, 
      /** ERROR日志级别，使用OH_LOG_ERROR接口打印。 */
      LOG_ERROR = 6, 
      /** FATAL日志级别，使用OH_LOG_FATAL接口打印。 */
      LOG_FATAL = 7, 
}; 

/**
 * @brief 写日志接口。
 *
 * 指定日志类型、日志级别、业务领域、TAG，按照类printf格式类型和隐私指示确定需要输出的变参。
 * 
 * @param type 日志类型，三方应用日志类型为LOG_APP。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @param domain 日志业务领域，16进制整数，范围0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @return 大于等于0表示成功；小于0表示失败。
 *
 * @since 8
 */
int OH_LOG_Print(LogType type, LogLevel level, unsigned int domain, const char *tag, const char *fmt, ...)
    __attribute__((__format__(os_log, 5, 6)));

/**
 * @brief 检查指定业务领域、TAG、级别的日志是否可以打印。
 * 
 * @param domain 日志业务领域，16进制整数，范围0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @return 如果指定domain、tag、level日志可以打印则返回true；否则返回false。
 *
 * @since 8
 */
bool OH_LOG_IsLoggable(unsigned int domain, const char *tag, LogLevel level);

/**
 * @brief DEBUG级别写日志，宏封装接口。
 *
 * 使用时需要先定义日志业务领域、日志TAG，一般在源文件起始处统一定义一次。\n
 * 
 * @param type 日志类型，三方应用日志类型为{@link LOG_APP}。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @see OH_LOG_Print 
 *
 * @since 8
 */
#define OH_LOG_DEBUG(type, ...) ((void)OH_LOG_Print((type), LOG_DEBUG, LOG_DOMAIN, LOG_TAG, __VA_ARGS__))

/**
 * @brief INFO级别写日志，宏封装接口。
 *
 * 使用时需要先定义日志业务领域、日志TAG，一般在源文件起始处统一定义一次。\n
 * 
 * @param type 日志类型，三方应用日志类型为LOG_APP。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @see OH_LOG_Print 
 *
 * @since 8
 */
#define OH_LOG_INFO(type, ...) ((void)OH_LOG_Print((type), LOG_INFO, LOG_DOMAIN, LOG_TAG, __VA_ARGS__))

/**
 * @brief WARN级别写日志，宏封装接口。
 *
 * 使用时需要先定义日志业务领域、日志TAG，一般在源文件起始处统一定义一次。
 * 
 * @param type 日志类型，三方应用日志类型为{@link LOG_APP}。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @see OH_LOG_Print 
 *
 * @since 8
 */
#define OH_LOG_WARN(type, ...) ((void)OH_LOG_Print((type), LOG_WARN, LOG_DOMAIN, LOG_TAG, __VA_ARGS__))

/**
 * @brief ERROR级别写日志，宏封装接口。
 *
 * 使用时需要先定义日志业务领域、日志TAG，一般在源文件起始处统一定义一次。
 * 
 * @param type 日志类型，三方应用日志类型为{@link LOG_APP}。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @see OH_LOG_Print
 *
 * @since 8
 */
#define OH_LOG_ERROR(type, ...) ((void)OH_LOG_Print((type), LOG_ERROR, LOG_DOMAIN, LOG_TAG, __VA_ARGS__))

/**
 * @brief FATAL级别写日志，宏封装接口。
 *
 * 使用时需要先定义日志业务领域、日志TAG，一般在源文件起始处统一定义一次。\n
 * 
 * @param type 日志类型，三方应用日志类型为{@link LOG_APP}。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中%符号后类型前增加{public}、{private}标识。
 * @param ... 与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @see OH_LOG_Print
 *
 * @since 8
 */
#define OH_LOG_FATAL(type, ...) ((void)HiLogPrint((type), LOG_FATAL, LOG_DOMAIN, LOG_TAG, __VA_ARGS__))

/**
 * @brief 函数指针，开发者自定义回调函数内容，在回调函数中，可自行对hilog日志进行处理。
 *
 * @param type 日志类型，三方应用日志类型为{@link LOG_APP}。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @param domain 日志业务领域，16进制整数，范围为0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param msg 日志内容，格式化之后的日志字符串。
 * @since 11
 */
typedef void (*LogCallback)(const LogType type, const LogLevel level, const unsigned int domain, const char *tag,
    const char *msg);
 
/**
 * @brief 注册函数。
 *
 * 调用此函数后，用户实现的回调函数可以接收当前进程的所有hilog日志。\n
 * 请注意，无论是否调用该接口，它都不会更改当前进程的hilog日志的默认行为。
 *
 * @param callback 用户实现的回调函数。如果不需要处理hilog日志，可以传输空指针。
 * @since 11
 */
void OH_LOG_SetCallback(LogCallback callback);

/**
 * @brief 设置应用日志打印的最低日志级别。
 * 进程在打印日志时，需要同时校验该日志级别和全局日志级别，<br/>所以设置的日志级别不能低于全局日志级别，[全局日志级别](../../dfx/hilog.md#查看和设置日志级别)默认为Info。
 *
 * @param level 指定日志level。
 * @since 15
 */
void OH_LOG_SetMinLogLevel(LogLevel level);

/**
 * @brief 写日志接口。输出指定type、level、domain、tag的常量日志字符串。
 *
 * @param type 日志类型，三方应用日志类型为LOG_APP。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @param domain 日志业务领域，16进制整数，范围为0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param message 常量日志字符串。
 * @return 大于等于0表示成功；小于0表示失败。
 *
 * @since 18
 */
int OH_LOG_PrintMsg(LogType type, LogLevel level, unsigned int domain, const char *tag, const char *message)

/**
 * @brief 写日志接口。输出指定domain、tag和日志级别的常量日志字符串，需要指定tag及字符串长度，和OH_LOG_PrintMsg区别是可以接受不带结束符的字符串。
 *
 * @param type 日志类型，三方应用日志类型为LOG_APP。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @param domain 日志业务领域，16进制整数，范围为0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param tagLen tag长度。
 * @param message 常量日志字符串。
 * @param messageLen 常量字符串长度，小于3500。
 * @return 大于等于0表示成功；小于0表示失败。
 *
 * @since 18
 */
int OH_LOG_PrintMsgByLen(LogType type, LogLevel level, unsigned int domain, const char *tag, size_t tagLen, const char *message, size_t messageLen)

/**
 * @brief 写日志接口。指定日志类型、日志级别、业务领域、TAG，按照类printf格式类型和隐私指示确定需要输出的变参，变参为va_list类型。
 *
 * @param type 日志类型，三方应用日志类型为LOG_APP。
 * @param level 日志级别，日志级别包括LOG_DEBUG、LOG_INFO、LOG_WARN、LOG_ERROR、LOG_FATAL。
 * @param domain 日志业务领域，16进制整数，范围为0x0~0xFFFF，超出范围则日志无法打印。
 * @param tag 日志TAG，字符串，标识调用所在的类或者业务。tag最多为31字节，超出后会截断，不建议使用中文字符，可能出现乱码或者对齐问题。
 * @param fmt 格式化字符串，基于类printf格式的增强，支持隐私参数标识，即在格式字符串每个参数中符号后类型前增加{public}、{private}标识。
 * @param ap va_list类型，与格式字符串里参数类型对应的参数列表，参数数目、参数类型必须与格式字符串中的标识一一对应。
 * @return 大于等于0表示成功；小于0表示失败。
 *
 * @since 18
 */
int OH_LOG_VPrint(LogType type, LogLevel level, unsigned int domain, const char *tag, const char *fmt, va_list ap)

#ifdef __cplusplus
}
#endif
/** @} */
#endif  // HIVIEWDFX_HILOG_H