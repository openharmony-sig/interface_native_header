/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_REGISTER_FONT_H
#define C_INCLUDE_DRAWING_REGISTER_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_register_font.h
 *
 * @brief Declares the functions related to the font manager in the drawing module.
 *
 * File to include: native_drawing/drawing_register_font.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_text_declaration.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief Registers a custom font with the font manager. The supported font file formats are .ttf and .otf.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an <b>OH_Drawing_FontCollection</b> object.
 * @param fontFamily Pointer to the family name of the font to register.
 * @param familySrc Pointer to the path of the font file.
 * @return Returns <b>0</b> if the font is registered; returns <b>1</b> if the file does not exist;
 * returns <b>2</b> if opening the file fails; returns <b>3</b> if reading the file fails;
 * returns <b>4</b> if the file is not found; returns <b>5</b> if the file size is not obtained;
 * returns <b>9</b> if the file is damaged.
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_RegisterFont(OH_Drawing_FontCollection*, const char* fontFamily, const char* familySrc);

/**
 * @brief Registers a font buffer with the font manager.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontCollection Pointer to an <b>OH_Drawing_FontCollection</b> object.
 * @param fontFamily Pointer to the family name of the font to register.
 * @param fontBuffer Pointer to the buffer of the font file.
 * @param length Length of the font file.
 * @return Returns <b>0</b> if the font is registered; returns <b>6</b> if the buffer size is zero;
 * returns <b>7</b> if the font set is empty; returns <b>9</b> if the file is damaged.
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_RegisterFontBuffer(OH_Drawing_FontCollection*, const char* fontFamily, uint8_t* fontBuffer,
    size_t length);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
