/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供获取pixelmap的数据和信息的接口方法。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 1.0
 */

/**
 * @file image_pixel_map_mdk.h
 *
 * @brief 声明可以锁定并访问pixelmap数据的方法，声明解锁的方法。
 * Need link <b>libpixelmapndk.z.so</b>
 *
 * @since 10
 * @version 1.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_MDK_H_
#include <stdint.h>
#include "napi/native_api.h"
#include "image_mdk_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义native层像素位图信息。
 * @since 10
 * @version 1.0
 */
struct NativePixelMap_;

/**
 * @brief 定义native层pixelmap数据类型名称。
 * @since 10
 * @version 1.0
 */
typedef struct NativePixelMap_ NativePixelMap;

/**
 * @brief 用于定义 pixel map 的相关信息。
 *
 * @since 10
 * @version 1.0
 */
typedef struct OhosPixelMapInfos {
    /** 图片的宽, 用pixels表示 */
    uint32_t width;
    /** 图片的高, 用pixels表示 */
    uint32_t height;
    /** 每行的bytes数 */
    uint32_t rowSize;
    /** Pixel 的格式 */
    int32_t pixelFormat;
} OhosPixelMapInfos;

/**
 * @brief PixelMap 透明度类型的枚举。
 *
 * @since 10
 * @version 1.0
 */
enum {
    /**
     * 未知的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_UNKNOWN = 0,
    /**
     * 不透明的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_OPAQUE = 1,
    /**
     * 预乘的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_PREMUL = 2,
    /**
     * 预除的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_UNPREMUL = 3
};

/**
 * @brief PixelMap 编辑类型的枚举。
 *
 * @since 10
 * @version 1.0
 */
enum {
    /**
     * 只读的格式
     */
    OHOS_PIXEL_MAP_READ_ONLY = 0,
    /**
     * 可编辑的格式
     */
    OHOS_PIXEL_MAP_EDITABLE = 1,
};

/**
 * @brief Pixelmap缩放时采用的缩放算法。
 *
 * @since 12
 * @version 1.0
 */
typedef enum {
    /**
     * 最近邻缩放算法。
     */
    OH_PixelMap_AntiAliasing_NONE = 0,
    /**
     * 双线性缩放算法。
     */
    OH_PixelMap_AntiAliasing_LOW = 1,
    /**
     * 双线性缩放算法，同时开启mipmap。
     */
    OH_PixelMap_AntiAliasing_MEDIUM = 2,
    /**
     * cubic缩放算法。
     */
    OH_PixelMap_AntiAliasing_HIGH = 3,
} OH_PixelMap_AntiAliasingLevel;

/**
 * @brief 用于定义创建 pixel map 设置选项的相关信息。
 *
 * @since 10
 * @version 1.0
 */
struct OhosPixelMapCreateOps {
    /** 图片的宽, 用pixels表示 */
    uint32_t width;
    /** 图片的高, 用pixels表示 */
    uint32_t height;
    /** 图片的格式 */
    int32_t pixelFormat;
    /** 图片的编辑类型 */
    uint32_t editable;
    /** 图片的alpha类型 */
    uint32_t alphaType;
    /** 图片的缩放类型 */
    uint32_t scaleMode;
};

/**
 * @brief @brief 创建<b>PixelMap</b>对象。
 *
 * @param env napi的环境指针。
 * @param info pixel map 数据设置项。
 * @param buf 图片的buffer数据。
 * @param len 图片大小信息。
 * @param res 应用层的 <b>PixelMap</b> 对象的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果图像头解码失败则返回 IMAGE_RESULT_DECODE_HEAD_ABNORMAL {@link IRNdkErrCode}；
 * 如果创建解码器失败则返回 IMAGE_RESULT_CREATE_DECODER_FAILED {@link IRNdkErrCode}；
 * 如果创建编码器失败则返回 IMAGE_RESULT_CREATE_ENCODER_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像解码失败则返回 IMAGE_RESULT_DECODE_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像初始化失败则返回 IMAGE_RESULT_INIT_ABNORMAL {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果图像添加像素位图失败则返回 IMAGE_RESULT_ENCODE_FAILED {@link IRNdkErrCode}；
 * 如果图像不支持硬件解码则返回 IMAGE_RESULT_HW_DECODE_UNSUPPORT {@link IRNdkErrCode}；
 * 如果硬件解码失败则返回 IMAGE_RESULT_HW_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果ipc失败则返回 IMAGE_RESULT_INDEX_INVALID {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see CreatePixelMap
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_CreatePixelMap(napi_env env, OhosPixelMapCreateOps info,
    void* buf, size_t len, napi_value* res);

/**
 * @brief 创建<b>PixelMap</b>对象。当前只支持输入流为BGRA格式的流。pixelmap内存在RGBA格式下，默认为DMA内存（图片512*512以上）。
 *
 * @param env napi的环境指针。
 * @param info PixelMap数据设置项。
 * @param buf 图片的buffer数据。
 * @param len 图片buffer大小信息。
 * @param rowStride 图片跨距信息。 跨距，图像每行占用的真实内存大小，单位为字节。跨距 = width * 单位像素字节数 + padding，padding为每行为内存对齐做的填充区域。
 * @param res 应用层的 PixelMap 对象的指针。
 * @return 如果操作成功则返回IMAGE_RESULT_SUCCESS;
 * 如果参数错误则返回IMAGE_RESULT_BAD_PARAMETER;
 * 如果JNI环境异常则返回IMAGE_RESULT_JNI_ENV_ABNORMAL;
 * 如果图像获取数据失败则返回IMAGE_RESULT_GET_DATA_ABNORMAL;
 * 如果检查格式失败则返回IMAGE_RESULT_CHECK_FORMAT_ERROR;
 * 如果图像输入数据失败则返回IMAGE_RESULT_DATA_ABNORMAL;
 * 如果共享内存数据错误则返回IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL;
 * 如果属性无效或图像数据不支持则返回IMAGE_RESULT_DATA_UNSUPPORT;
 * 如果图像格式未知则返回IMAGE_RESULT_UNKNOWN_FORMAT;
 * @see CreatePixelMapWithStride
 * @since 12
 * @version 1.0
 */
int32_t OH_PixelMap_CreatePixelMapWithStride(napi_env env, OhosPixelMapCreateOps info,
    void* buf, size_t len, int32_t rowStride, napi_value* res);

/**
 * @brief 根据Alpha通道的信息，来生成一个仅包含Alpha通道信息的<b>PixelMap</b>对象。
 *
 * @param env napi的环境指针。
 * @param source <b>PixelMap</b>数据设置项。
 * @param alpha alpha通道的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果图像头解码失败则返回 IMAGE_RESULT_DECODE_HEAD_ABNORMAL {@link IRNdkErrCode}；
 * 如果创建解码器失败则返回 IMAGE_RESULT_CREATE_DECODER_FAILED {@link IRNdkErrCode}；
 * 如果创建编码器失败则返回 IMAGE_RESULT_CREATE_ENCODER_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像解码失败则返回 IMAGE_RESULT_DECODE_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像初始化失败则返回 IMAGE_RESULT_INIT_ABNORMAL {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果图像添加像素位图失败则返回 IMAGE_RESULT_ENCODE_FAILED {@link IRNdkErrCode}；
 * 如果图像不支持硬件解码则返回 IMAGE_RESULT_HW_DECODE_UNSUPPORT {@link IRNdkErrCode}；
 * 如果硬件解码失败则返回 IMAGE_RESULT_HW_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果ipc失败则返回 IMAGE_RESULT_INDEX_INVALID {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see CreateAlphaPixelMap
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_CreateAlphaPixelMap(napi_env env, napi_value source, napi_value* alpha);

/**
 * @brief 初始化<b>PixelMap</b>对象数据。
 *
 * @param env napi的环境指针。
 * @param source <b>PixelMap</b> 数据设置项。
 * @return 操作成功则返回NativePixelMap的指针；如果操作失败，则返回错误码。
 * @see InitNativePixelMap
 * @since 10
 * @version 1.0
 */
NativePixelMap* OH_PixelMap_InitNativePixelMap(napi_env env, napi_value source);

/**
 * @brief 获取<b>PixelMap</b>对象每行字节数。
 *
 * @param native NativePixelMap的指针。
 * @param num <b>PixelMap</b>对象的每行字节数指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see GetBytesNumberPerRow
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_GetBytesNumberPerRow(const NativePixelMap* native, int32_t* num);

/**
 * @brief 获取<b>PixelMap</b>对象是否可编辑的状态。
 *
 * @param native NativePixelMap的指针。
 * @param editable <b>PixelMap</b> 对象是否可编辑的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see GetIsEditable
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_GetIsEditable(const NativePixelMap* native, int32_t* editable);

/**
 * @brief 获取<b>PixelMap</b>对象是否支持Alpha通道。
 *
 * @param native NativePixelMap的指针。
 * @param alpha 是否支持Alpha的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see IsSupportAlpha
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_IsSupportAlpha(const NativePixelMap* native, int32_t* alpha);

/**
 * @brief 设置<b>PixelMap</b>对象的Alpha通道。
 *
 * @param native NativePixelMap的指针。
 * @param alpha Alpha通道。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see SetAlphaAble
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_SetAlphaAble(const NativePixelMap* native, int32_t alpha);

/**
 * @brief 获取<b>PixelMap</b>对象像素密度。
 *
 * @param native NativePixelMap的指针。
 * @param density 像素密度指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see GetDensity
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_GetDensity(const NativePixelMap* native, int32_t* density);

/**
 * @brief 设置<b>PixelMap</b>对象像素密度。
 *
 * @param native NativePixelMap的指针。
 * @param density 像素密度。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see GetDensity
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_SetDensity(const NativePixelMap* native, int32_t density);

/**
 * @brief 设置<b>PixelMap</b>对象的透明度。
 *
 * @param native NativePixelMap的指针。
 * @param opacity 透明度。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see SetOpacity
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_SetOpacity(const NativePixelMap* native, float opacity);

/**
 * @brief 设置<b>PixelMap</b>对象的缩放。
 *
 * @param native NativePixelMap的指针。
 * @param x 宽度的缩放比例。
 * @param y 高度的缩放比例。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像初始化失败则返回 IMAGE_RESULT_INIT_ABNORMAL {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see Scale
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_Scale(const NativePixelMap* native, float x, float y);

/**
 * @brief 根据指定的缩放算法和输入的宽高对图片进行缩放。
 *
 * @param native NativePixelMap的指针。
 * @param x 宽度的缩放比例。
 * @param y 高度的缩放比例。
 * @param level 缩放算法。
 * @return 如果操作成功则返回IMAGE_RESULT_SUCCESS;
 * 如果JNI环境异常则返回IMAGE_RESULT_JNI_ENV_ABNORMAL;
 * 如果参数无效则返回IMAGE_RESULT_INVALID_PARAMETER;
 * 如果图像获取数据失败则返回IMAGE_RESULT_GET_DATA_ABNORMAL;
 * 如果检查格式失败则返回IMAGE_RESULT_CHECK_FORMAT_ERROR;
 * 如果skia能力失败则返回IMAGE_RESULT_THIRDPART_SKIA_ERROR;
 * 如果共享内存数据错误则返回IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL;
 * 如果图像分配内存失败则返回IMAGE_RESULT_MALLOC_ABNORMAL;
 * 如果图像格式未知则返回IMAGE_RESULT_UNKNOWN_FORMAT;
 * @see ScaleWithAntiAliasing
 * @since 12
 * @version 1.0
 */
int32_t OH_PixelMap_ScaleWithAntiAliasing(const NativePixelMap* native, float x, float y,
    OH_PixelMap_AntiAliasingLevel level);

/**
 * @brief 设置<b>PixelMap</b>对象的偏移。
 *
 * @param native NativePixelMap的指针。
 * @param x 水平偏移量。
 * @param y 垂直偏移量。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see Translate
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_Translate(const NativePixelMap* native, float x, float y);

/**
 * @brief 设置<b>PixelMap</b>对象的旋转。
 *
 * @param native NativePixelMap的指针。
 * @param angle 旋转角度。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see Rotate
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_Rotate(const NativePixelMap* native, float angle);

/**
 * @brief 设置<b>PixelMap</b>对象的翻转。
 *
 * @param native NativePixelMap的指针。
 * @param x 根据水平方向x轴进行图片翻转。
 * @param y 根据垂直方向y轴进行图片翻转。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see Flip
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_Flip(const NativePixelMap* native, int32_t x, int32_t y);

/**
 * @brief 设置<b>PixelMap</b>对象的裁剪。
 *
 * @param native NativePixelMap的指针。
 * @param x 目标图片左上角的x坐标。
 * @param y 目标图片左上角的y坐标。
 * @param width 裁剪区域的宽度。
 * @param height 裁剪区域的高度。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see Crop
 * @since 10
 * @version 1.0
 */
int32_t OH_PixelMap_Crop(const NativePixelMap* native, int32_t x, int32_t y, int32_t width, int32_t height);

/**
 * @brief 获取<b>PixelMap</b>对象图像信息。
 *
 * @param native NativePixelMap的指针。
 * @param info 图像信息指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see OhosPixelMapInfos
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_GetImageInfo(const NativePixelMap* native, OhosPixelMapInfos *info);

/**
 * @brief 获取native <b>PixelMap</b> 对象数据的内存地址，并锁定该内存。
 *
 * @param native NativePixelMap的指针。
 * @param addr 用于指向的内存地址的双指针对象。
 * @see UnAccessPixels
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see AccessPixels
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_AccessPixels(const NativePixelMap* native, void** addr);

/**
/**
 * @brief 释放native <b>PixelMap</b>对象数据的内存锁，用于匹配方法{@link OH_PixelMap_AccessPixels}。
 *
 * @param native NativePixelMap的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像获取数据失败则返回 IMAGE_RESULT_GET_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果解码失败则返回 IMAGE_RESULT_DECODE_FAILED {@link IRNdkErrCode}；
 * 如果检查格式失败则返回 IMAGE_RESULT_CHECK_FORMAT_ERROR {@link IRNdkErrCode}；
 * 如果skia能力失败则返回 IMAGE_RESULT_THIRDPART_SKIA_ERROR {@link IRNdkErrCode}；
 * 如果图像输入数据失败则返回 IMAGE_RESULT_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果共享内存失败则返回 IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST {@link IRNdkErrCode}；
 * 如果共享内存数据错误则返回 IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像分配内存失败则返回 IMAGE_RESULT_MALLOC_ABNORMAL {@link IRNdkErrCode}；
 * 如果图像数据不支持则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果裁剪失败失败则返回 IMAGE_RESULT_CROP {@link IRNdkErrCode}；
 * 如果图像格式未知则返回 IMAGE_RESULT_UNKNOWN_FORMAT {@link IRNdkErrCode}；
 * 如果注册插件失败失败则返回 IMAGE_RESULT_PLUGIN_REGISTER_FAILED {@link IRNdkErrCode}；
 * 如果创建插件失败失败则返回 IMAGE_RESULT_PLUGIN_CREATE_FAILED {@link IRNdkErrCode}；
 * 如果属性无效则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果透明度类型错误则返回 IMAGE_RESULT_ALPHA_TYPE_ERROR {@link IRNdkErrCode}；
 * 如果内存分配类型错误则返回 IMAGE_RESULT_ALLOCATER_TYPE_ERROR {@link IRNdkErrCode}。
 * @see UnAccessPixels
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_UnAccessPixels(const NativePixelMap* native);

#ifdef __cplusplus
};
#endif
/** @} */

#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_NAPI_H_
