/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief Provides APIs for key-value (KV) data processing, including querying, modifying, and persisting KV data.
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file preferences_err_code.h
 *
 * @brief Defines the error codes used in the <b>Preferences</b> module.
 *
 * File to include: <database/preferences/oh_preferences_err_code.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */


#ifndef OH_PREFERENCES_ERR_CODE_H
#define OH_PREFERENCES_ERR_CODE_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the error codes.
 *
 * @since 13
 */
typedef enum OH_Preferences_ErrCode {
    /** The operation is successful. */
    PREFERENCES_OK = 0,
    /** Invalid parameter. */
    PREFERENCES_ERROR_INVALID_PARAM = 401,
    /** The system capability is not supported. */
    PREFERENCES_ERROR_NOT_SUPPORTED = 801,
    /** Base error code. */
    PREFERENCES_ERROR_BASE = 15500000,
    /** Failed to delete the file. */
    PREFERENCES_ERROR_DELETE_FILE = 15500010,
    /** The storage is abnormal. */
    PREFERENCES_ERROR_STORAGE = 15500011,
    /** Failed to allocate memory. */
    PREFERENCES_ERROR_MALLOC = 15500012,
    /** The key does not exist. */
    PREFERENCES_ERROR_KEY_NOT_FOUND = 15500013,
    /** Failed to obtain the data change subscription service. */
    PREFERENCES_ERROR_GET_DATAOBSMGRCLIENT = 15500019,
} OH_Preferences_ErrCode;

#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_ERR_CODE_H
