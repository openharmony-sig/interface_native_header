/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_IMAGE_FILTER_H
#define C_INCLUDE_DRAWING_IMAGE_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_image_filter.h
 *
 * @brief 声明与绘图模块中的图像滤波器对象相关的函数。
 *
 * @include native_drawing/drawing_image_filter.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_shader_effect.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建具有模糊效果的图像滤波器。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sigmaX 表示沿x轴方向上高斯模糊的标准差，必须大于0。
 * @param sigmaY 表示沿y轴方向上高斯模糊的标准差，必须大于0。
 * @param tileMode 图像滤波器效果平铺模式类型，支持可选的具体模式可见{@link OH_Drawing_TileMode}枚举。
 * @param imageFilter 表示将要和当前图像滤波器叠加的输入滤波器, 如果为NULL，表示直接将当前图像滤波器作用于原始图像。
 * @return 函数会返回一个指针，指针指向创建的图像滤波器对象{@link OH_Drawing_ImageFilter}。如果对象返回NULL，表示创建失败；可能的原因是可用内存为空。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ImageFilter* OH_Drawing_ImageFilterCreateBlur(float sigmaX, float sigmaY, OH_Drawing_TileMode tileMode,
    OH_Drawing_ImageFilter* imageFilter);

/**
 * @brief 创建具有颜色变换效果的图像滤波器。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * colorFilter为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param colorFilter 指向具有颜色变换效果的颜色滤波器对象{@link OH_Drawing_ColorFilter}。
 * @param imageFilter 表示将要和当前图像滤波器叠加的输入滤波器, 如果为NULL，表示直接将当前图像滤波器作用于原始图像。
 * @return 函数会返回一个指针，指针指向创建的图像滤波器对象{@link OH_Drawing_ImageFilter}。如果对象返回NULL，表示创建失败；可能的原因是可用内存为空，或者是colorFilter为NULL。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ImageFilter* OH_Drawing_ImageFilterCreateFromColorFilter(OH_Drawing_ColorFilter* colorFilter,
    OH_Drawing_ImageFilter* imageFilter);

/**
 * @brief 销毁图像滤波器对象并回收该对象占有内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param imageFilter 指向图像滤波器对象{@link OH_Drawing_ImageFilter}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ImageFilterDestroy(OH_Drawing_ImageFilter* imageFilter);

#ifdef __cplusplus
}
#endif
/** @} */
#endif