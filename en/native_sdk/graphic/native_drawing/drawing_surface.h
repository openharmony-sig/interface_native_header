/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_GPU_SURFACE_H
#define C_INCLUDE_DRAWING_GPU_SURFACE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_surface.h
 *
 * @brief Declares the functions related to the surface in the drawing module,
 * including creating, destroying, and using the surface.
 *
 * File to include: native_drawing/drawing_surface.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Surface</b> object using the GPU context to manage the content drawn on the canvas.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_GpuContext</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContext Pointer to an {@link OH_Drawing_GpuContext} object.
 * @param budgeted Whether memory allocation is included in the cache budget.
 * The value <b>true</b> means that memory allocation is included in the cache budget,
 * and <b>false</b> means the opposite.
 * @param OH_Drawing_Image_Info {@link OH_Drawing_Image_Info} object.
 * @return Returns the pointer to the {@link OH_Drawing_Surface} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Surface* OH_Drawing_SurfaceCreateFromGpuContext(
    OH_Drawing_GpuContext*, bool budgeted, OH_Drawing_Image_Info);

/**
 * @brief Obtains a canvas from an <b>OH_Drawing_Surface</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Surface</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Surface Pointer to an <b>OH_Drawing_Surface</b> object.
 * @return Returns the pointer to the {@link OH_Drawing_Canvas} object obtained.
 * The pointer returned does not need to be managed by the caller.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_SurfaceGetCanvas(OH_Drawing_Surface*);

/**
 * @brief Destroys an <b>OH_Drawing_Surface</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Surface Pointer to an <b>OH_Drawing_Surface</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SurfaceDestroy(OH_Drawing_Surface*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
