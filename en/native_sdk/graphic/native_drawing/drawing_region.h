/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_REGION_H
#define C_INCLUDE_DRAWING_REGION_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_region.h
 *
 * @brief Declares the functions related to the region in the drawing module, including creating a region,
 * setting the boundary, and destroying a region.
 *
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the operation modes available for a region.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_RegionOpMode {
    /**
     * Difference operation.
     */
    REGION_OP_MODE_DIFFERENCE,
    /**
     * Intersection operation.
     */
    REGION_OP_MODE_INTERSECT,
    /**
     * Union operation.
     */
    REGION_OP_MODE_UNION,
    /**
     * XOR operation.
     */
    REGION_OP_MODE_XOR,
    /**
     * Reverse difference operation.
     */
    REGION_OP_MODE_REVERSE_DIFFERENCE,
    /**
     * Replacement operation.
     */
    REGION_OP_MODE_REPLACE,
} OH_Drawing_RegionOpMode;

/**
 * @brief Creates an <b>OH_Drawing_Region</b> object for more accurate graphical control.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_Region} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Region* OH_Drawing_RegionCreate(void);

/**
 * @brief Checks whether a region contains the specified point.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>region</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region Pointer to an {@link OH_Drawing_Region} object.
 * @param x X coordinate of the point.
 * @param y Y coordinate of the point.
 * @return Returns <b>true</b> if the region contains the specified point; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionContains(OH_Drawing_Region* region, int32_t x, int32_t y);

/**
 * @brief Combines two regions based on the specified operation mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>region</b> or <b>dst</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>op</b> is not set to one of the enumerated values, <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region Pointer to an {@link OH_Drawing_Region} object, in which the resulting region is saved.
 * @param other Pointer to an {@link OH_Drawing_Region} object.
 * @param op Operation mode of the region. For details about the available options, see {@link OH_Drawing_RegionOpMode}.
 * @return Returns <b>true</b> if the resulting region is not empty; returns false otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionOp(OH_Drawing_Region* region, const OH_Drawing_Region* other, OH_Drawing_RegionOpMode op);

/**
 * @brief Sets the boundary for an <b>OH_Drawing_Region</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>region</b> or <b>rect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region Pointer to an {@link OH_Drawing_Region} object.
 * @param rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @return Returns <b>true</b> if the setting is successful; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionSetRect(OH_Drawing_Region* region, const OH_Drawing_Rect* rect);

/**
 * @brief Sets a region to the area described by the path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>region</b>, <b>path</b>, or <b>clip</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region Pointer to an {@link OH_Drawing_Region} object.
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param clip Pointer to an {@link OH_Drawing_Region} object.
 * @return Returns <b>true</b> if the resulting region is not empty; returns false otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionSetPath(OH_Drawing_Region* region, const OH_Drawing_Path* path, const OH_Drawing_Region* clip);

/**
 * @brief Destroys an <b>OH_Drawing_Region</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Region Pointer to an {@link OH_Drawing_Region} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RegionDestroy(OH_Drawing_Region*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
