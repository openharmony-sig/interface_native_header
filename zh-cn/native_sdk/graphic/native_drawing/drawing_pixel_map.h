/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PIXEL_MAP_H
#define C_INCLUDE_DRAWING_PIXEL_MAP_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_pixel_map.h
 *
 * @brief 声明与绘图模块中的像素图对象相关的函数。
 *
 * @include native_drawing/drawing_pixel_map.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 声明由图像框架定义的像素图对象。
 * @since 12
 * @version 1.0
 */
struct NativePixelMap_;

/**
 * @brief 声明由图像框架定义的像素图对象。
 * @since 12
 * @version 1.0
 */
struct OH_PixelmapNative;

/**
 * @brief 从图像框架定义的像素图对象中获取本模块定义的像素图对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param nativePixelMap 指向图像框架定义的像素图对象{@link NativePixelMap_}的指针。
 * @return 函数会返回一个指向本模块定义的像素图对象{@link OH_Drawing_PixelMap}的指针。如果对象返回NULL，表示创建失败；可能的原因是NativePixelMap_为NULL。
 * @since 12
 * @version 1.0
 */
OH_Drawing_PixelMap* OH_Drawing_PixelMapGetFromNativePixelMap(NativePixelMap_* nativePixelMap);

/**
 * @brief 从图像框架定义的像素图对象中获取本模块定义的像素图对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param pixelmapNative 指向图像框架定义的像素图对象{@link OH_PixelmapNative}的指针。
 * @return 函数会返回一个指向本模块定义的像素图对象{@link OH_Drawing_PixelMap}的指针。如果对象返回NULL，表示创建失败；可能的原因是OH_PixelmapNative为NULL。
 * @since 12
 * @version 1.0
 */
OH_Drawing_PixelMap* OH_Drawing_PixelMapGetFromOhPixelMapNative(OH_PixelmapNative* pixelmapNative);

/**
 * @brief 解除本模块定义的像素图对象和图像框架定义的像素图对象之间的关系，该关系通过调用{@link OH_Drawing_PixelMapGetFromNativePixelMap}或{@link OH_Drawing_PixelMapGetFromOhPixelMapNative}建立。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param pixelMap 指向像素图对象{@link OH_Drawing_PixelMap}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PixelMapDissolve(OH_Drawing_PixelMap* pixelMap);

#ifdef __cplusplus
}
#endif
/** @} */
#endif