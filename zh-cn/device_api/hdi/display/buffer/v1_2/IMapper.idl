/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.2
 */

/**
 * @file IMapper.idl
 *
 * @brief 显示内存映射接口声明。
 *
 * 模块包路径：ohos.hdi.display.buffer.v1_2
 *
 * 引用：
 * - ohos.hdi.display.buffer.v1_2.DisplayBufferType
 * - ohos.hdi.display.buffer.v1_0.IMapper
 *
 * @since 3.2
 * @version 1.2
 */

package ohos.hdi.display.buffer.v1_2;
import ohos.hdi.display.buffer.v1_2.DisplayBufferType;
import ohos.hdi.display.buffer.v1_0.IMapper;

/**
 * @brief 定义释放显示内存接口，基于v1_0.IMapper增加获取图像位置接口GetImageLayout。
 *
 * @since 3.2
 * @version 1.2
 */
interface IMapper extends ohos.hdi.display.buffer.v1_0.IMapper {
    /**
     * @brief 获取图像位置。
     *
     * @param handle 输入句柄指针。
     * @param layout 图像位置数据。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。 
     * @since 5.0
     * @version 1.2
     */
    GetImageLayout([in] NativeBuffer handle, [out] struct ImageLayout layout);
}
/** @} */