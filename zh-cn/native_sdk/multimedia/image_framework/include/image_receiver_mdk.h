/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供从native层获取图片数据的方法。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 2.0
 */

/**
 * @file image_receiver_mdk.h
 *
 * @brief 声明从native层获取图片数据的方法。
 *
 * @since 10
 * @version 2.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_MDK_H_
#include "napi/native_api.h"
#include "image_mdk_common.h"
#include "image_mdk.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义native层<b>ImageReceiver</b>对象。
 *
 * @since 10
 * @version 2.0
 */
struct ImageReceiverNative_;

/**
 * @brief 用于定义ImageReceiverNative数据类型名称。
 *
 * @since 10
 * @version 2.0
 */
typedef struct ImageReceiverNative_ ImageReceiverNative;

/**
 * @brief 定义native层图片的回调方法。
 *
 * @since 10
 * @version 2.0
 */
typedef void (*OH_Image_Receiver_On_Callback)();

/**
 * @brief 定义<b>ImageReceiver</b>的相关信息。
 *
 * @since 10
 * @version 2.0
 */
struct OhosImageReceiverInfo {
    /* 消费端接收图片时的默认图像宽度，用pixels表示 */
    int32_t width;
    /* 消费端接收图片时的默认图像高度，用pixels表示 */
    int32_t height;
    /* 通过接收器创建图像格式{@link OHOS_IMAGE_FORMAT_JPEG} */
    int32_t format;
    /* 图片缓存数量的最大值 */
    int32_t capicity;
};

/**
 * @brief 创建应用层 <b>ImageReceiver</b> 对象。
 *
 * @param env napi的环境指针。
 * @param info <b>ImageReceiver</b> 数据设置项。
 * @param res 应用层的 <b>ImageReceiver</b> 对象的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果创建surface失败则返回 IMAGE_RESULT_CREATE_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果surface分配内存失败则返回 IMAGE_RESULT_SURFACE_GRALLOC_BUFFER_FAILED {@link IRNdkErrCode}；
 * 如果获取surface失败则返回 IMAGE_RESULT_GET_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果媒体rtsp surface不支持则返回 IMAGE_RESULT_MEDIA_RTSP_SURFACE_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果媒体类型不支持失败则返回 IMAGE_RESULT_MEDIA_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see OhosImageReceiverInfo
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_CreateImageReceiver(napi_env env, struct OhosImageReceiverInfo info, napi_value* res);

/**
 * @brief 通过应用层<b>ImageReceiver</b>对象初始化native层{@link ImageReceiverNative}对象。
 *
 * @param env napi的环境指针。
 * @param source napi的 <b>ImageReceiver</b> 对象。
 * @return 操作成功则返回 {@link ImageReceiverNative} 指针；如果操作失败，则返回nullptr。
 * @see ImageReceiverNative, OH_Image_Receiver_Release
 * @since 10
 * @version 2.0
 */
ImageReceiverNative* OH_Image_Receiver_InitImageReceiverNative(napi_env env, napi_value source);

/**
 * @brief 通过{@link ImageReceiverNative}获取receiver的id。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param id 指向字符缓冲区的指针，用于获取字符串的id。
 * @param len <b>id</b>所对应的字符缓冲区的大小。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果获取surface失败则返回 IMAGE_RESULT_GET_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果媒体类型不支持失败则返回 IMAGE_RESULT_MEDIA_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_GetReceivingSurfaceId(const ImageReceiverNative* native, char* id, size_t len);

/**
 * @brief 通过{@link ImageReceiverNative}获取最新的一张图片。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param image 获取到的应用层的 <b>Image</b> 指针对象。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果创建surface失败则返回 IMAGE_RESULT_CREATE_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果surface分配内存失败则返回 IMAGE_RESULT_SURFACE_GRALLOC_BUFFER_FAILED {@link IRNdkErrCode}；
 * 如果获取surface失败则返回 IMAGE_RESULT_GET_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果媒体rtsp surface不支持则返回 IMAGE_RESULT_MEDIA_RTSP_SURFACE_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果媒体类型不支持失败则返回 IMAGE_RESULT_MEDIA_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_ReadLatestImage(const ImageReceiverNative* native, napi_value* image);

/**
 * @brief 通过{@link ImageReceiverNative}获取下一张图片。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param image 读取到的应用层的 <b>Image</b> 指针对象。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果创建surface失败则返回 IMAGE_RESULT_CREATE_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果surface分配内存失败则返回 IMAGE_RESULT_SURFACE_GRALLOC_BUFFER_FAILED {@link IRNdkErrCode}；
 * 如果获取surface失败则返回 IMAGE_RESULT_GET_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果媒体rtsp surface不支持则返回 IMAGE_RESULT_MEDIA_RTSP_SURFACE_UNSUPPORT {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果媒体类型不支持失败则返回 IMAGE_RESULT_MEDIA_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_ReadNextImage(const ImageReceiverNative* native, napi_value* image);

/**
 * @brief 注册一个{@link OH_Image_Receiver_On_Callback}回调事件。每当接收新图片，该回调事件就会响应。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param callback {@link OH_Image_Receiver_On_Callback}事件的回调函数。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果从surface获取参数失败返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果获取surface失败则返回 IMAGE_RESULT_GET_SURFACE_FAILED {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}；
 * 如果媒体类型不支持失败则返回 IMAGE_RESULT_MEDIA_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_On(const ImageReceiverNative* native, OH_Image_Receiver_On_Callback callback);

/**
 * @brief 通过{@link ImageReceiverNative}获取<b>ImageReceiver</b>的大小。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param size 作为结果的{@link OhosImageSize}指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative, OH_Image_Receiver_On_Callback
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_GetSize(const ImageReceiverNative* native, struct OhosImageSize* size);

/**
 * @brief 通过{@link ImageReceiverNative}获取<b>ImageReceiver</b>的容量。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param capacity 作为结果的指向容量的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative, OhosImageSize
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_GetCapacity(const ImageReceiverNative* native, int32_t* capacity);

/**
 * @brief 通过{@link ImageReceiverNative}获取<b>ImageReceiver</b>的格式。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @param format 作为结果的指向格式的指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果JNI环境异常则返回 IMAGE_RESULT_JNI_ENV_ABNORMAL {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative

 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_GetFormat(const ImageReceiverNative* native, int32_t* format);

/**
 * @brief 释放native层 {@link ImageReceiverNative} 对象。注意: 此方法不能释放应用层<b>ImageReceiver</b>对象。
 *
 * @param native native层的{@link ImageReceiverNative}指针。
 * @return 如果操作成功则返回 IMAGE_RESULT_SUCCESS {@link IRNdkErrCode}；
 * 如果参数错误则返回 IMAGE_RESULT_BAD_PARAMETER {@link IRNdkErrCode}；
 * 如果参数无效则返回 IMAGE_RESULT_INVALID_PARAMETER {@link IRNdkErrCode}；
 * 如果图像类型不支持失败则返回 IMAGE_RESULT_DATA_UNSUPPORT {@link IRNdkErrCode}。
 * @see ImageReceiverNative
 * @since 10
 * @version 2.0
 */
int32_t OH_Image_Receiver_Release(ImageReceiverNative* native);
#ifdef __cplusplus
};
#endif
/** @} */

#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_MDK_H_
