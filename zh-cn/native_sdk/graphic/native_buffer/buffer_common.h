/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_NativeBuffer
 * @{
 *
 * @brief 提供NativeBuffer功能，通过提供的接口，可以实现共享内存的申请、使用、属性查询、释放等操作。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

/**
 * @file buffer_common.h
 *
 * @brief 提供NativeBuffer模块的公共类型定义。\n
 * 从API version 12开始，部分类型定义从native_buffer.h移动至此头文件统一呈现，对于此类类型，API version 12之前即支持使用，各版本均可正常使用。
 *
 * @kit ArkGraphics2D
 * @library libnative_buffer.so
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */

#ifndef NDK_INCLUDE_BUFFER_COMMON_H_
#define NDK_INCLUDE_BUFFER_COMMON_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief OH_NativeBuffer的颜色空间。由native_buffer.h移动至此头文件统一呈现。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 11
 * @version 1.0
 */
typedef enum OH_NativeBuffer_ColorSpace {
    /** 无颜色空间 */
    OH_COLORSPACE_NONE,
    /** 色域范围为BT601_P，传递函数为BT709，转换矩阵为BT601_P，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_BT601_EBU_FULL,
    /** 色域范围为BT601_N，传递函数为BT709，转换矩阵为BT601_N，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_BT601_SMPTE_C_FULL,
    /** 色域范围为BT709，传递函数为BT709，转换矩阵为BT709，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_BT709_FULL,
    /** 色域范围为BT2020，传递函数为HLG，转换矩阵为BT2020，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_BT2020_HLG_FULL,
    /** 色域范围为BT2020，传递函数为PQ，转换矩阵为BT2020，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_BT2020_PQ_FULL,
    /** 色域范围为BT601_P，传递函数为BT709，转换矩阵为BT601_P，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_BT601_EBU_LIMIT,
    /** 色域范围为BT601_N，传递函数为BT709，转换矩阵为BT601_N，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_BT601_SMPTE_C_LIMIT,
    /** 色域范围为BT709，传递函数为BT709，转换矩阵为BT709，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_BT709_LIMIT,
    /** 色域范围为BT2020，传递函数为HLG，转换矩阵为BT2020，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_BT2020_HLG_LIMIT,
    /** 色域范围为BT2020，传递函数为PQ，转换矩阵为BT2020，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_BT2020_PQ_LIMIT,
    /** 色域范围为SRGB，传递函数为SRGB，转换矩阵为BT601_N，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_SRGB_FULL,
    /** 色域范围为P3_D65，传递函数为SRGB，转换矩阵为P3，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_P3_FULL,
    /** 色域范围为P3_D65，传递函数为HLG，转换矩阵为P3，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_P3_HLG_FULL,
    /** 色域范围为P3_D65，传递函数为PQ，转换矩阵为P3，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_P3_PQ_FULL,
    /** 色域范围为ADOBERGB，传递函数为ADOBERGB，转换矩阵为ADOBERGB，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_ADOBERGB_FULL,
    /** 色域范围为SRGB，传递函数为SRGB，转换矩阵为BT601_N，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_SRGB_LIMIT,
    /** 色域范围为P3_D65，传递函数为SRGB，转换矩阵为P3，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_P3_LIMIT,
    /** 色域范围为P3_D65，传递函数为HLG，转换矩阵为P3，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_P3_HLG_LIMIT,
    /** 色域范围为P3_D65，传递函数为PQ，转换矩阵为P3，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_P3_PQ_LIMIT,
    /** 色域范围为ADOBERGB，传递函数为ADOBERGB，转换矩阵为ADOBERGB，数据范围为RANGE_LIMITED。 */
    OH_COLORSPACE_ADOBERGB_LIMIT,
    /** 色域范围为SRGB，传递函数为LINEAR。 */
    OH_COLORSPACE_LINEAR_SRGB,
    /** 等同于 OH_COLORSPACE_LINEAR_SRGB。 */
    OH_COLORSPACE_LINEAR_BT709,
    /** 色域范围为P3_D65，传递函数为LINEAR。 */
    OH_COLORSPACE_LINEAR_P3,
    /** 色域范围为BT2020，传递函数为LINEAR。 */
    OH_COLORSPACE_LINEAR_BT2020,
    /** 等同于OH_COLORSPACE_SRGB_FULL。 */
    OH_COLORSPACE_DISPLAY_SRGB,
    /** 等同于OH_COLORSPACE_P3_FULL。 */
    OH_COLORSPACE_DISPLAY_P3_SRGB,
    /** 等同于OH_COLORSPACE_P3_HLG_FULL。 */
    OH_COLORSPACE_DISPLAY_P3_HLG,
    /** 等同于OH_COLORSPACE_P3_PQ_FULL。 */
    OH_COLORSPACE_DISPLAY_P3_PQ,
    /** 色域范围为BT2020，传递函数为SRGB，转换矩阵为BT2020，数据范围为RANGE_FULL。 */
    OH_COLORSPACE_DISPLAY_BT2020_SRGB,
    /** 等同于 OH_COLORSPACE_BT2020_HLG_FULL。 */
    OH_COLORSPACE_DISPLAY_BT2020_HLG,
    /** 等同于OH_COLORSPACE_BT2020_PQ_FULL。 */
    OH_COLORSPACE_DISPLAY_BT2020_PQ,
} OH_NativeBuffer_ColorSpace;

/**
 * @brief OH_NativeBuffer的图像标准。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_MetadataType {
    /**
     * 视频HLG。
     */
    OH_VIDEO_HDR_HLG,
    /**
     * 视频HDR10。
     */
    OH_VIDEO_HDR_HDR10,
    /**
     * 视频HDR VIVID。
     */
    OH_VIDEO_HDR_VIVID,
    /**
     * 无元数据。
     * @since 13
     */
    OH_VIDEO_NONE = -1
} OH_NativeBuffer_MetadataType;

/**
 * @brief 表示基色的X和Y坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_ColorXY {
    /**
     * 基色X坐标。
     */
    float x;
    /**
     * 基色Y坐标。
     */
    float y;
} OH_NativeBuffer_ColorXY;

/**
 * @brief 表示smpte2086静态元数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Smpte2086 {
    /**
     * 红基色。
     */
    OH_NativeBuffer_ColorXY displayPrimaryRed;
    /**
     * 绿基色。
     */
    OH_NativeBuffer_ColorXY displayPrimaryGreen;
    /**
     * 蓝基色。
     */
    OH_NativeBuffer_ColorXY displayPrimaryBlue;
    /**
     * 白点。
     */
    OH_NativeBuffer_ColorXY whitePoint;
    /**
     * 最大的光亮度。
     */
    float maxLuminance;
    /**
     * 最小的光亮度。
     */
    float minLuminance;
} OH_NativeBuffer_Smpte2086;

/**
 * @brief 表示CTA-861.3静态元数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Cta861 {
    /**
     * 最大内容亮度水平。
     */
    float maxContentLightLevel;
    /**
     * 最大的帧平均亮度水平。
     */
    float maxFrameAverageLightLevel;
} OH_NativeBuffer_Cta861;

/**
 * @brief 表示HDR静态元数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_StaticMetadata {
    /**
     * smpte2086静态元数据。
     */
    OH_NativeBuffer_Smpte2086 smpte2086;
    /**
     * CTA-861.3静态元数据。
     */
    OH_NativeBuffer_Cta861 cta861;
} OH_NativeBuffer_StaticMetadata;

/**
 * @brief 表示OH_NativeBuffer的HDR元数据种类的键值。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef enum OH_NativeBuffer_MetadataKey {
    /**
     * 元数据类型，其值见{@link OH_NativeBuffer_MetadataType}，size为OH_NativeBuffer_MetadataType大小。
     */
    OH_HDR_METADATA_TYPE,
    /**
     * 静态元数据，其值见{@link OH_NativeBuffer_StaticMetadata}，size为OH_NativeBuffer_StaticMetadata大小。
     */
    OH_HDR_STATIC_METADATA,
    /**
     * 动态元数据，其值见视频流中SEI的字节流，size的取值范围为1-3000。
     */
    OH_HDR_DYNAMIC_METADATA
} OH_NativeBuffer_MetadataKey;

#ifdef __cplusplus
}
#endif

/** @} */
#endif