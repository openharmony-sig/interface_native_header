/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Web
 * @{
 *
 * @brief 提供注入对象和执行JavaScript代码的API接口。
 * @since 11
 */
/**
 * @file native_interface_arkweb.h
 *
 * @brief 声明API接口供开发者使用注入对象和执行JavaScript代码等功能。
 * @kit ArkWeb
 * @library libohweb.so
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
#ifndef NATIVE_INTERFACE_ARKWEB_H
#define NATIVE_INTERFACE_ARKWEB_H

#include <stdbool.h>
#include <stdint.h>

#include "arkweb_error_code.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 定义执行JavaScript代码后返回结果的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnJavaScriptCallback)(const char*);

/**
* @brief 定义注入对象的回调函数的类型。
*
* @since 11
*/
typedef char* (*NativeArkWeb_OnJavaScriptProxyCallback)(const char** argv, int32_t argc);

/**
* @brief 定义Web组件可用时的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnValidCallback)(const char*);

/**
* @brief 定义Web组件销毁时的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnDestroyCallback)(const char*);

/**
 * @brief 在当前显示页面的环境下，加载并执行一段JavaScript代码。
 *
 * @param webTag Web组件的名称。
 * @param jsCode 一段JavaScript的代码脚本。
 * @param callback 代码执行完后通知开发者结果的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_RunJavaScript(const char* webTag, const char* jsCode, NativeArkWeb_OnJavaScriptCallback callback);

/**
 * @brief 注册对象及函数名称列表。
 *
 * @param webTag Web组件的名称。
 * @param objName 注入对象的名称。
 * @param methodList 注入函数列表的名称。
 * @param callback 注入的回调函数。
 * @param size 注入的回调函数的个数。
 * @param needRefresh 是否需要刷新页面。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_RegisterJavaScriptProxy(const char* webTag, const char* objName, const char** methodList,
    NativeArkWeb_OnJavaScriptProxyCallback* callback, int32_t size, bool needRefresh);

/**
 * @brief 删除已注册的对象及其下的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param objName 注入对象的名称。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_UnregisterJavaScriptProxy(const char* webTag, const char* objName);

/**
 * @brief 设置对象可注册时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param callback 对象可注册时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_SetJavaScriptProxyValidCallback(const char* webTag, NativeArkWeb_OnValidCallback callback);

/**
 * @brief 获取已注册的对象可注册时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @return return 已注册的对象可注册时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
NativeArkWeb_OnValidCallback OH_NativeArkWeb_GetJavaScriptProxyValidCallback(const char* webTag);

/**
 * @brief 设置组件销毁时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param callback 组件销毁时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_SetDestroyCallback(const char* webTag, NativeArkWeb_OnDestroyCallback callback);

/**
 * @brief 获取已注册的组件销毁时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @return return 已注册的组件销毁时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
NativeArkWeb_OnDestroyCallback OH_NativeArkWeb_GetDestroyCallback(const char* webTag);

/**
 * @brief 加载数据或URL，此函数应在主线程中调用。
 *
 * @param webTag Web组件的名称。
 * @param data "Base64"或"URL"编码的字符串，不能为空。
 * @param mimeType 媒体类型，例如"text/html"，不能为空。
 * @param encoding 编码类型，例如"UTF-8"，不能为空。
 * @param baseUrl 指定的URL路径("http"/"https"/"data"协议),
 *                由Web组件分配给window.origin。
 * @param historyUrl 历史URL，当它不为空时，可以通过历史记录来管理，实现前进和后退功能。
 * @return LoadData 错误码。
 *         {@link ARKWEB_SUCCESS} 加载数据成功。
 *         {@link ARKWEB_INVALID_PARAM} 必填参数未指定或参数类型不正确或参数校验失败。
 *         {@link ARKWEB_INIT_ERROR} 初始化失败，根据传入的"webTag"找不到有效的Web组件。
 *         {@link ARKWEB_LIBRARY_OPEN_FAILURE} 打开动态链接库失败。
 *         {@link ARKWEB_LIBRARY_SYMBOL_NOT_FOUND} 动态链接库中未找到所需的符号。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 15
 */
ArkWeb_ErrorCode OH_NativeArkWeb_LoadData(const char* webTag,
                                          const char* data,
                                          const char* mimeType,
                                          const char* encoding,
                                          const char* baseUrl,
                                          const char* historyUrl);

#ifdef __cplusplus
};
#endif
#endif // NATIVE_INTERFACE_ARKWEB_H