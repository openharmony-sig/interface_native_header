/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
/**
 * @addtogroup ArkUI_Accessibility
 * @{
 *
 * @brief 描述ArkUI Accessibilit对外支持的Native能力，如查询无障碍节点、上报无障碍事件等。
 *
 * @since 13
 */

/**
 * @file native_interface_accessibility.h
 *
 * @brief 声明用于访问Native Accessibility的API。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @kit ArkUI
 * @since 13
 */
#ifndef _NATIVE_INTERFACE_ACCESSIBILITY_H
#define _NATIVE_INTERFACE_ACCESSIBILITY_H

#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @brief 提供封装的ArkUI_AccessibilityElementInfo实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfo ArkUI_AccessibilityElementInfo;

/**
 * @brief 定义Accessibility事件信息。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityEventInfo ArkUI_AccessibilityEventInfo;

/**
 * @brief 定义Accessibility本地提供者。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProvider ArkUI_AccessibilityProvider;

/**
 * @brief 提供一个封装的ArkUI_AccessibilityActionArguments实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityActionArguments  ArkUI_AccessibilityActionArguments;

/**
 * @brief Accessibility操作类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_INVALID = 0,
    /** 收到事件后，组件需要对点击做出响应。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_CLICK = 0x00000010,
    /** 收到事件后，组件需要对长点击做出响应。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_LONG_CLICK = 0x00000020,
    /** 表示获取辅助功能焦点的操作，特定组件已聚焦。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_GAIN_ACCESSIBILITY_FOCUS = 0x00000040,
    /** 表示清除辅助功能焦点的操作。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_CLEAR_ACCESSIBILITY_FOCUS = 0x00000080,
    /** 滚动组件响应向前滚动动作。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_SCROLL_FORWARD = 0x00000100,
    /** 滚动组件响应反向滚动操作。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_SCROLL_BACKWARD = 0x00000200,
    /** 复制文本组件的选定内容。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_COPY = 0x00000400,
    /** 粘贴文本组件的选定内容。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_PASTE = 0x00000800,
    /** 剪切文本组件的选定内容。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_CUT = 0x00001000,
    /** 表示选择操作，需要设置selectTextBegin、TextEnd和TextInForWard参数，在编辑框中选择文本段。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_SELECT_TEXT = 0x00002000,
    /** 设置文本组件的文本内容。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_SET_TEXT = 0x00004000,
    /** 设置文本组件的光标位置。 */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_SET_CURSOR_POSITION = 0x00100000,
    /** 焦点移动操作中支持查找下一个焦点。
     *  @since 15
     */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_NEXT_HTML_ITEM = 0x02000000,
    /** 焦点移动操作中支持查找上一个焦点。
     *  @since 15
     */
    ARKUI_ACCESSIBILITY_NATIVE_ACTION_TYPE_PREVIOUS_HTML_ITEM = 0x04000000,
} ArkUI_Accessibility_ActionType;

/**
 * @brief Accessibility事件类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_INVALID = 0,
    /** 点击事件，在UI组件响应后发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_CLICKED = 0x00000001,
    /** 长点击事件，在UI组件响应后发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_LONG_CLICKED = 0x00000002,
    /** 被选中事件，控件响应完成后发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_SELECTED = 0x00000004,
    /** 文本更新事件，需要在文本更新时发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_TEXT_UPDATE = 0x00000010,
    /** 页面更新事件，当页面跳转、切换、大小更改或移动时发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_PAGE_STATE_UPDATE = 0x00000020,
    /** 页面内容发生变化时需要发送事件。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_PAGE_CONTENT_UPDATE = 0x00000800,
    /** scrolled事件，当可滚动的组件上发生滚动事件时，会发送此事件。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_SCROLLED = 0x000001000,
    /** Accessibility焦点事件，在UI组件响应后发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_ACCESSIBILITY_FOCUSED = 0x00008000,
    /** Accessibility焦点清除事件，在UI组件响应后发送。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_ACCESSIBILITY_FOCUS_CLEARED = 0x00010000,
    /** 主动请求指定节点聚焦。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_REQUEST_ACCESSIBILITY_FOCUS = 0x02000000,
    /** UI组件上报页面打开事件。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_PAGE_OPEN = 0x20000000,
    /** UI组件上报页面关闭事件。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_PAGE_CLOSE = 0x08000000,
    /** 广播Accessibility事件，请求主动播放指定的内容事件。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_ANNOUNCE_FOR_ACCESSIBILITY = 0x10000000,
    /** 焦点更新事件，用于焦点更新场景。 */
    ARKUI_ACCESSIBILITY_NATIVE_EVENT_TYPE_FOCUS_NODE_UPDATE = 0x10000001,
} ArkUI_AccessibilityEventType;

/**
 * @brief 定义Accessible操作结构体。
 *
 * @since 13
 */
typedef struct {
    /** 操作类型。 */
    ArkUI_Accessibility_ActionType actionType;
    /** 操作描述信息。 */
    const char* description;
} ArkUI_AccessibleAction;

/**
 * @brief 定义Accessible区域。
 *
 * @since 13
 */
typedef struct {
    /** 左上角x像素坐标。 */
    int32_t leftTopX;
    /** 左上角y像素坐标。 */
    int32_t leftTopY;
    /** 右下角x像素坐标。 */
    int32_t rightBottomX;
    /** 右下角y像素坐标。 */
    int32_t rightBottomY;
} ArkUI_AccessibleRect;

/**
 * @brief 定义Accessible范围信息。
 *
 * @since 13
 */
typedef struct {
    /** 最小值。 */
    double min;
    /** 最大值。 */
    double max;
    /** 当前值。 */
    double current;
} ArkUI_AccessibleRangeInfo;

/**
 * @brief 定义accessible网格信息。
 *
 * @since 13
 */
typedef struct {
    /** 行数。 */
    int32_t rowCount;
    /** 列数。 */
    int32_t columnCount;
    /** 0: 仅选择一行，否则选择多行。 */
    int32_t selectionMode;
} ArkUI_AccessibleGridInfo;

/**
 * @brief 定义accessible网格项信息。
 *
 * @since 13
 */
typedef struct {
    /** 是否头部。 */
    bool heading;
    /** 是否选择。 */
    bool selected;
    /** 列数。 */
    int32_t columnIndex;
    /** 行数。 */
    int32_t rowIndex;
    /** 列间距。 */
    int32_t columnSpan;
    /** 行间距。 */
    int32_t rowSpan;
} ArkUI_AccessibleGridItemInfo;

/**
 * @brief Accessibility错误代码状态的枚举。
 *
 * @since 13
 */
typedef enum{
    /** 成功。 */
    ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL = 0,
    /** 失败。 */
    ARKUI_ACCESSIBILITY_NATIVE_RESULT_FAILED = -1,
    /** 无效参数。 */
    ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER = -2,
    /** 内存超出。 */
    ARKUI_ACCESSIBILITY_NATIVE_RESULT_OUT_OF_MEMORY = -3,
} ArkUI_AcessbilityErrorCode;

/**
 * @brief Accessibility搜索类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 查询当前节点。 */
    ARKUI_ACCESSIBILITY_NATIVE_SEARCH_MODE_PREFETCH_CURRENT = 0,
    /** 查询父节点。 */
    ARKUI_ACCESSIBILITY_NATIVE_SEARCH_MODE_PREFETCH_PREDECESSORS = 1 << 0,
    /** 查询兄弟节点。 */
    ARKUI_ACCESSIBILITY_NATIVE_SEARCH_MODE_PREFETCH_SIBLINGS = 1 << 1,
    /** 查询下一层孩子节点。 */
    ARKUI_ACCESSIBILITY_NATIVE_SEARCH_MODE_PREFETCH_CHILDREN = 1 << 2,
    /** 查询所有孩子节点。 */
    ARKUI_ACCESSIBILITY_NATIVE_SEARCH_MODE_PREFETCH_RECURSIVE_CHILDREN = 1 << 3,
} ArkUI_AccessibilitySearchMode;

/**
 * @brief Accessibility焦点类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_ACCESSIBILITY_NATIVE_FOCUS_TYPE_INVALID = -1,
    /** 输入焦点类型。 */
    ARKUI_ACCESSIBILITY_NATIVE_FOCUS_TYPE_INPUT = 1 << 0,
    /** Accessibility焦点类型。 */
    ARKUI_ACCESSIBILITY_NATIVE_FOCUS_TYPE_ACCESSIBILITY = 1 << 1,
} ArkUI_AccessibilityFocusType;

/**
 * @brief Accessibility焦点移动方向的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_INVALID = 0,
    /** 上。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_UP = 0x00000001,
    /** 下。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_DOWN = 0x00000002,
    /** 左。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_LEFT = 0x00000004,
    /** 右。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_RIGHT = 0x00000008,
    /** 前进。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_FORWARD = 0x00000010,
    /** 后退。 */
    ARKUI_ACCESSIBILITY_NATIVE_DIRECTION_BACKWARD = 0x00000020,
} ArkUI_AccessibilityFocusMoveDirection;

/**
 * @brief 提供封装的ArkUI_AccessibilityElementInfoList实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfoList ArkUI_AccessibilityElementInfoList;

/**
 * @brief 注册Accessibility提供程序回调。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProviderCallbacks {
    /**
    * @brief 当需要基于指定节点获取元素信息时调用。
    *
    * @param elementId 表示元素ID。
    * @param mode 表示无障碍搜索模式。
    * @param requestId 表示请求ID。
    * @param elementList 表示无障碍元素信息列表。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findAccessibilityNodeInfosById)(int64_t elementId, ArkUI_AccessibilitySearchMode mode,
        int32_t requestId, ArkUI_AccessibilityElementInfoList* elementList);
    /**
    * @brief 当需要基于指定节点和文本内容获取元素信息时调用。
    *
    * @param elementId 表示元素ID。
    * @param text 表示无障碍文本。
    * @param requestId 表示请求ID。
    * @param elementList 表示无障碍元素信息列表。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findAccessibilityNodeInfosByText)(int64_t elementId, const char* text, int32_t requestId,
        ArkUI_AccessibilityElementInfoList* elementList);
    /**
    * @brief 当需要基于指定节点获取焦点元素信息时调用。
    *
    * @param elementId 表示元素ID。
    * @param focusType 表示焦点的类型。
    * @param requestId 表示请求ID。
    * @param elementInfo 表示无障碍元素信息。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findFocusedAccessibilityNode)(int64_t elementId, ArkUI_AccessibilityFocusType focusType,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementinfo);
    /**
    * @brief 根据参考节点查询可以聚焦的节点，根据模式和方向查询下一个可以聚焦的节点。
    *
    * @param elementId 表示元素ID。
    * @param direction 表示查找方向。
    * @param requestId 表示请求ID。
    * @param elementInfo 表示无障碍元素信息。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findNextFocusAccessibilityNode)(
        int64_t elementId, ArkUI_AccessibilityFocusMoveDirection direction,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementInfo);
    /**
    * @brief 在指定节点上执行Action操作。
    *
    * @param elementId 表示元素ID。
    * @param action 表示要执行的动作。
    * @param actionArguments 表示动作的参数。
    * @param requestId 表示请求的ID。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*executeAccessibilityAction)(int64_t elementId, ArkUI_Accessibility_ActionType action,
        ArkUI_AccessibilityActionArguments *actionArguments, int32_t requestId);
    /**
    * @brief 清除当前焦点节点的焦点状态。
    *
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*clearFocusedFocusAccessibilityNode)();
    /**
    * @brief 查询指定节点的当前光标位置。
    *
    * @param elementId 表示元素ID。
    * @param requestId 表示请求的ID。
    * @param index 表示光标位置的索引。
    * @return 如果操作成功，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         如果参数错误，则返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*getAccessibilityNodeCursorPosition)(int64_t elementId, int32_t requestId, int32_t* index);
} ArkUI_AccessibilityProviderCallbacks;

/**
 * @brief 注册Accessibility提供具有实例编号的程序回调。
 * @since 15
 */
typedef struct ArkUI_AccessibilityProviderCallbacksWithInstance {
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。基于指定的节点，查询所需的节点信息。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param mode 无障碍服务的搜索模式。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @param elementList 本次查询到的所有无障碍元素列表。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findAccessibilityNodeInfosById)(const char* instanceId, int64_t elementId,
        ArkUI_AccessibilitySearchMode mode, int32_t requestId, ArkUI_AccessibilityElementInfoList* elementList);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。基于指定的节点，查询满足指定组件文本内容的节点信息。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param text 组件需要匹配的文本内容。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @param elementList 本次查询到的所有无障碍元素列表。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findAccessibilityNodeInfosByText)(const char* instanceId, int64_t elementId, const char* text,
        int32_t requestId, ArkUI_AccessibilityElementInfoList* elementList);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。从指定节点查找已经聚焦的节点。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param focusType 焦点类型。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @param elementInfo 本次查询到的无障碍元素。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findFocusedAccessibilityNode)(const char* instanceId, int64_t elementId,
        ArkUI_AccessibilityFocusType focusType, int32_t requestId, ArkUI_AccessibilityElementInfo* elementInfo);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。从指定节点查询指定方向的节点。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param direction 搜索方向。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @param elementInfo 本次查询到的无障碍元素。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*findNextFocusAccessibilityNode)(
        const char* instanceId, int64_t elementId, ArkUI_AccessibilityFocusMoveDirection direction,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementInfo);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。对指定节点执行指定的操作。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param action 需要执行的操作，比如聚焦、点击和长按等。
    * @param actionArguments 控制操作的参数。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*executeAccessibilityAction)(const char* instanceId, int64_t elementId,
        ArkUI_Accessibility_ActionType action, ArkUI_AccessibilityActionArguments *actionArguments, int32_t requestId);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。 清除当前获焦的节点。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*clearFocusedFocusAccessibilityNode)(const char* instanceId);
    /**
    * @brief 由接入方平台实现的回调函数，注册给系统侧调用。获取当前组件中（文本组件）光标位置。支持多实例场景。
    * @param instanceId 第三方框架的实例编码。
    * @param elementId 无障碍元素的唯一编号。
    * @param requestId 请求id，用于关联请求过程，方便问题定位。建议日志打印时附带输出该信息，方便定位。
    * @param index 光标的位置结果。
    * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
    *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
    */
    int32_t (*getAccessibilityNodeCursorPosition)(const char* instanceId, int64_t elementId,
        int32_t requestId, int32_t* index);
} ArkUI_AccessibilityProviderCallbacksWithInstance;
/**
 * @brief 为此ArkUI_AccessibilityProvider实例注册具有实例信息的回调。
 * @param instanceId 第三方框架的实例编码。
 * @param provider 表示指向ArkUI_AccessibilityProvider实例的指针。
 * @param callbacks 表示指向ArkUI_AccessibilityProviderCallbacksWithInstance实例的指针。
 * @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
 *         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
 * @since 15
 */
int32_t OH_ArkUI_AccessibilityProviderRegisterCallbackWithInstance(const char* instanceId,
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityProviderCallbacksWithInstance* callbacks);

/**
 * @brief 发送accessibility事件信息。
 * 
 * @param provider 表示指向ArkUI_AccessibilityProvider实例的指针。
 * @param eventInfo 表示指向Accessibility事件信息的指针。
 * @param callback 表示指向SendAccessibilityAsyncEvent回调。
 * @since 13
 */
void OH_ArkUI_SendAccessibilityAsyncEvent(
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityEventInfo* eventInfo,
    void (*callback)(int32_t errorCode));

/**
 * @brief 添加并获取ArkUI_AccessibilityElementInfo指针。
 * 
 * @param list 表示指向ArkUI_AccessibilityElementInfoList指针。
 * @return 返回表示指向ArkUI_AccessibilityElementInfo指针。
 * @since 13
 */
ArkUI_AccessibilityElementInfo* OH_ArkUI_AddAndGetAccessibilityElementInfo(
    ArkUI_AccessibilityElementInfoList* list);
/**
* @brief 为ArkUI_AccessibilityElementInfo设置componentId。
*
* @param elementInfo ArkUI_AccessibilityElementInfo指针。
* @param elementId 表示元素的ID。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetElementId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t elementId);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置parentId。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param parentId 表示parentId。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetParentId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t parentId);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置componentType。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param componentType 表示componentType。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetComponentType(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* componentType);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置组件内容。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param contents 表示组件内容。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetContents(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* contents);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置提示文本。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param hintText 表示提示文本。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetHintText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hintText);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置Accessibility文本。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityText 表示Accessibility文本。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityText);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置Accessibility描述信息。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityDescription 表示Accessibility描述信息。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityDescription(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityDescription);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置childCount和childNodeIds。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param childCount 表示孩子节点数量。
* @param childNodeIds 表示孩子节点id集合。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetChildNodeIds(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t childCount, int64_t* childNodeIds);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置operationActions。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param operationActions 表示operationActions。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetOperationActions(ArkUI_AccessibilityElementInfo* elementInfo,
    int32_t operationCount, ArkUI_AccessibleAction* operationActions);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置屏幕区域。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param screenRect 表示屏幕区域。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetScreenRect(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRect* screenRect);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置checkable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param checkable 表示checkable。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetCheckable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checkable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置checked。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param checked 表示checked。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetChecked(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checked);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否可获焦。
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param focusable 表示是否可获焦。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetFocusable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool focusable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否获焦。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isFocused 表示是否获焦。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isFocused);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否可见。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isVisible 表示是否可见。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetVisible(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isVisible);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityFocused。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityFocused 表示accessibilityFocused。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityFocused);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置selected。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param selected 表示selected。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, bool selected);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置clickable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param clickable 表示clickable。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool clickable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置longClickable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param longClickable 表示longClickable。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetLongClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool longClickable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isEnable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isEnable 表示是否允许。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetEnabled(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isEnabled);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isPassword。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isPassword 表示isPassword。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetIsPassword(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isPassword);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置scrollable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param scrollable 表示scrollable。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetScrollable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool scrollable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置editable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param editable 表示editable。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetEditable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool editable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isHint。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isHint 表示isHint。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetIsHint(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isHint);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置rangeInfo。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param rangeInfo 表示rangeInfo。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetRangeInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRangeInfo* rangeInfo);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置gridInfo。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param gridInfo 表示gridInfo。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetGridInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridInfo* gridInfo);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置gridItem。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param gridItem 表示gridItem。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetGridItemInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridItemInfo* gridItem);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置selectedTextStart。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param selectedTextStart 表示selectedTextStart。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetSelectedTextStart(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t selectedTextStart);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置selectedTextEnd。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param selectedTextEnd 表示selectedTextEnd。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetSelectedTextEnd(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t selectedTextEnd);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置currentItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param currentItemIndex 表示currentItemIndex。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetCurrentItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t currentItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置startItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param startItemIndex 表示startItemIndex。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetStartItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t startItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置endItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param endItemIndex 表示endItemIndex。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetEndItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t endItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置itemCount。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param itemCount 表示itemCount。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetItemCount(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t itemCount);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置offset。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param offset 表示相对于元素顶部坐标的滚动像素偏移。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityOffset(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t offset);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityGroup。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityGroup 表示accessibilityGroup。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityGroup(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityGroup);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityLevel。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityLevel 表示accessibilityLevel。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityLevel(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityLevel);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置zIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param zIndex 表示zIndex。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetZIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t zIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置opacity。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param opacity 表示opacity。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetAccessibilityOpacity(
    ArkUI_AccessibilityElementInfo* elementInfo, float opacity);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置backgroundColor。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param backgroundColor 表示backgroundColor。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetBackgroundColor(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundColor);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置backgroundImage。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param backgroundImage 表示backgroundImage。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetBackgroundImage(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundImage);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置blur。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param blur 表示blur。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetBlur(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* blur);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置hitTestBehavior。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param hitTestBehavior 表示hitTestBehavior。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityElementInfoSetHitTestBehavior(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hitTestBehavior);

/**
 * @brief 创建一个ArkUI_AccessibilityEventInfo对象。
 *
 * @return Returns ArkUI_AccessibilityEventInfo对象。
 * @since 13
 */
ArkUI_AccessibilityEventInfo* OH_ArkUI_CreateAccessibilityEventInfo(void);

/**
 * @brief 销毁ArkUI_AccessibilityEventInfo对象。
 *
 * @param eventInfo 需要被销毁的ArkUI_AccessibilityEventInfo对象。
 * @since 13
 */
void OH_ArkUI_DestoryAccessibilityEventInfo(ArkUI_AccessibilityEventInfo* eventInfo);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置事件类型。
*
* @param eventInfo 表示事件信息。
* @param eventType 表示事件类型。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityEventSetEventType(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityEventType eventType);
/**
* @brief 为ArkUI_AccessibilityEventInfo设置textAnnouncedForAccessibility。
*
* @param eventInfo 表示事件信息。
* @param textAnnouncedForAccessibility 表示textAnnouncedForAccessibility。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityEventSetTextAnnouncedForAccessibility(
    ArkUI_AccessibilityEventInfo* eventInfo,  const char* textAnnouncedForAccessibility);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置requestFocusId。
*
* @param eventInfo 表示事件信息。
* @param requestFocusId 表示请求焦点id。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityEventSetRequestFocusId(
    ArkUI_AccessibilityEventInfo* eventInfo,  int32_t requestFocusId);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置elementInfo。
*
* @param eventInfo 表示事件信息。
* @param elementInfo 表示ArkUI_AccessibilityElementInfo元素信息。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_AccessibilityEventSetElementInfo(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityElementInfo* elementInfo);

/**
* @brief 通过key从ArkUI_AccessibilityActionArguments获取值。
*
* @param arguments 表示ArkUI_AccessibilityActionArguments对象信息。
* @param key 表示key。
* @param value 表示value。
* @return 成功返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_SUCCESSFUL}。
*         参数错误返回 {@link ARKUI_ACCESSIBILITY_NATIVE_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_FindAccessibilityActionArgumentByKey(
    ArkUI_AccessibilityActionArguments* arguments, const char* key, char** value);
#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_ACCESSIBILITY_H
