/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 * @since 10
 */

/**
 * @file task.h
 *
 * @brief 声明任务的C接口。
 *
 * @library libffrt.z.so
 * @kit FunctionFlowRuntimeKit
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 10
 * @version 1.0
 */

#ifndef FFRT_API_C_TASK_H
#define FFRT_API_C_TASK_H

#include <stdint.h>
#include "type_def.h"

/**
 * @brief 初始化任务属性。
 *
 * @param attr 任务属性指针。
 * @return 初始化任务属性成功返回0，
           初始化任务属性失败返回-1。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_task_attr_init(ffrt_task_attr_t* attr);

/**
 * @brief 设置任务名字。
 *
 * @param attr 任务属性指针。
 * @param name 任务名字。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_set_name(ffrt_task_attr_t* attr, const char* name);

/**
 * @brief 获取任务名字。
 *
 * @param attr 任务属性指针。
 * @return 获取任务名字成功返回非空指针，
           获取任务名字失败返回空指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API const char* ffrt_task_attr_get_name(const ffrt_task_attr_t* attr);

/**
 * @brief 销毁任务属性。
 *
 * @param attr 任务属性指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_destroy(ffrt_task_attr_t* attr);

/**
 * @brief 设置任务QoS。
 *
 * @param attr 任务属性指针。
 * @param qos 任务QoS。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_set_qos(ffrt_task_attr_t* attr, ffrt_qos_t qos);

/**
 * @brief 获取任务QoS。
 *
 * @param attr 任务属性指针。
 * @return 返回任务的QoS，默认返回ffrt_qos_default。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_qos_t ffrt_task_attr_get_qos(const ffrt_task_attr_t* attr);

/**
 * @brief 设置任务延迟时间。
 *
 * @param attr 任务属性指针。
 * @param delay_us 任务延迟时间，单位微秒。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_set_delay(ffrt_task_attr_t* attr, uint64_t delay_us);

/**
 * @brief 获取任务延迟时间。
 *
 * @param attr 任务属性指针。
 * @return 返回任务的延迟时间。
 * @since 10
 * @version 1.0
 */
FFRT_C_API uint64_t ffrt_task_attr_get_delay(const ffrt_task_attr_t* attr);

/**
 * @brief 设置并行队列任务优先级。
 *
 * @param attr 任务属性指针。
 * @param priority 任务优先级。
 * @since 12
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_set_queue_priority(ffrt_task_attr_t* attr, ffrt_queue_priority_t priority);

/**
 * @brief 获取并行队列任务优先级。
 *
 * @param attr 任务属性指针。
 * @return 返回任务优先级。
 * @since 12
 * @version 1.0
 */
FFRT_C_API ffrt_queue_priority_t ffrt_task_attr_get_queue_priority(const ffrt_task_attr_t* attr);

/**
 * @brief 设置任务栈大小。
 *
 * @param attr 任务属性指针。
 * @param size 任务栈大小，单位是字节。
 * @since 12
 * @version 1.0
 */
FFRT_C_API void ffrt_task_attr_set_stack_size(ffrt_task_attr_t* attr, uint64_t size);

/**
 * @brief 获取任务栈大小。
 *
 * @param attr 任务属性指针。
 * @return 返回任务栈大小，单位是字节。
 * @since 12
 * @version 1.0
 */
FFRT_C_API uint64_t ffrt_task_attr_get_stack_size(const ffrt_task_attr_t* attr);

/**
 * @brief 更新任务QoS。
 *
 * @param qos 当前任务待更新的QoS。
 * @return 更新任务QoS成功返回0，
           更新任务QoS失败返回-1。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_this_task_update_qos(ffrt_qos_t qos);

/**
 * @brief 获取任务QoS。
 *
 * @return 返回任务QoS。
 * @since 12
 * @version 1.0
 */
FFRT_C_API ffrt_qos_t ffrt_this_task_get_qos(void);

/**
 * @brief 获取任务id。
 *
 * @return 返回当前任务的id。
 * @since 10
 * @version 1.0
 */
FFRT_C_API uint64_t ffrt_this_task_get_id(void);

/**
 * @brief 申请函数执行结构的内存。
 *
 * @param kind 函数执行结构类型，支持通用和队列函数执行结构类型。
 * @return 申请函数执行结构成功返回非空指针，
           申请函数执行结构失败返回空指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void *ffrt_alloc_auto_managed_function_storage_base(ffrt_function_kind_t kind);

/**
 * @brief 提交任务调度执行。
 *
 * @param f 任务执行体封装的指针。
 * @param in_deps 输入依赖指针。
 * @param out_deps 输出依赖指针。
 * @param attr 任务属性。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_submit_base(ffrt_function_header_t* f, const ffrt_deps_t* in_deps, const ffrt_deps_t* out_deps,
    const ffrt_task_attr_t* attr);

/**
 * @brief 提交任务调度执行并返回任务句柄。
 *
 * @param f 任务执行体封装的指针。
 * @param in_deps 输入依赖指针。
 * @param out_deps 输出依赖指针。
 * @param attr 任务属性。
 * @return 提交任务成功返回非空任务句柄，
           提交任务失败返回空指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_task_handle_t ffrt_submit_h_base(ffrt_function_header_t* f, const ffrt_deps_t* in_deps,
    const ffrt_deps_t* out_deps, const ffrt_task_attr_t* attr);

/**
 * @brief 增加任务句柄的引用数。
 *
 * @param handle 任务句柄。
 * @return 返回任务句柄原始引用计数。
 * @since 12
 * @version 1.0
 */
FFRT_C_API uint32_t ffrt_task_handle_inc_ref(ffrt_task_handle_t handle);

/**
 * @brief 减少任务句柄的引用计数。
 *
 * @param handle 任务句柄
 * @return 返回任务句柄原始引用计数。
 * @since 12
 * @version 1.0
 */
FFRT_C_API uint32_t ffrt_task_handle_dec_ref(ffrt_task_handle_t handle);

/**
 * @brief 销毁任务句柄。
 *
 * @param handle 任务句柄。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_task_handle_destroy(ffrt_task_handle_t handle);

/**
 * @brief 等待依赖的任务完成，当前任务开始执行。
 *
 * @param deps 依赖的指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_wait_deps(const ffrt_deps_t* deps);

/**
 * @brief 等待之前所有提交任务完成，当前任务开始执行。
 *
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_wait(void);

#endif // FFRT_API_C_TASK_H
/** @} */
