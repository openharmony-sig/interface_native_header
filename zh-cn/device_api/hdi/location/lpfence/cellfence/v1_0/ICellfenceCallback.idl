/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellfence
 * @{
 *
 * @brief 为低功耗围栏服务提供基站围栏的API。
 *
 * 本模块接口提供添加基站围栏、删除基站围栏和获取基站围栏使用信息的功能。
 *
 * 应用场景：判断用户设备是否达到某个较大范围的位置区域，从而进行一些后续服务，如景区服务介绍等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ICellfenceCallback.idl
 *
 * @brief 定义基站围栏模块回调接口。
 *
 * 模块包路径：ohos.hdi.location.lpfence.cellfence.v1_0
 *
 * 引用：ohos.hdi.location.lpfence.cellfence.v1_0.CellfenceTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.cellfence.v1_0;

import ohos.hdi.location.lpfence.cellfence.v1_0.CellfenceTypes;

/**
 * @brief 定义基站围栏模块的回调函数
 *
 * 用户在开启基站围栏功能前，需要先注册该回调函数。当基站围栏状态发生变化时，会通过回调函数进行上报。
 * 详情可参考{@link ICellfenceInterface}。 
 *
 * @since 4.0
 */
[callback] interface ICellfenceCallback {
    /**
     * @brief 定义基站围栏状态变化通知的回调函数。
     *
     * 设备与基站围栏的状态关系发生变化时，会通过该回调函数进行上报。
     *
     * @param fences 上报基站围栏变化的状态。详见{@link CellfenceStatus}定义。
     *
     * @return 如果回调函数上报数据成功，则返回0。
     * @return 如果回调函数上报数据失败，则返回负值。
     *
     * @since 4.0
     */
    OnCellfenceChanged([in] struct CellfenceStatus fences);

    /**
     * @brief 定义基站围栏使用信息的回调函数。
     *
     * 获取基站围栏使用信息时，会通过该回调函数进行上报。
     *
     * @param size 基站围栏使用信息。详见{@link CellfenceSize}定义。
     *
     * @return 如果回调函数上报数据成功，则返回0。
     * @return 如果回调函数上报数据失败，则返回负值。
     *
     * @since 4.0
     */
    OnGetCellfenceSizeCb([in] struct CellfenceSize size);

    /**
     * @brief 定义低功耗围栏服务复位事件通知的回调函数。
     *
     * 低功耗围栏服务发生复位时会通过该回调函数进行事件上报。
     *
     * @return 如果回调函数调用成功，则返回0。
     * @return 如果回调函数调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnCellfenceReset();
}
/** @} */