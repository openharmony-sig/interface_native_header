/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的UI能力，如UI组件创建销毁、树节点操作，属性设置，事件监听等。
 *
 * @since 12
 */

/**
 * @file drawable_descriptor.h
 *
 * @brief 提供NativeDrawableDescriptor接口的类型定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @kit ArkUI
 * @since 12
 */

#ifndef ARKUI_NATIVE_DRAWABLE_DESCRIPTOR_H
#define ARKUI_NATIVE_DRAWABLE_DESCRIPTOR_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义 DrawableDescriptor 对象。
 *
 * @since 12
 */
typedef struct ArkUI_DrawableDescriptor ArkUI_DrawableDescriptor;

/**
 * @brief 使用Image Kit定义的native侧的OH_PixelmapNative对象。
 *
 * @since 12
 */
struct OH_PixelmapNative;

/**
 * @brief 定义OH_PixelmapNative对象指针类型。
 *
 * @since 12
*/
typedef struct OH_PixelmapNative* OH_PixelmapNativeHandle;

/**
 * @brief 使用 PixelMap 创建 DrawableDescriptor 对象。 
 *
 * @param pixelMap PixelMap 对象指针。
 * @return 返回 DrawableDescriptor 对象指针。
 * @since 12
*/
ArkUI_DrawableDescriptor* OH_ArkUI_DrawableDescriptor_CreateFromPixelMap(OH_PixelmapNativeHandle pixelMap);

/**
 * @brief 使用 PixelMap 图片数组创建DrawableDescriptor 对象。
 *
 * @param array PixelMap 图片数组对象指针。
 * @param size PixelMap 图片数组大小。
 * @return 返回 DrawableDescriptor 对象指针。
 * @since 12
*/
ArkUI_DrawableDescriptor* OH_ArkUI_DrawableDescriptor_CreateFromAnimatedPixelMap(
    OH_PixelmapNativeHandle* array, int32_t size);

/**
 * @brief 销毁 DrawableDescriptor 对象指针。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @since 12
*/
void OH_ArkUI_DrawableDescriptor_Dispose(ArkUI_DrawableDescriptor* drawableDescriptor);

/**
 * @brief 获取 PixelMap 图片对象指针。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @return PixelMap 对象指针。
 * @since 12
*/
OH_PixelmapNativeHandle OH_ArkUI_DrawableDescriptor_GetStaticPixelMap(ArkUI_DrawableDescriptor* drawableDescriptor);

/**
 * @brief 获取用于播放动画的 PixelMap 图片数组数据。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @return PixelMap 图片数组指针。
 * @since 12
*/
OH_PixelmapNativeHandle* OH_ArkUI_DrawableDescriptor_GetAnimatedPixelMapArray(
    ArkUI_DrawableDescriptor* drawableDescriptor);

/**
 * @brief 获取用于播放动画的 PixelMap 图片数组数据。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @return PixelMap 图片数组大小。
 * @since 12
*/
int32_t OH_ArkUI_DrawableDescriptor_GetAnimatedPixelMapArraySize(ArkUI_DrawableDescriptor* drawableDescriptor);

/**
 * @brief 设置 PixelMap 图片数组播放总时长。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @param duration 播放总时长，单位毫秒。
 * @since 12
*/
void OH_ArkUI_DrawableDescriptor_SetAnimationDuration(ArkUI_DrawableDescriptor* drawableDescriptor, int32_t duration);

/**
 * @brief 获取 PixelMap 图片数组播放总时长。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @return 播放总时长，单位毫秒。
 * @since 12
*/
int32_t OH_ArkUI_DrawableDescriptor_GetAnimationDuration(ArkUI_DrawableDescriptor* drawableDescriptor);

/**
 * @brief 设置 PixelMap 图片数组播放次数。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @param iterations 播放次数。
 * @since 12
*/
void OH_ArkUI_DrawableDescriptor_SetAnimationIteration(
    ArkUI_DrawableDescriptor* drawableDescriptor, int32_t iteration);

/**
 * @brief 获取 PixelMap 图片数组播放次数。
 *
 * @param drawableDescriptor DrawableDescriptor 对象指针。
 * @return 播放次数。
 * @since 12
*/
int32_t OH_ArkUI_DrawableDescriptor_GetAnimationIteration(ArkUI_DrawableDescriptor* drawableDescriptor);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_DRAWABLE_DESCRIPTOR_H
/** @} */
