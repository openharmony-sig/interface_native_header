/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceTrigger
 * @{
 *
 * @brief IntelligentVoiceTrigger模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上统一接口获取如下能力：触发器适配器加载卸载、智能语音触发器模型加载卸载、底层唤醒业务启动停止等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceTriggerCallback.idl
 *
 * @brief IntelligentVoiceTrigger模块触发器回调接口，包括识别事件上报等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.trigger.v1_0
 *
 * 引用：ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes
 *
 * @since 4.0
 * @version 1.0
 */


package ohos.hdi.intelligent_voice.trigger.v1_0;

import ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes;

 /**
 * @brief IntelligentVoiceTrigger模块向上层服务提供了智能语音触发器回调接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上智能语音触发器回调接口实现通知上层识别事件等功能。
 *
 * @since 4.0
 * @version 1.0
 */
[callback] interface IIntellVoiceTriggerCallback {
    /**
     * @brief 识别事件上报
     *
     * @param event 智能语音识别事件信息，信息包含识别状态、智能语音触发器模型类型、智能语音触发器模型句柄，具体参考{@link IntellVoiceRecognitionEvent}。
     * @param cookie 上层调用者标识。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    OnRecognitionHdiEvent([in] struct IntellVoiceRecognitionEvent event, [in] int cookie);
}
/** @} */