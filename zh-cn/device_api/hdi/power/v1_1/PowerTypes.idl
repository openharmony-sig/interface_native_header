/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup power
 * @{
 *
 * @brief 提供休眠/唤醒操作、订阅休眠/唤醒状态、运行锁管理的接口。
 *
 * 电源模块为电源服务提供的休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口对设备进行休眠/唤醒、订阅休眠/唤醒状态和管理运行锁。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file PowerTypes.idl
 *
 * @brief 电源相关的数据类型。
 *
 * 电源管理中使用的数据类型，包括命令参数、回调参数和系统状态。
 *
 * 模块包路径：ohos.hdi.power.v1_1
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.power.v1_1;

/**
 * @brief 枚举电源命令的参数。
 *
 * @since 3.1
 * @deprecated 从4.0版本起废弃。
 */
enum PowerHdfCmd {
    /** 订阅状态的命令参数 */
    CMD_REGISTER_CALLBCK = 0,
    /** 休眠的命令参数 */
    CMD_START_SUSPEND,
    /** 唤醒的命令参数 */
    CMD_STOP_SUSPEND,
    /** 强制休眠的命令参数 */
    CMD_FORCE_SUSPEND,
    /** 打开运行锁的命令参数 */
    CMD_SUSPEND_BLOCK,
    /** 关闭运行锁的命令参数 */
    CMD_SUSPEND_UNBLOCK,
    /** Dump的命令参数 */
    CMD_DUMP,
};

/**
 * @brief 枚举电源状态回调的参数。
 *
 * @since 3.1
 * @deprecated 从4.0版本起废弃
 */
enum PowerHdfCallbackCmd {
    /** 休眠回调的命令参数。 */
    CMD_ON_SUSPEND = 0,
    /** 唤醒回调的命令参数。 */
    CMD_ON_WAKEUP,
};

/**
 * @brief 枚举电源的状态。
 *
 * @since 3.1
 */
enum PowerHdfState {
    /** 唤醒状态。 */
    AWAKE = 0,
    /** 非活动状态。 */
    INACTIVE,
    /** 休眠状态。 */
    SLEEP,
};
/** @} */
