/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HiAppEvent
 * @{
 *
 * @brief HiAppEvent模块提供应用事件打点功能。
 *
 * 为应用程序提供事件打点功能，记录运行过程中上报的故障事件、统计事件、安全事件和用户行为事件。基于事件信息，开发者可以分析应用的运行状态。
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file hiappevent_param.h
 *
 * @brief 定义所有预定义事件的参数名称。
 *
 * 除了与特定应用关联的自定义事件之外，开发者还可以使用预定义事件进行打点。
 *
 * 示例代码:
 * <pre>
 *     ParamList list = OH_HiAppEvent_CreateParamList();
 *     OH_HiAppEvent_AddInt32Param(list, PARAM_USER_ID, 123);
 *     int res = OH_HiAppEvent_Write("user_domain", EVENT_USER_LOGIN, BEHAVIOR, list);
 *     OH_HiAppEvent_DestroyParamList(list);
 * </pre>
 *
 * @kit PerformanceAnalysisKit
 * @library libhiappevent_ndk.z.so
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 * @since 8
 * @version 1.0
 */

#ifndef HIVIEWDFX_HIAPPEVENT_PARAM_H
#define HIVIEWDFX_HIAPPEVENT_PARAM_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用户ID。
 *
 * @since 8
 * @version 1.0
 */
#define PARAM_USER_ID "user_id"

/**
 * @brief 分布式服务名称。
 *
 * @since 8
 * @version 1.0
 */
#define PARAM_DISTRIBUTED_SERVICE_NAME "ds_name"

/**
 * @brief 分布式服务实例ID。
 *
 * @since 8
 * @version 1.0
 */
#define PARAM_DISTRIBUTED_SERVICE_INSTANCE_ID "ds_instance_id"

#ifdef __cplusplus
}
#endif
/** @} */
#endif // HIVIEWDFX_HIAPPEVENT_PARAM_H