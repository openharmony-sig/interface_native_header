#ifndef VULKAN_OHOS_H_
#define VULKAN_OHOS_H_ 1

/*
** Copyright 2015-2022 The Khronos Group Inc.
**
** SPDX-License-Identifier: Apache-2.0
*/

/*
** This header is generated from the Khronos Vulkan XML API Registry.
**
*/


/**
 * @addtogroup Vulkan
 * @{
 *
 * @brief 提供OpenHarmony平台扩展的Vulkan能力，扩展了使用OHNativeWindow创建Vulkan Surface的能力，以及获取OH_NativeBuffer和OH_NativeBuffer属性的能力。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @since 10
 * @version 1.0
 */

/**
 * @file vulkan_ohos.h
 *
 * @brief 定义了OpenHarmony平台扩展的Vulkan接口。引用文件：<vulkan/vulkan.h>。
 *
 * @library libvulkan.so
 * @since 10
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief OpenHarmony平台Surface扩展宏定义。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_surface 1

/**
 * @brief 本地窗口。
 * @since 10
 * @version 1.0
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief OpenHarmony平台Surface扩展版本号。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_SPEC_VERSION      1

/**
 * @brief OpenHarmony平台Surface扩展名。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_EXTENSION_NAME    "VK_OHOS_surface"

/**
 * @brief 用于Vulkan Surface创建时使用到的VkFlags类型位掩码，预留的标志类型。
 * @since 10
 * @version 1.0
 */
typedef VkFlags VkSurfaceCreateFlagsOHOS;

/**
 * @brief 包含创建Vulkan Surface时必要的参数。
 * @since 10
 * @version 1.0
 */
typedef struct VkSurfaceCreateInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType             sType;
    /**
     * 下一级结构体指针。
     */
    const void*                 pNext;
    /**
     * 预留的标志类型参数。
     */
    VkSurfaceCreateFlagsOHOS    flags;
    /**
     * OHNativeWindow指针。
     */
    OHNativeWindow*             window;
} VkSurfaceCreateInfoOHOS;

/**
 * @brief 创建Vulkan Surface的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance Vulkan实例。
 * @param pCreateInfo 一个VkSurfaceCreateInfoOHOS结构体的指针，包含创建Vulkan Surface时必要的参数。
 * @param pAllocator 用户自定义内存分配的回调函数，如果不需要可以传入NULL，接口会使用默认的内存分配函数。
 * @param pSurface 出参，用于接收创建的Vulkan Surface，类型为VkSurfaceKHR。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkCreateSurfaceOHOS)(
    VkInstance                     instance,
    const VkSurfaceCreateInfoOHOS* pCreateInfo,
    const VkAllocationCallbacks*   pAllocator,
    VkSurfaceKHR*                  pSurface
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief 创建Vulkan Surface。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance Vulkan实例。
 * @param pCreateInfo 一个VkSurfaceCreateInfoOHOS结构体的指针，包含创建Vulkan Surface时必要的参数。
 * @param pAllocator 用户自定义内存分配的回调函数，如果不需要可以传入NULL，接口会使用默认的内存分配函数。
 * @param pSurface 出参，用于接收创建的Vulkan Surface，类型为VkSurfaceKHR。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_OUT_OF_HOST_MEMORY，表示分配VkSurfaceKHR内存失败。\n
 * 返回VK_ERROR_SURFACE_LOST_KHR，表示操作NativeWindow失败。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkCreateSurfaceOHOS(
    VkInstance                                  instance,
    const VkSurfaceCreateInfoOHOS*              pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkSurfaceKHR*                               pSurface);
#endif


/**
 * @brief OpenHarmony平台external_memory扩展宏定义。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_external_memory 1

/**
 * @brief OH_NativeBuffer结构体声明。
 * @since 10
 * @version 1.0
 */
struct OH_NativeBuffer;

/**
 * @brief OpenHarmony平台external_memory扩展版本号。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_SPEC_VERSION 1

/**
 * @brief OpenHarmony平台external_memory扩展名。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_EXTENSION_NAME "VK_OHOS_external_memory"

/**
 * @brief 提供OpenHarmony NativeBuffer用途的说明。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferUsageOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * NativeBuffer的用途说明。
     */
    uint64_t           OHOSNativeBufferUsage;
} VkNativeBufferUsageOHOS;

/**
 * @brief 包含了NaitveBuffer的属性。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferPropertiesOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * 占用的内存大小。
     */
    VkDeviceSize       allocationSize;
    /**
     * 内存类型。
     */
    uint32_t           memoryTypeBits;
} VkNativeBufferPropertiesOHOS;

/**
 * @brief 包含了NaitveBuffer的一些格式属性。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferFormatPropertiesOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType                  sType;
    /**
     * 下一级结构体指针。
     */
    void*                            pNext;
    /**
     * 格式说明。
     */
    VkFormat                         format;
    /**
     * 外部定义的格式标识符。
     */
    uint64_t                         externalFormat;
    /**
     * 描述了与externalFormat对应的能力。
     */
    VkFormatFeatureFlags             formatFeatures;
    /**
     * 表示一组VkComponentSwizzle。
     */
    VkComponentMapping               samplerYcbcrConversionComponents;
    /**
     * 色彩模型。
     */
    VkSamplerYcbcrModelConversion    suggestedYcbcrModel;
    /**
     * 色彩数值范围。
     */
    VkSamplerYcbcrRange              suggestedYcbcrRange;
    /**
     * X色度偏移。
     */
    VkChromaLocation                 suggestedXChromaOffset;
    /**
     * Y色度偏移。
     */
    VkChromaLocation                 suggestedYChromaOffset;
} VkNativeBufferFormatPropertiesOHOS;

/**
 * @brief 包含了OH_NativeBuffer结构体的指针。
 * @since 10
 * @version 1.0
 */
typedef struct VkImportNativeBufferInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType            sType;
    /**
     * 下一级结构体指针。
     */
    const void*                pNext;
    /**
     * OH_NativeBuffer结构体的指针。
     */
    struct OH_NativeBuffer*    buffer;
} VkImportNativeBufferInfoOHOS;

/**
 * @brief 用于从Vulkan内存中获取OH_NativeBuffer。
 * @since 10
 * @version 1.0
 */
typedef struct VkMemoryGetNativeBufferInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    const void*        pNext;
    /**
     * VkDeviceMemory对象。
     */
    VkDeviceMemory     memory;
} VkMemoryGetNativeBufferInfoOHOS;

/**
 * @brief 表示外部定义的格式标识符。
 * @since 10
 * @version 1.0
 */
typedef struct VkExternalFormatOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * 外部定义的格式标识符。
     */
    uint64_t           externalFormat;
} VkExternalFormatOHOS;

/**
 * @brief 获取OH_NativeBuffer属性的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param buffer OH_NativeBuffer结构体指针。
 * @param pProperties 用于接收OH_NativeBuffer属性的结构体。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetNativeBufferPropertiesOHOS)(
    VkDevice                      device,
    const struct OH_NativeBuffer* buffer,
    VkNativeBufferPropertiesOHOS* pProperties
);

/**
 * @brief 获取OH_NativeBuffer的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param pInfo VkMemoryGetNativeBufferInfoOHOS结构体对象。
 * @param pBuffer 用于接收获取到的OH_NativeBuffer。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetMemoryNativeBufferOHOS)(
    VkDevice                               device,
    const VkMemoryGetNativeBufferInfoOHOS* pInfo,
    struct OH_NativeBuffer**               pBuffer
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief 获取OH_NativeBuffer属性。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param buffer OH_NativeBuffer结构体指针。
 * @param pProperties 用于接收OH_NativeBuffer属性的结构体。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR，表示入参存在异常。\n
 * 返回VK_ERROR_OUT_OF_DEVICE_MEMORY，表示设备内存不足。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetNativeBufferPropertiesOHOS(
    VkDevice                                    device,
    const struct OH_NativeBuffer*               buffer,
    VkNativeBufferPropertiesOHOS*               pProperties);

/**
 * @brief 获取OH_NativeBuffer。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param pInfo VkMemoryGetNativeBufferInfoOHOS结构体对象。
 * @param pBuffer 用于接收获取到的OH_NativeBuffer。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_OUT_OF_HOST_MEMORY，表示pInfo入参异常，或获取的pBuffer异常。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetMemoryNativeBufferOHOS(
    VkDevice                                    device,
    const VkMemoryGetNativeBufferInfoOHOS*      pInfo,
    struct OH_NativeBuffer**                    pBuffer);

/**
 * @brief 根据给定的Vulkan设备、图像格式和图像使用标志, 返回适当的Gralloc(内存分配器)使用标志。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param format 图像格式。
 * @param imageUsage 图像使用标志。
 * @param grallocUsage 出参, 返回Gralloc(内存分配器)使用标志。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_INITIALIZATION_FAILED，表示入参异常。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetSwapchainGrallocUsageOHOS (
    VkDevice                                device,
    VkFormat                                format,
    VkImageUsageFlags                       imageUsage,
    uint64_t*                               grallocUsage);

/**
 * @brief 用于获取交换链图像的所有权, 并将外部信号的Fence导入到VkSemaphore对象和VkFence对象中。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param image 要获取的Vulkan图像。
 * @param nativeFenceFd 原生Fence的文件描述符。
 * @param semaphore 表示图像可用状态的Vulkan Semaphore(信号量)。
 * @param fence 用于在图像获取完成时进行同步的Vulkan Fence。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_OUT_OF_HOST_MEMORY，表示主机内存不足。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkAcquireImageOHOS (
    VkDevice        device,
    VkImage         image,
    int32_t         nativeFenceFd,
    VkSemaphore     semaphore,
    VkFence         fence);

/**
 * @brief 当前图像使用完毕后，通过该函数向系统硬件缓冲区发出释放信号, 以便其他组件可以访问该图像。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param queue Vulkan队列的句柄。
 * @param waitSemaphoreCount 等待Semaphore(信号量)的数量。
 * @param pWaitSemaphores 指向等待Semaphore(信号量)数组的指针。
 * @param images 要释放的Vulkan图像句柄。
 * @param pNativeFenceFd 指向Fence的文件描述符的指针。
 * @return 返回一个VkResult类型的错误码，具体返回类型如下：\n
 * 返回VK_SUCCESS，表示执行成功。\n
 * 返回VK_ERROR_DEVICE_LOST，表示Vulkan设备链接丢失。\n
 * 返回VK_ERROR_OUT_OF_HOST_MEMORY，表示主机内存不足。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkQueueSignalReleaseImageOHOS (
    VkQueue                 queue,
    uint32_t                waitSemaphoreCount,
    const VkSemaphore*      pWaitSemaphores,
    VkImage                 image,
    int32_t*                pNativeFenceFd);
#endif

#ifdef __cplusplus
}
#endif

#endif
