/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Provides unified, standard USB driver APIs to implement USB driver access.
 *
 * Using the APIs provided by the USB driver module, USB service developers can implement the following functions:
 * enabling/disabling a device, obtaining a device descriptor, obtaining a file descriptor, enabling/disabling an interface, 
 * reading/writing data in bulk mode, setting or obtaining device functions, and binding or unbinding a subscriber.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbdBulkCallback.idl
 *
 * @brief Provides bulk data reading/writing callbacks for the USB service.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the USB driver module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

/**
 * @brief Defines callbacks of the USB driver module.
 *
 * When the USB driver reads or writes data during isochronous bulk transfer, the USB service can call the respective callback 
 * for data processing.
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IUsbdBulkCallback {
    /**
     * @brief Callback for bulk data writing.
     *
     * @param status Completion status.
     * @param actLength Actual length of the transmitted data.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    OnBulkWriteCallback([in] int status, [in] int actLength);

    /**
     * @brief Callback for bulk data reading.
     *
     * @param status Completion status.
     * @param actLength Actual length of the received data.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    OnBulkReadCallback([in] int status, [in] int actLength);
}
/** @} */
