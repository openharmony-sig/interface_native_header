/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_AVSCREEN_CAPTURE_H
#define NATIVE_AVSCREEN_CAPTURE_H

#include <stdint.h>
#include <stdio.h>
#include "native_avscreen_capture_errors.h"
#include "native_avscreen_capture_base.h"
#include "native_window/external_window.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup AVScreenCapture
 * @{
 * 
 * @brief 调用本模块下的接口，应用可以完成屏幕录制的功能。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @since 10
 */

/**
 * @file native_avscreen_capture.h
 * 
 * @brief 声明用于构造屏幕录制对象的API。
 *
 * @library libnative_avscreen_capture.so
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @since 10
 */

/**
 * @brief 创建OH_AVScreenCapture。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @return 返回一个capture 指向OH_AVScreenCapture实例的指针。
 * @since 10
 * @version 1.0
 */
struct OH_AVScreenCapture *OH_AVScreenCapture_Create(void);

/**
 * @brief 初始化OH_AVScreenCapture相关参数，包括下发的音频麦克风采样相关参数（可选），音频内录采样相关参数，视频分辨率相关参数。
 *
 * 录屏存文件场景，应用需要保证视频编码参数、视频采样参数、音频编码参数、音频内录采样参数均合法，音频麦克风采样参数合法（可选）。
 * 录屏出码流场景，应用需要保证音频内录采样参数、视频采样参数至少一个合法，音频麦克风采样参数合法（可选）。
 * 由于结构体变量在初始化时不会对成员进行初始化，应用必须根据使用场景正确设置各项参数。建议应用先将
 * OH_AVScreenCaptureConfig结构体变量的所有内存字节均设置为0，然后再根据录屏场景设置合法参数。
 * 音频采样参数结构体{@link OH_AudioCaptureInfo}，若audioSampleRate和audioChannels同时为0，
 * 则录屏实例OH_AVScreenCapture将忽略该类型的音频参数，且不采集该类型的音频数据。
 * 视频采样参数结构体{@link OH_VideoCaptureInfo}，若videoFrameWidth和videoFrameHeight同时为0，
 * 则录屏实例OH_AVScreenCapture将忽略对应视频参数，且不采集屏幕数据。
 *
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param config 录屏初始化相关参数。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，初始化配置失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_Init(struct OH_AVScreenCapture *capture,
    OH_AVScreenCaptureConfig config);

/**
 * @brief 启动录屏，调用此接口，可采集编码后的码流。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置隐私权限启用失败或启动录屏失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenCapture(struct OH_AVScreenCapture *capture);

/**
 * @brief 使用Surface模式录屏。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param window 指向OHNativeWindow实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数window为空指针或window指向的windowSurface为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置隐私权限启用失败或启动ScreenCaptureWithSurface失败。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenCaptureWithSurface(struct OH_AVScreenCapture *capture,
    OHNativeWindow* window);

/**
 * @brief 结束录屏，与OH_AVScreenCapture_StartScreenCapture配合使用。调用后针对调用该接口的应用会停止录屏或屏幕共享，释放麦克风。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，结束录屏失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StopScreenCapture(struct OH_AVScreenCapture *capture);

/**
 * @brief 启动录屏，调用此接口，可将录屏文件保存。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置隐私权限启用失败或启用屏幕录制失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenRecording(struct OH_AVScreenCapture *capture);

/**
 * @brief 停止录屏，与OH_AVScreenCapture_StartScreenRecording配合使用。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，停止屏幕录制失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StopScreenRecording(struct OH_AVScreenCapture *capture);

/**
 * @brief 获取音频buffer，应用在调用时，需要对audiobuffer分配对应结构体大小的内存，否则会影响音频buffer的获取。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param audiobuffer 保存音频buffer的结构体，通过该结构体获取到音频buffer以及buffer的时间戳等信息。
 * @param type 音频buffer的类型，区分是麦克风录制的外部流还是系统内部播放音频的内录流，详情请参阅OH_AudioCaptureSourceType
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数audiobuffer为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_NO_MEMORY}内存不足，audiobuffer分配失败；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置隐私权限启用失败或获取音频buffer失败。
 * @since 10
 * @version 1.0
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_AcquireAudioBuffer(struct OH_AVScreenCapture *capture,
    OH_AudioBuffer **audiobuffer, OH_AudioCaptureSourceType type);

/**
 * @brief 获取视频buffer，应用在调用时，通过此接口来获取到视频的buffer以及时间戳等信息。
 * buffer使用完成后，调用OH_AVScreenCapture_ReleaseVideoBuffer接口进行视频buffer的释放。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param fence 用于同步的显示相关参数信息。
 * @param timestamp 视频帧的时间戳。
 * @param region 视频显示相关的坐标信息。
 * @return 执行成功返回OH_NativeBuffer对象，通过OH_NativeBuffer对象相关接口可以获取到视频buffer和分辨率等信息参数。
 * @since 10
 * @version 1.0
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 */
OH_NativeBuffer* OH_AVScreenCapture_AcquireVideoBuffer(struct OH_AVScreenCapture *capture,
    int32_t *fence, int64_t *timestamp, struct OH_Rect *region);

/**
 * @brief 根据音频类型释放buffer。当某一帧音频buffer使用完成后，调用此接口进行释放对应的音频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param type 音频buffer的类型，区分麦克风录制的外部流还是系统内部播放音频的内录流。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，不允许用于已设置过DataCallback或释放音频buffer失败。
 * @since 10
 * @version 1.0
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseAudioBuffer(struct OH_AVScreenCapture *capture,
    OH_AudioCaptureSourceType type);

/**
 * @brief 根据视频类型释放buffer。当某一帧视频buffer使用完成后，调用此接口进行释放对应的视频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，不允许用于已设置过DataCallback或释放视频buffer失败。
 * @since 10
 * @version 1.0
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseVideoBuffer(struct OH_AVScreenCapture *capture);

/**
 * @brief 设置监听接口，通过设置监听，可以监听到调用过程中的错误信息，以及是否有可用的视频buffer和音频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback OH_AVScreenCaptureCallback的结构体，保存相关回调函数指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数callback为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置监听接口失败。
 * @since 10
 * @version 1.0
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_SetErrorCallback} {@link OH_AVScreenCapture_SetDataCallback}
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetCallback(struct OH_AVScreenCapture *capture,
    struct OH_AVScreenCaptureCallback callback);

/**
 * @brief 释放创建的OH_AVScreenCapture实例，对应OH_AVScreenCapture_Create。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，OH_AVScreenCapture实例释放失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_Release(struct OH_AVScreenCapture *capture);

/**
 * @brief 设置麦克风开关。当isMicrophone为true时，则打开麦克风，通过调用OH_AVScreenCapture_StartScreenCapture和
 * OH_AVScreenCapture_AcquireAudioBuffer可以正常获取到音频的麦克风原始PCM数据；isMicrophone为false时，获取到的音频数据为无声数据。 默认麦克风开关为开启。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param isMicrophone 麦克风开关参数。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置麦克风开关失败。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetMicrophoneEnabled(struct OH_AVScreenCapture *capture,
    bool isMicrophone);

/**
 * @brief 设置状态变更处理回调方法，在开始录制前调用。
 *
 * 调用该方法设置状态变更处理回调方法，当OH_AVScreenCapture实例发生状态变更时，该状态变更处理回调方法将会被调用。
 * 调用该设置方法成功后，在启动录屏时将通过隐私弹窗方式征求用户同意：
 * 1. 如果用户同意则开始启动录屏流程，在启动录屏成功后，通过该状态处理回调方法上报
 * {@Link OH_SCREEN_CAPTURE_STATE_STARTED}状态，告知应用启动录屏成功，并在屏幕显示录屏通知。
 * 如果启动录屏失败，则通过该状态处理回调方法上报失败状态信息（如，若麦克风不可用则上报
 * {@link OH_SCREEN_CAPTURE_STATE_MIC_UNAVAILABLE}状态），或通过错误处理回调方法
 * {@link OH_AVScreenCapture_OnError}上报错误信息。
 * 2. 如果用户拒绝，则终止启动录屏，通过该状态处理回调方法上报
 * {@link OH_SCREEN_CAPTURE_STATE_CANCELED}状态，告知应用用户拒绝启动录屏，启动录屏失败。
 *
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback 指向状态处理回调方法实例的指针{@link OH_AVScreenCapture_OnStateChange}。
 * @param userData 指向应用提供的自定义数据的指针，在状态处理回调方法被调用时作为入参回传。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数callback为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置StateCallback失败。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetStateCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnStateChange callback, void *userData);

/**
 * @brief 设置数据处理回调方法，在开始录制前调用。
 *
 * 调用该方法设置数据处理回调方法，当OH_AVScreenCapture操作期间有音频或视频数据缓存区可用时，将调用该数据处理回调方法。
 * 应用需要在该数据处理回调方法中根据数据类型完成处理麦克风音频、内录音频、视频数据，当该数据处理回调方法返回后数据缓存区将不再有效。
 * 调用该方法成功后：
 * 1. 当OH_AVScreenCapture操作期间有音视频缓存区可用时，将不再调用通过
 * {@link OH_AVScreenCapture_SetCallback}设置的数据回调方法
 * {@link OH_AVScreenCaptureOnAudioBufferAvailable}和
 * {@link OH_AVScreenCaptureOnVideoBufferAvailable}。
 * 2. 不允许应用调用如下4个方法{@link OH_AVScreenCapture_AcquireAudioBuffer}、
 * {@link OH_AVScreenCapture_ReleaseAudioBuffer}、
 * {@link OH_AVScreenCapture_AcquireVideoBuffer}和
 * {@link OH_AVScreenCapture_ReleaseVideoBuffer}，直接返回失败。
 *
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback 指向数据处理回调方法实例的指针{@link OH_AVScreenCapture_OnBufferAvailable}。
 * @param userData 指向应用提供的自定义数据的指针，在数据处理回调方法被调用时作为入参回传。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数callback为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置DataCallback失败。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetDataCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnBufferAvailable callback, void *userData);

/**
 * @brief 设置错误处理回调方法，在开始录制前调用。
 *
 * 调用该方法设置错误处理回调方法，当OH_AVScreenCapture实例发生错误时，该错误处理回调方法将会被调用。
 * 调用该设置方法成功后：当OH_AVScreenCapture实例发生错误时，将不再调用通过{@link OH_AVScreenCapture_SetCallback}
 * 设置的错误处理回调方法{@link OH_AVScreenCaptureOnError}。
 *
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback 指向错误处理回调方法实例的指针{@link OH_AVScreenCapture_OnError}。
 * @param userData 指向应用提供的自定义数据的指针，在错误处理回调方法被调用时作为入参回传。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数callback为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置ErrorCallback失败。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetErrorCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnError callback, void *userData);

/**
 * @brief 设置录屏屏幕数据旋转。
 *
 * 调用该方法可以设置录屏屏幕数据是否旋转，当canvasRotation为true时，打开录屏屏幕数据旋转功能，录制的屏幕数据保持正向。
 * 默认为false。
 *
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param canvasRotation 指示屏幕数据旋转参数。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置录屏屏幕数据旋转失败。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetCanvasRotation(struct OH_AVScreenCapture *capture,
    bool canvasRotation);

/**
 * @brief 创建ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @return 执行成功返回OH_AVScreenCapture_ContentFilter实例，否则返回空指针。
 * @since 12
 * @version 1.0
 */
struct OH_AVScreenCapture_ContentFilter *OH_AVScreenCapture_CreateContentFilter(void);

/**
 * @brief 释放ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数filter为空指针。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseContentFilter(struct OH_AVScreenCapture_ContentFilter *filter);

/**
 * @brief 向ContentFilter实例添加可被过滤的声音类型。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @param content OH_AVScreenCaptureFilterableAudioContent实例。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数filter为空指针或输入参数content无效。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ContentFilter_AddAudioContent(
    struct OH_AVScreenCapture_ContentFilter *filter, OH_AVScreenCaptureFilterableAudioContent content);

/**
 * @brief 向OH_AVScreenCapture实例设置内容过滤器ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数filter为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_UNSUPPORT}操作不支持，对于流，在启动时应该调用AudioCapturer接口生效；\n
 *         对于capture文件，在启动时应该调用Recorder接口生效。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ExcludeContent(struct OH_AVScreenCapture *capture,
    struct OH_AVScreenCapture_ContentFilter *filter);

/**
 * @brief 向ContentFilter实例添加可被过滤的窗口ID列表
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @param windowIDs 指向窗口ID的指针。
 * @param windowCount 窗口ID列表的长度。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK，否则返回具体错误码，请参阅OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ContentFilter_AddWindowContent(
    struct OH_AVScreenCapture_ContentFilter *filter, int32_t *windowIDs, int32_t windowCount);

/**
 * @brief 调整屏幕的分辨率
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param width 录屏屏幕的宽度。
 * @param height 录屏屏幕的高度。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ResizeCanvas(struct OH_AVScreenCapture *capture,
    int32_t width, int32_t height);

/**
 * @brief 录屏时豁免隐私窗口
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param windowIDs 向隐私窗口ID的指针。
 * @param windowCount 隐私窗口ID列表的长度。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SkipPrivacyMode(struct OH_AVScreenCapture *capture,
    int32_t *windowIDs, int32_t windowCount);

/**
 * @brief 设置录屏时的最大帧率
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param frameRate 录屏的最大帧率。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或者输入参数frameRate不支持；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作。
 * @since 14
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetMaxVideoFrameRate(struct OH_AVScreenCapture *capture,
    int32_t frameRate);

/**
 * @brief 设置光标显示开关
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param showCursor 光标显示参数。
 * @return 函数结果代码：   
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_OPERATE_NOT_PERMIT}不允许操作，设置光标失败。
 * @since 15
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ShowCursor(struct OH_AVScreenCapture *capture,
    bool showCursor);

/**
 * @brief 设置获取录屏屏幕Id的回调。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture OH_AVScreenCapture实例的指针。
 * @param callback 指向录屏屏幕Id回调方法实例的指针, see {@link OH_AVScreenCapture_OnDisplaySelected}。
 * @param userData 指向应用提供的自定义数据的指针，在状态处理回调方法被调用时作为入参回传。
 * @return 函数结果代码：
 *         {@link AV_SCREEN_CAPTURE_ERR_OK}如果执行成功；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_VAL}输入参数capture为空指针或输入参数callback为空指针；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link AV_SCREEN_CAPTURE_ERR_INVALID_STATE}回调必须在start方法前调用。 \n
 * @since 15
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetDisplayCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnDisplaySelected callback, void *userData);
#ifdef __cplusplus
}
#endif

/** @} */ 

#endif // NATIVE_AVSCREEN_CAPTURE_H