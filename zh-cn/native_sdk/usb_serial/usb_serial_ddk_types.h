/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef USB_SERIAL_DDK_TYPES_H
#define USB_SERIAL_DDK_TYPES_H

/**
 * @addtogroup SerialDdk
 * @{
 *
 * @brief 提供USB Serial DDK接口，包括枚举类型和USB Serial DDK API使用的数据结构。
 * 工业用途及一些老旧设备会使用到串口通信，如：发卡机、身份证读卡器等。通过构建USB Serial DDK，
 * 支持外设扩展驱动基于USB Serial DDK开发非标外设扩展驱动。
 *
 * @syscap SystemCapability.Driver.SERIAL.Extension
 * @since 16
 */

/**
 * @file usb_serial_ddk_types.h
 *
 * @brief 提供USB SERIAL DDK中的枚举变量、结构体定义与宏定义。
 *
 * @kit DriverDevelopmentKit
 * @library libusb_serial.z.so
 * @syscap SystemCapability.Driver.SERIAL.Extension
 * 引用文件：<serial/usb_serial_ddk_api.h>
 * @since 16
 */

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @brief USB串口设备数据结构（不透明）。
 *
 * @since 16
 */
typedef struct UsbSerial_DeviceHandle UsbSerial_DeviceHandle;

/**
 * @brief 定义USB SERIAL DDK使用的返回码。
 *
 * @since 16
 */
typedef enum {
    /** 权限被拒绝。 */
    USB_SERIAL_DDK_NO_PERM = 201,
    /** 无效参数。 */
    USB_SERIAL_DDK_INVALID_PARAMETER = 401,
    /** 操作成功。 */
    USB_SERIAL_DDK_SUCCESS = 31600000,
    /**  无效操作。 */
    USB_SERIAL_DDK_INVALID_OPERATION = 31600001,
    /** 初始化失败。 */
    USB_SERIAL_DDK_INIT_ERROR = 31600002,
    /** 服务错误。 */
    USB_SERIAL_DDK_SERVICE_ERROR = 31600003,
    /** 内存相关错误，例如内存不足、内存数据复制失败或内存应用程序故障。 */
    USB_SERIAL_DDK_MEMORY_ERROR = 31600004,
    /** I/O 错误。 */
    USB_SERIAL_DDK_IO_ERROR = 31600005,
    /** 未找到设备。 */
    USB_SERIAL_DDK_DEVICE_NOT_FOUND = 31600006,
} UsbSerial_DdkRetCode;

/**
 * @brief 定义USB SERIAL DDK使用的USB串口参数.
 *
 * @since 16
 */
typedef struct UsbSerial_Params {
    /** 波特率。 */
    uint32_t baudRate;
    /** 数据传输位数。 */
    uint8_t nDataBits;
    /** 数据停止位数。 */
    uint8_t nStopBits;
    /** 校验参数设置。 */
    uint8_t parity;
} __attribute__((aligned(8))) UsbSerial_Params;

/**
 * @brief 定义USB SERIAL DDK中的流量控制。
 *
 * @since 16
 */
typedef enum {
    /** 无流量控制。 */
    USB_SERIAL_NO_FLOW_CONTROL = 0,
    /** 软件流控。 */
    USB_SERIAL_SOFTWARE_FLOW_CONTROL = 1,
    /** 硬件流控。 */
    USB_SERIAL_HARDWARE_FLOW_CONTROL = 2,
} UsbSerial_FlowControl;

/**
 * @brief 定义USB SERIAL DDK使用的校验参数枚举。
 *
 * @since 16
 */
typedef enum {
    /** 无校验。 */
    USB_SERIAL_PARITY_NONE = 0,
    /** 奇校验。 */
    USB_SERIAL_PARITY_ODD = 1,
    /** 偶校验。 */
    USB_SERIAL_PARITY_EVEN = 2,
} UsbSerial_Parity;
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // USB_DDK_TYPES_H