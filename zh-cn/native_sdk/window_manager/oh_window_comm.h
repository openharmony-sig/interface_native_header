/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WindowManager_NativeModule
 * @{
 *
 *
 * @brief 提供应用窗口的管理能力。
 *
 * @since 12
 */

/**
 * @file oh_window_comm.h
 *
 * @brief 提供窗口的公共枚举、公共定义等。
 *
 * @syscap SystemCapability.Window.SessionManager
 * @include window_manager/oh_window_comm.h
 * @library libnative_window_manager.so
 * @since 12
 */
#ifndef OH_WINDOW_COMM_H
#define OH_WINDOW_COMM_H

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义原生像素图片信息。
 *
 * @since 15
 */
typedef struct OH_PixelmapNative;

/**
 * @brief 窗口管理接口返回状态码枚举。
 *
 * @since 12
 */
typedef enum WindowManager_ErrorCode {
    /** 成功。 */
    OK = 0,
    /**
     * 无权限。
     *
     * @since 15
     */
    WINDOW_MANAGER_ERRORCODE_NO_PERMISSION = 201,
    /**
     * 非法参数。
     *
     * @since 15
     */
    WINDOW_MANAGER_ERRORCODE_INVALID_PARAM = 401,
    /**
     * 设备不支持。
     *
     * @since 15
     */
    WINDOW_MANAGER_ERRORCODE_DEVICE_NOT_SUPPORTED = 801,
    /** 非法窗口ID。 */
    INVAILD_WINDOW_ID = 1000,
    /** 服务异常。 */
    SERVICE_ERROR = 2000,
    /**
     * 窗口状态异常。
     *
     * @since 15
     */
    WINDOW_MANAGER_ERRORCODE_STATE_ABNORMAL = 1300002,
    /**
     * 窗口管理器服务异常。
     *
     * @since 15
     */
    WINDOW_MANAGER_ERRORCODE_SYSTEM_ABNORMAL = 1300003,
} WindowManager_ErrorCode;

/**
 * @brief 避让区域枚举类型。
 *
 * @since 15
 */
typedef enum {
    /** 系统避让区域。 */
    WINDOW_MANAGER_AVOID_AREA_TYPE_SYSTEM = 0,
    /** 刘海屏避让。 */
    WINDOW_MANAGER_AVOID_AREA_TYPE_CUTOUT = 1,
    /** 系统手势区域。 */
    WINDOW_MANAGER_AVOID_AREA_TYPE_SYSTEM_GESTURE = 2,
    /** 键盘区域。 */
    WINDOW_MANAGER_AVOID_AREA_TYPE_KEYBOARD = 3,
    /** 导航条区域。 */
    WINDOW_MANAGER_AVOID_AREA_TYPE_NAVIGATION_INDICATOR = 4,
} WindowManager_AvoidAreaType;

/**
 * @brief 窗口类型。
 *
 * @since 15
 */
typedef enum {
    /** 子窗口。 */
    WINDOW_MANAGER_WINDOW_TYPE_APP = 0,
    /** 主窗口。 */
    WINDOW_MANAGER_WINDOW_TYPE_MAIN = 1,
    /** 悬浮窗口。 */
    WINDOW_MANAGER_WINDOW_TYPE_FLOAT = 8,
    /** 模态窗口。 */
    WINDOW_MANAGER_WINDOW_TYPE_DIALOG = 16,
} WindowManager_WindowType;

/**
 * @brief 定义窗口矩形结构体，包含窗口位置和宽高信息。
 *
 * @since 15
 */
typedef struct {
    /** 窗口的x轴，单位为px，该参数为整数。 */
    int32_t posX;
    /** 窗口的y轴，单位为px，该参数为整数。 */
    int32_t posY;
    /** 窗口的宽度，单位为px，该参数为整数。 */
    uint32_t width;
    /** 窗户的高度，单位为px，该参数为整数。 */
    uint32_t height;
} WindowManager_Rect;

/**
 * @brief 窗口属性。
 *
 * @since 15
*/
typedef struct {
    /** 窗口的位置和尺寸。 */
    WindowManager_Rect windowRect;
    /** 窗口内可绘制区域的尺寸。 */
    WindowManager_Rect drawableRect;
    /** 窗口类型。 */
    WindowManager_WindowType type;
    /** 窗口是否全屏模式。默认值为false。true表示窗口是全屏模式，false表示窗口是非全屏模式。 */
    bool isFullScreen;
    /** 窗口布局是否沉浸式。默认值为false。true表示窗口布局是沉浸式，false表示窗口布局是非沉浸式。 */
    bool isLayoutFullScreen;
    /** 窗口是否能获取焦点。默认值为true。true表示窗口能获取焦点，false表示窗口不能获取焦点。 */
    bool focusable;
    /** 窗口是否可触。默认值为true。true表示窗口可触，false表示窗口不可触。 */
    bool touchable;
    /** 窗口亮度值。该参数为浮点数，取值范围为[0.0, 1.0]或-1.0。1.0表示最亮，-1.0表示默认亮度。 */
    float brightness;
    /** 是否打开屏幕常亮。默认值为false。true表示屏幕常亮，false表示屏幕不常亮。 */
    bool isKeepScreenOn;
    /** 窗口是否打开隐私模式。默认值为false。true表示窗口打开隐私模式，false表示窗口关闭隐私模式。 */
    bool isPrivacyMode;
    /** 窗口是否透明。默认值为false。true表示窗口透明，false表示窗口非透明。 */
    bool isTransparent;
    /** 窗口id。默认值为0，该参数为整数。 */
    uint32_t id;
    /** 窗口所在屏幕的id，默认返回主屏幕id，该参数为整数 */
    uint32_t displayId;
} WindowManager_WindowProperties;

/**
 * @brief 定义避让区域结构体。
 *
 * @since 15
 */
typedef struct {
    /** 避让区域的顶部矩形。 */
    WindowManager_Rect topRect;
    /** 避让区域的左侧矩形。 */
    WindowManager_Rect leftRect;
    /** 避让区域的右侧矩形。 */
    WindowManager_Rect rightRect;
    /** 避让区域的底部矩形。 */
    WindowManager_Rect bottomRect;
} WindowManager_AvoidArea;

#ifdef __cplusplus
}
#endif

#endif // OH_WINDOW_COMM_H
/** @} */