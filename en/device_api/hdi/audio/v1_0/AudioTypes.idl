/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Defines audio-related APIs, including data types and functions for loading drivers, accessing a driver adapter, and rendering and capturing audios.
 *
 *
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file AudioTypes.idl
 *
 * @brief Defines the data types used in API declarations for the audio module,
 *
 * including audio ports, adapter descriptors, device descriptors, scene descriptors, sampling attributes, and timestamp.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the audio APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.audio.v1_0;

/**
 * @brief Enumerates the audio port directions.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioPortDirection {
    PORT_OUT = 1,    /**< Audio output port. */
    PORT_IN = 2,     /**< Audio input port. */
    PORT_OUT_IN = 3, /**< Audio output and input port. */
};

/**
 * @brief Enumerates the pins on the audio port.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioPortPin {
    PIN_NONE = 0,               /**< Invalid port. */
    PIN_OUT_SPEAKER = 1,        /**< Speaker output. */
    PIN_OUT_HEADSET = 2,        /**< Wired headset output. */
    PIN_OUT_LINEOUT = 4,        /**< Line-out output. */
    PIN_OUT_HDMI = 8,           /**< High-Definition Multimedia Interface (HDMI) output. */
    PIN_IN_MIC = 134217729,     /**< Microphone input. */
    PIN_IN_HS_MIC = 134217730,  /**< Input via the wired headset with a microphone. */
    PIN_IN_LINEIN = 134217732,  /**< Line-in input. */
    PIN_IN_USB_EXT = 134217736, /**< USB audio adapter output. */
};

/**
 * @brief Enumerates the audio categories (scenes).
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioCategory {
    AUDIO_IN_MEDIA = 0,         /**< Media. */
    AUDIO_IN_COMMUNICATION = 1, /**< Communication. */
    AUDIO_IN_RINGTONE = 2,      /**< Ringtone. */
    AUDIO_IN_CALL = 3,          /**< Call. */
};

/**
 * @brief Enumerates the audio formats.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioFormat {
    AUDIO_FORMAT_PCM_8_BIT = 1,        /**< 8-bit pulse code modulation (PCM). */
    AUDIO_FORMAT_PCM_16_BIT = 2,       /**< 16-bit PCM. */
    AUDIO_FORMAT_PCM_24_BIT = 3,       /**< 24-bit PCM. */
    AUDIO_FORMAT_PCM_32_BIT = 4,       /**< 32-bit PCM. */
    AUDIO_FORMAT_AAC_MAIN = 16777217,  /**< Advanced Audio Coding (AAC) Main. */
    AUDIO_FORMAT_AAC_LC = 16777218,    /**< AAC Low Complexity (LC). */
    AUDIO_FORMAT_AAC_LD = 16777219,    /**< AAC Low Delay (LD). */
    AUDIO_FORMAT_AAC_ELD = 16777220,   /**< AAC Enhanced Low Delay (ELD). */
    AUDIO_FORMAT_AAC_HE_V1 = 16777221, /**< High Efficiency (HE) AAC V1. */
    AUDIO_FORMAT_AAC_HE_V2 = 16777222, /**< HE AAC V2. */
    AUDIO_FORMAT_G711A = 33554433,     /**< PCM G.711 a-law. */
    AUDIO_FORMAT_G711U = 33554434,     /**< PCM G.711 µ-law. */
    AUDIO_FORMAT_G726 = 33554435,      /**< PCM G.726. */
};

/**
 * @brief Enumerates the audio channel masks.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioChannelMask {
    AUDIO_CHANNEL_FRONT_LEFT = 1,  /**< Front left channel. */
    AUDIO_CHANNEL_FRONT_RIGHT = 2, /**< Front right channel. */
    AUDIO_CHANNEL_MONO = 1,        /**< Mono channel. */
    AUDIO_CHANNEL_STEREO = 3,      /**< Stereo channel, consisting of front left and front right channels. */
};

/**
 * @brief Enumerates the audio sampling rate masks.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioSampleRatesMask {
    AUDIO_SAMPLE_RATE_MASK_8000 = 1 << 0,          /**< 8 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_12000 = 1 << 1,         /**< 12 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_11025 = 1 << 2,         /**< 11.025 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_16000 = 1 << 3,         /**< 16 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_22050 = 1 << 4,         /**< 22.050 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_24000 = 1 << 5,         /**< 24 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_32000 = 1 << 6,         /**< 32 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_44100 = 1 << 7,         /**< 44.1 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_48000 = 1 << 8,         /**< 48 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_64000 = 1 << 9,         /**< 64 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_96000 = 1 << 10,        /**< 96 kHz sampling rate. */
    AUDIO_SAMPLE_RATE_MASK_INVALID = 4294967295,   /**< Invalid sampling rate. */
};

/**
 * @brief Enumerates the passthrough data transmission modes of the audio port.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioPortPassthroughMode {
    PORT_PASSTHROUGH_LPCM = 1,    /**< Linear PCM. */
    PORT_PASSTHROUGH_RAW = 2,     /**< HDMI passthrough. */
    PORT_PASSTHROUGH_HBR2LBR = 4, /**< Blu-ray next-generation audio output with reduced specifications. */
    PORT_PASSTHROUGH_AUTO = 8,    /**< Mode automatically matched based on the HDMI Extended Display Identification Data (EDID). */
};

/**
 * @brief Enumerates the audio device types.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioDeviceType {
    AUDIO_LINEOUT        = 1 << 0,  /**< Line-out device. */
    AUDIO_HEADPHONE      = 1 << 1,  /**< Headphone. */
    AUDIO_HEADSET        = 1 << 2,  /**< Headset. */
    AUDIO_USB_HEADSET    = 1 << 3,  /**< USB headset. */
    AUDIO_USB_HEADPHONE  = 1 << 4,  /**< USB headphone. */
    AUDIO_USBA_HEADSET   = 1 << 5,  /**< USB analog headset. */
    AUDIO_USBA_HEADPHONE = 1 << 6, /**< USB analog headphone. */
    AUDIO_PRIMARY_DEVICE = 1 << 7, /**< Primary audio device. */
    AUDIO_USB_DEVICE     = 1 << 8,  /**< USB audio device. */
    AUDIO_A2DP_DEVICE    = 1 << 9,  /**< Bluetooth audio device. */
    AUDIO_DEVICE_UNKNOWN,            /**< Unknown device. */
};

/**
 * @brief Enumerates the audio event types.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioEventType {
    AUDIO_DEVICE_ADD = 1,         /**< Audio device addition. */
    AUDIO_DEVICE_REMOVE = 2,      /**< Audio device removal. */
    AUDIO_LOAD_SUCCESS = 3,       /**< Audio adapter loading. */
    AUDIO_LOAD_FAILURE = 4,       /**< Failure in loading an audio adapter. */
    AUDIO_UNLOAD = 5,             /**< Audio adapter unloading. */
    AUDIO_SERVICE_VALID = 7,      /**< Audio service available. */
    AUDIO_SERVICE_INVALID = 8,    /**< Audio service unavailable. */
    AUDIO_CAPTURE_THRESHOLD = 9,  /**< Capture threshold reporting. */
    AUDIO_EVENT_UNKNOWN = 10,      /**< Unknown event. */
};

/**
 * @brief Enumerates the keys of extended audio parameters.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioExtParamKey {
    AUDIO_EXT_PARAM_KEY_NONE = 0,        /**< Distributed audio – invalid event. */
    AUDIO_EXT_PARAM_KEY_VOLUME = 1,      /**< Distributed audio – volume event. */
    AUDIO_EXT_PARAM_KEY_FOCUS = 2,       /**< Distributed audio – focus event. */
    AUDIO_EXT_PARAM_KEY_BUTTON = 3,      /**< Distributed audio – media button event.       */
    AUDIO_EXT_PARAM_KEY_EFFECT = 4,      /**< Distributed audio – audio effect event. */
    AUDIO_EXT_PARAM_KEY_STATUS = 5,      /**< Distributed audio – device status event. */
    AUDIO_EXT_PARAM_KEY_LOWPOWER = 1000, /**< Low battery event. */
};

/**
 * @brief Enumerates the audio device statuses.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioDeviceStatus {
    unsigned int pnpStatus;  /**< Plug and Play (PnP) device status. For details, see {@link AudioDeviceType} and {@link AudioEventType}. */
};

/**
 * @brief Enumerates the raw audio sampling formats.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioSampleFormat {
    /* 8 bits */
    AUDIO_SAMPLE_FORMAT_S8 = 0,  /**< 8-bit signed interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_S8P = 1, /**< 8-bit signed non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U8 = 2,  /**< 8-bit unsigned interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U8P = 3, /**< 8-bit unsigned non-interleaved sampling. **/
    /* 16 bits */
    AUDIO_SAMPLE_FORMAT_S16 = 4,  /**< 16-bit signed interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_S16P = 5, /**< 16-bit signed non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U16 = 6,  /**< 16-bit unsigned interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U16P = 7, /**< 16-bit unsigned non-interleaved sampling. **/
    /* 24 bits */
    AUDIO_SAMPLE_FORMAT_S24 = 8,   /**< 24-bit signed interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_S24P = 9,  /**< 24-bit signed non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U24 = 10,  /**< 24-bit unsigned interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U24P = 11, /**< 24-bit unsigned non-interleaved sampling. **/
    /* 32 bits */
    AUDIO_SAMPLE_FORMAT_S32 = 12,  /**< 32-bit signed interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_S32P = 13, /**< 32-bit signed non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U32 = 14,  /**< 32-bit unsigned interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U32P = 15, /**< 32-bit unsigned non-interleaved sampling. **/
    /* 64 bits */
    AUDIO_SAMPLE_FORMAT_S64 = 16,  /**< 64-bit signed interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_S64P = 17, /**< 64-bit signed non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U64 = 18,  /**< 64-bit unsigned interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_U64P = 19, /**< 64-bit unsigned non-interleaved sampling. **/
    /* float double */
    AUDIO_SAMPLE_FORMAT_F32 = 20,  /**< 32-bit floating-point interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_F32P = 21, /**< 32-bit floating-point non-interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_F64 = 22,  /**< 64-bit floating-point interleaved sampling. **/
    AUDIO_SAMPLE_FORMAT_F64P = 23, /**< 64-bit floating-point non-interleaved sampling. **/
};

/**
 * @brief Enumerates the channel modes for audio rendering.
 *
 * @attention The following modes are set for rendering dual-channel audios. Others are not supported.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioChannelMode {
    AUDIO_CHANNEL_NORMAL = 0, /**< Normal mode. No processing is required. */
    AUDIO_CHANNEL_BOTH_LEFT,  /**< Two left channels. */
    AUDIO_CHANNEL_BOTH_RIGHT, /**< Two right channels. */
    AUDIO_CHANNEL_EXCHANGE,   /**< Data exchange between the left and right channels. The left channel takes the audio stream of the right channel, and the right channel takes that of the left channel. */
    AUDIO_CHANNEL_MIX,        /**< Mix of streams of the left and right channels. */
    AUDIO_CHANNEL_LEFT_MUTE,  /**< Left channel muted. The stream of the right channel is output. */
    AUDIO_CHANNEL_RIGHT_MUTE, /**< Right channel muted. The stream of the left channel is output. */
    AUDIO_CHANNEL_BOTH_MUTE,  /**< Both left and right channels muted. */
};

/**
 * @brief Enumerates the callback notification types of the audio data.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioDrainNotifyType {
    AUDIO_DRAIN_NORMAL_MODE, /**< A callback notification is sent when all data finishes playback. */
    The AUDIO_DRAIN_EARLY_MODE, /**< A callback notification is sent before all the data finishes playback to reserve time for a smooth track switch by the audio service. */
};

/**
 * @brief Enumerates the callback notification events.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioCallbackType {
    AUDIO_NONBLOCK_WRITE_COMPLETED, /**< The non-block write is complete. */
    AUDIO_DRAIN_COMPLETED,          /**< The draining is complete. For details, see {@link DrainBuffer}. */
    AUDIO_FLUSH_COMPLETED,           /**< The flush is complete. For details, see {@link Flush}. */
    AUDIO_RENDER_FULL,               /**< The render buffer is full. */
    AUDIO_ERROR_OCCUR,               /**< An error occurs. */
};

/**
 * @brief Enumerates the roles of the audio port.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioPortRole {
    AUDIO_PORT_UNASSIGNED_ROLE = 0, /**< Unspecified role. */
    AUDIO_PORT_SOURCE_ROLE = 1,     /**< Sender. */
    AUDIO_PORT_SINK_ROLE = 2,       /**< Receiver. */
};

/**
 * @brief Enumerates the audio port types.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioPortType {
    AUDIO_PORT_UNASSIGNED_TYPE = 0, /**< Unspecified type. */
    AUDIO_PORT_DEVICE_TYPE = 1,     /**< Device type. */
    AUDIO_PORT_MIX_TYPE = 2,        /**< Mix type. */
    AUDIO_PORT_SESSION_TYPE = 3,    /**< Session type. */
};

/**
 * @brief Enumerates the session types of the port.
 *
 * @since 3.2
 * @version 1.0
 */
enum AudioSessionType {
    AUDIO_OUTPUT_STAGE_SESSION = 0, /**< The session is bound to the specified output stream. */
    AUDIO_OUTPUT_MIX_SESSION,       /**< The session is bound to the specified audio track. */
    AUDIO_ALLOCATE_SESSION,         /**< A new session ID must be applied for. */
    AUDIO_INVALID_SESSION,          /**< Invalid session type. */
};

/**
 * @brief Describes the audio scene.
 *
 * @since 3.2
 * @version 1.0
 */
union SceneDesc {
    unsigned int id; /**< Audio scene ID. For details, see {@link AudioCategory}. */
};

/**
 * @brief Defines the audio port.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioPort {
    enum AudioPortDirection dir; /**< Audio port type. For details, see {@link AudioPortDirection}. */
    unsigned int portId;         /**< Audio port ID. */
    String portName;             /**< Audio port name. */
};

/**
 * @brief Defines the audio adapter descriptor.
 *
 * An audio adapter is a set of port drivers for an audio adapter, including the input and output ports.
 * One port corresponds to multiple pins, and each pin belongs to a physical component (such as a speaker or a wired headset).
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioAdapterDescriptor {
    String adapterName;       /**< Name of the audio adapter. */
    struct AudioPort[] ports; /**< List of ports supported by the audio adapter. For details, see {@link AudioPort}. */
};

/**
 * @brief Defines the audio device descriptor.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioDeviceDescriptor {
    unsigned int portId;   /**< Audio port ID. */
    enum AudioPortPin pins; /**< Pins of audio ports (input and output). For details, see {@link AudioPortPin}. */
    String desc;            /**< Audio device name, expressed in a string. */
};

/**
 * @brief Describes the audio scene.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioSceneDescriptor {
    union SceneDesc scene;             /**< Audio scene description. For details, see {@link SceneDesc}. */
    struct AudioDeviceDescriptor desc; /**< Audio device descriptor. For details, see {@link AudioDeviceDescriptor}. */
};

/**
 * @brief Defines the audio sampling attributes.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioSampleAttributes {
    enum AudioCategory type;       /**< Audio type. For details, see {@link AudioCategory}. */
    boolean interleaved;           /**< Interleaved flag of audio data. */
    enum AudioFormat format;       /**< Audio format. For details, see {@link AudioFormat}. */
    unsigned int sampleRate;      /**< Audio sampling rate. */
    unsigned int channelCount;     /**< Number of audio channels. For example, for the mono channel, the value is <b>1</b>, and for the stereo channel, the value is <b>2</b>. */
    unsigned int period;           /**< Audio sampling period, in Hz. */
    unsigned int frameSize;        /**< Frame size of the audio data. */
    boolean isBigEndian;           /**< Big-endian flag of the audio data. */
    boolean isSignedData;          /**< Signed or unsigned flag of the audio data. */
    unsigned int startThreshold;   /**< Audio rendering start threshold. */
    unsigned int stopThreshold;    /**< Audio rendering stop threshold. */
    unsigned int silenceThreshold; /**< Recording buffer threshold. */
    int streamId;                  /**< Rendering or recording flag. */
};

/**
 * @brief Defines the audio timestamp, which is a substitute for POSIX <b>timespec</b>.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioTimeStamp {
    long tvSec;  /**< Seconds. */
    long tvNSec; /**< Nanoseconds. */
};

/**
 * @brief Defines the sub-port capability.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioSubPortCapability {
    unsigned int portId;                /**< Sub-port ID. */
    String desc;                        /**< Sub-port name, expressed in a string. */
    enum AudioPortPassthroughMode mask; /**< Passthrough mode of data transmission. For details, see {@link AudioPortPassthroughMode}. */
};

/**
 * @brief Defines the audio port capability.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioPortCapability {
    unsigned int deviceType;                     /**< Device type (input or output). */
    unsigned int deviceId;                         /**< Device ID, which uniquely identifies a device. */
    boolean hardwareMode;                          /**< Support for device binding. */
    unsigned int formatNum;                       /**< Number of the supported audio formats. */
    enum AudioFormat[] formats;                    /**< Supported audio formats. For details, see {@link AudioFormat}. */
    unsigned int sampleRateMasks;                  /**< Supported audio sampling rates (8 kHz, 16 kHz, 32 kHz, and 48 kHz). */
    enum AudioChannelMask channelMasks;            /**< Audio channel masks of the device. For details, see {@link AudioChannelMask}. */
    unsigned int channelCount;                     /**< Maximum number of audio channels supported. */
    struct AudioSubPortCapability[] subPorts;      /**< List of supported sub-ports. For details, see {@link AudioSubPortCapability}. */
    enum AudioSampleFormat[] supportSampleFormats; /**< List of supported sampling formats. For details, see {@link AudioSampleFormat}. */
};

/**
 * @brief Describes a mmap buffer.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioMmapBufferDescriptor {
    byte[] memoryAddress;  /**< Pointer to the mmap buffer. */
    int memoryFd;          /**< File descriptor of the mmap buffer. */
    int totalBufferFrames; /**< Total buffer size, in frames. */
    int transferFrameSize; /**< Transfer size, in frames. */
    int isShareable;       /**< Whether the mmap buffer can be shared among processes. */
    unsigned int offset;   /**< File offset. */
    String filePath;       /**< Path of the mmap file. */
};

/**
 * @brief Defines the extended audio device information.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioDevExtInfo {
    unsigned int moduleId;  /**< ID of the module to which the audio stream is bound. */
    enum AudioPortPin type; /**< Pins of audio ports (input and output). For details, see {@link AudioPortPin}. */
    String desc;            /**< Address description.  */
};

/**
 * @brief Defines the extended audio track information.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioMixExtInfo {
    unsigned int moduleId; /**< ID of the module to which the stream belongs. */
    unsigned int streamId; /**< Render or Capture flag passed by the caller. */
};

/**
 * @brief Defines the extended session information.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioSessionExtInfo {
    enum AudioSessionType sessionType; /**< Audio session type. For details, see {@link AudioSessionType}. */
};

/**
 * @brief Defines the audio information.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioInfo {
    struct AudioDevExtInfo device;      /**< Device-specific information. For details, see {@link AudioDevExtInfo}. */
    struct AudioMixExtInfo mix;         /**< Audio track–specific information. For details, see {@link AudioMixExtInfo}. */
    struct AudioSessionExtInfo session; /**< Session-specific information. For details, see {@link AudioSessionExtInfo}. */
};

/**
 * @brief Defines the audio route node.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioRouteNode {
    int portId;     /**< Audio port ID. */
    enum AudioPortRole role; /**< Audio port role (sender or receiver). For details, see {@link AudioPortRole}. */
    enum AudioPortType type; /**< Audio port type, which can be the device type, mix type, or session type. For details, see {@link AudioPortType}. */
    struct AudioInfo ext;    /**< Audio information. For details, see {@link AudioInfo}. */
};

/**
 * @brief Defines the audio route information.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioRoute {
    struct AudioRouteNode[] sources; /**< List of senders. For details, see {@link AudioRouteNode}. */
    struct AudioRouteNode[] sinks;   /**< List of receivers. For details, see {@link AudioRouteNode}. */
};

/**
 * @brief Defines the audio event.
 *
 * @since 3.2
 * @version 1.0
 */
struct AudioEvent {
    unsigned int eventType;  /**< Event type. For details, see {@link enum AudioEventType}. */
    unsigned int deviceType; /**< Device type. For details, see {@link enum AudioDeviceType}. */
};
/** @} */
