/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiUsb
 * @{
 *
 * @brief 提供统一的USB驱动标准接口，实现USB驱动接入。
 *
 * 上层USB服务开发人员可以根据USB驱动模块提供的标准接口获取如下功能：打开/关闭设备，获取设备描述符，获取文件描述符，打开/关闭接口，批量读取/写入数据，
 * 设置/获取设备功能，绑定/解绑订阅者等。
 *
 * @since 5.0
 */

/**
 * @file IUsbInterface.idl
 *
 * @brief 声明标准的USB驱动接口函数。
 *
 * 模块包路径：ohos.hdi.usb.v1_1
 *
 * 引用：
 * - ohos.hdi.usb.v1_1.UsbTypes
 * - ohos.hdi.usb.v1_0.UsbTypes
 * - ohos.hdi.usb.v1_0.IUsbdSubscriber
 * - ohos.hdi.usb.v1_0.IUsbdBulkCallback
 * - ohos.hdi.usb.v1_0.IUsbInterface
 *
 * @since 5.0
 * @version 1.1
 */

package ohos.hdi.usb.v1_1;

import ohos.hdi.usb.v1_1.UsbTypes;
import ohos.hdi.usb.v1_0.UsbTypes;
import ohos.hdi.usb.v1_0.IUsbdSubscriber;
import ohos.hdi.usb.v1_0.IUsbdBulkCallback;
import ohos.hdi.usb.v1_0.IUsbInterface;

/**
  * @brief 定义USB驱动基本的操作功能。
 *
 * 上层USB服务调用相关功能接口，可以打开/关闭设备，获取设备描述符，批量读取/写入数据等。
 
 * @since 5.0
 * @version 1.0
 */
interface IUsbInterface extends ohos.hdi.usb.v1_0.IUsbInterface{

    /**
     * @brief 获取USB设备接口的激活信息。
     *
     * @param dev USB设备地址信息。
     * @param interfaceid USB设备接口ID。
     * @param unactivated USB设备接口激活状态。true表示未激活，false表示已激活。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     */
    GetInterfaceActiveStatus ([in] struct UsbDev dev, [in] unsigned char interfaceid, [out] boolean unactivated );

    /**
     * @brief 获取USB设备速率。
     *
     * @param dev USB设备地址信息。
     * @param speed USB设备速率。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     */
    GetDeviceSpeed ([in] struct UsbDev dev, [out] unsigned char speed );

    /**
     * @brief 获取文件描述符。
     *
     * @param dev USB设备地址信息。
     * @param fd USB USB设备文件描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    GetDeviceFileDescriptor([in] struct UsbDev dev, [out] FileDescriptor fd);

    /**
     * @brief 在USB设备指定端点方向为读取时，执行批量数据读取。
     *
     * @param dev USB设备地址信息。
     * @param pipe USB设备管道信息。
     * @param timeout 超时时间。
     * @param length 读取的最大字节长度。
     * @param data 读取的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    BulkTransferReadwithLength([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] int length, [out] unsigned char[] data);

    /**
     * @brief 清除端点的暂停状态。
     *
     * @param dev USB设备地址信息。
     * @param pipe USB设备管道信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    ClearHalt([in] struct UsbDev dev, [in] struct UsbPipe pipe);

    /**
     * @brief 在传输状态为读取并且控制端点是端点零时，对USB设备执行控制传输。
     *
     * @param dev USB设备地址信息。
     * @param ctrl USB设备控制数据。
     * @param data 写入的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    ControlTransferReadwithLength([in] struct UsbDev dev, [in] struct UsbCtrlTransferParams ctrl, [out] unsigned char[] data);

    /**
     * @brief 重置设备。
     *
     * @param dev USB设备地址信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    ResetDevice([in] struct UsbDev dev);

    /**
     * @brief 获取配件信息。
     *
     * @param accessoryInfo 表示配件信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    GetAccessoryInfo([out] String[] accessoryInfo);

    /**
     * @brief 打开配件描述符。
     *
     * @param fd 配件的文件描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    OpenAccessory([out] FileDescriptor fd);

    /**
     * @brief 关闭配件描述符。
     *
     * @param fd 配件的文件描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * @since 5.0
     * @version 1.0
     */
    CloseAccessory([in] FileDescriptor fd);
}
/** @} */