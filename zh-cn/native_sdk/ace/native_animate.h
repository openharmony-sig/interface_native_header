/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧动画回调的能力。
 *
 * @since 12
 */

/**
 * @file native_animate.h
 *
 * @brief 提供ArkUI在Native侧的动画接口定义集合。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */
 
#ifndef ARKUI_NATIVE_ANIMATE_H
#define ARKUI_NATIVE_ANIMATE_H

#include <cstdint>

#include "native_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 设置动画的期望帧率。
*
* @since 12
*/
typedef struct {
    /** 期望的最小帧率。*/
    uint32_t min;
    /** 期望的最大帧率。*/
    uint32_t max;
    /** 期望的最优帧率。*/
    uint32_t expected;
} ArkUI_ExpectedFrameRateRange;


/**
* @brief 动画播放完成回调类型。
*
* @since 12
*/
typedef struct {
    /** 在动画中定义onFinish回调的类型。*/
    ArkUI_FinishCallbackType type;
    /** 动画播放完成回调。*/
    void (*callback)(void* userData);
    /** 自定义类型。*/
    void* userData;
} ArkUI_AnimateCompleteCallback;

/**
* @brief 设置动画效果相关参数。
*
* @since 12
*/
typedef struct ArkUI_AnimateOption ArkUI_AnimateOption;

/**
* @brief 提供曲线的插值对象定义。
*
* @since 12
*/
struct ArkUI_Curve;

/**
 * @brief 定义曲线的插值对象指针定义。
 *
 * @since 12
 */
typedef struct ArkUI_Curve* ArkUI_CurveHandle;

/**
 * @brief 定义关键帧动画参数对象。
 *
 * @since 12
 */
typedef struct ArkUI_KeyframeAnimateOption ArkUI_KeyframeAnimateOption;

/**
 * @brief 定义animator动画参数对象。
 *
 * @since 12
 */
typedef struct ArkUI_AnimatorOption ArkUI_AnimatorOption;

/**
 * @brief 定义animator动画对象指针。
 *
 * @since 12
 */
typedef struct ArkUI_Animator* ArkUI_AnimatorHandle;

/**
* @brief 定义animator回调事件对象。
*
* @since 12
*/
struct ArkUI_AnimatorEvent;

/**
* @brief 定义animator接收到帧时回调对象。
*
* @since 12
*/
struct ArkUI_AnimatorOnFrameEvent;

/**
 * @brief 定义transition属性配置转场参数对象。
 *
 * @since 12
 */
typedef struct ArkUI_TransitionEffect ArkUI_TransitionEffect;

/**
 * @brief ArkUI提供的Native侧动画接口集合。
 *
 * @version 1
 * @since 12
 */
typedef struct {
    /**
    * @brief 显式动画接口。
    *
    * @note event闭包中要设置的组件属性，必须在其之前设置过。
    *
    * @param context UIContext实例。
    * @param option 设置动画效果相关参数。
    * @param update 指定动效的闭包函数，在闭包函数中导致的状态变化系统会自动插入过渡动画。
    * @param complete 设置动画播放完成回调参数。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*animateTo)(ArkUI_ContextHandle context, ArkUI_AnimateOption* option, ArkUI_ContextCallback* update,
        ArkUI_AnimateCompleteCallback* complete);

    /**
    * @brief 关键帧动画接口。
    *
    *
    * @param context UIContext实例。
    * @param option 关键帧动画参数。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*keyframeAnimateTo)(ArkUI_ContextHandle context, ArkUI_KeyframeAnimateOption* option);

    /**
    * @brief 创建animator动画对象。
    *
    * @param context UIContext实例。
    * @param option animator动画参数。
    * @return animator动画对象指针。函数参数异常时返回NULL。
    */
    ArkUI_AnimatorHandle (*createAnimator)(ArkUI_ContextHandle context, ArkUI_AnimatorOption* option);

    /**
    * @brief 销毁animator动画对象。
    *
    * @param animator animator动画对象。
    */
    void (*disposeAnimator)(ArkUI_AnimatorHandle animator);
} ArkUI_NativeAnimateAPI_1;

/**
* @brief 创建动画效果参数。
*
* @return 新的动画效果参数指针。
* @since 12
*/
ArkUI_AnimateOption* OH_ArkUI_AnimateOption_Create();

/**
* @brief 销毁动画效果参数指针。
*
* @since 12
*/
void OH_ArkUI_AnimateOption_Dispose(ArkUI_AnimateOption* option);

/**
* @brief 获取动画持续时间，单位为ms(毫秒)。
*
* @param option 动画效果参数。
* @return 持续时间。
* @since 12
*/
int32_t OH_ArkUI_AnimateOption_GetDuration(ArkUI_AnimateOption* option);

/**
* @brief 获取动画播放速度。
*
* @param option 动画效果参数。
* @return 动画播放速度。
* @since 12
*/
float OH_ArkUI_AnimateOption_GetTempo(ArkUI_AnimateOption* option);

/**
* @brief 获取动画曲线。
*
* @param option 动画效果参数。
* @return 动画曲线。
* @since 12
*/
ArkUI_AnimationCurve OH_ArkUI_AnimateOption_GetCurve(ArkUI_AnimateOption* option);

/**
* @brief 获取动画延迟播放时间，单位为ms(毫秒)。
*
* @param option 动画效果参数。
* @return 动画延迟播放时间。
* @since 12
*/
int32_t OH_ArkUI_AnimateOption_GetDelay(ArkUI_AnimateOption* option);

/**
* @brief 获取动画播放次数。
*
* @param option 动画效果参数。
* @return 动画播放次数。
* @since 12
*/
int32_t OH_ArkUI_AnimateOption_GetIterations(ArkUI_AnimateOption* option);

/**
* @brief 获取动画播放模式。
*
* @param option 动画效果参数。
* @return 动画播放模式。
* @since 12
*/
ArkUI_AnimationPlayMode OH_ArkUI_AnimateOption_GetPlayMode(ArkUI_AnimateOption* option);

/**
* @brief 获取动画的期望帧率。
*
* @param option 动画效果参数。
* @return 动画的期望帧率。
* @since 12
*/
ArkUI_ExpectedFrameRateRange* OH_ArkUI_AnimateOption_GetExpectedFrameRateRange(ArkUI_AnimateOption* option);

/**
* @brief 设置动画持续时间。
*
* @param option 动画效果参数。
* @param value 持续时间，单位为ms(毫秒)。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetDuration(ArkUI_AnimateOption* option, int32_t value);

/**
* @brief 设置动画播放速度。
*
* @param option 动画效果参数。
* @param value 动画播放速度。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetTempo(ArkUI_AnimateOption* option, float value);

/**
* @brief 设置动画曲线。
*
* @param option 动画效果参数。
* @param value 动画曲线。默认值：ARKUI_CURVE_LINEAR。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetCurve(ArkUI_AnimateOption* option, ArkUI_AnimationCurve value);

/**
* @brief 设置动画延迟播放时间。
*
* @param option 动画效果参数。
* @param value 动画延迟播放时间。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetDelay(ArkUI_AnimateOption* option, int32_t value);

/**
* @brief 设置动画播放次数。
*
* @param option 动画效果参数。
* @param value 动画播放次数。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetIterations(ArkUI_AnimateOption* option, int32_t value);

/**
* @brief 设置动画播放模式。
*
* @param option 动画效果参数。
* @param value 动画播放模式。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetPlayMode(ArkUI_AnimateOption* option, ArkUI_AnimationPlayMode value);

/**
* @brief 设置动画的期望帧率。
*
* @param option 动画效果参数。
* @param value 动画的期望帧率。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetExpectedFrameRateRange(ArkUI_AnimateOption* option, ArkUI_ExpectedFrameRateRange* value);

/**
* @brief 设置动画的动画曲线。
*
* @note  此方法优于OH_ArkUI_AnimateOption_SetCurve设置的值。
* @param option animator动画参数。
* @param value 动画曲线参数。
* @since 12
*/
void OH_ArkUI_AnimateOption_SetICurve(ArkUI_AnimateOption* option, ArkUI_CurveHandle value);

/**
* @brief 获取动画的动画曲线。
*
* @param option animator动画参数。
* @return 动画的动画曲线。
* @since 12
*/
ArkUI_CurveHandle OH_ArkUI_AnimateOption_GetICurve(ArkUI_AnimateOption* option);

/**
 * @brief 获取关键帧动画参数。
 *
 * @param size 关键帧动画状态数。
 * @return 关键帧动画参数对象。size小于0时返回NULL。
 * @since 12
 */
ArkUI_KeyframeAnimateOption* OH_ArkUI_KeyframeAnimateOption_Create(int32_t size);

/**
 * @brief 销毁关键帧动画参数。
 *
 * @param option 关键帧动画参数对象。
 * @since 12
 */
void OH_ArkUI_KeyframeAnimateOption_Dispose(ArkUI_KeyframeAnimateOption* option);

/**
 * @brief 设置关键帧动画的整体延时时间，单位为ms(毫秒)，默认不延时播放。
 *
 * @param option 关键帧动画参数。
 * @param value 延时时间, 单位为ms(毫秒)。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_KeyframeAnimateOption_SetDelay(ArkUI_KeyframeAnimateOption* option, int32_t value);

/**
 * @brief 设置关键帧动画的动画播放次数。默认播放一次，设置为-1时表示无限次播放。设置为0时表示无动画效果。
 *
 * @param option 关键帧动画参数。
 * @param value 动画播放次数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_KeyframeAnimateOption_SetIterations(ArkUI_KeyframeAnimateOption* option, int32_t value);

/**
 * @brief 设置关键帧动画播放完成回调。当keyframe动画所有次数播放完成后调用。
 *
 * @param option 关键帧动画参数。
 * @param userData 用户自定义对象指针。
 * @param onFinish 回调方法。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_KeyframeAnimateOption_RegisterOnFinishCallback(
    ArkUI_KeyframeAnimateOption* option, void* userData, void (*onFinish)(void* userData));

/**
 * @brief 设置关键帧动画期望帧率。
 *
 * @param option 关键帧动画参数。
 * @param frameRate 关键帧动画的期望帧率。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_KeyframeAnimateOption_SetExpectedFrameRate(
    ArkUI_KeyframeAnimateOption* option, ArkUI_ExpectedFrameRateRange* frameRate);

/**
 * @brief 设置关键帧动画某段关键帧动画的持续时间，单位为毫秒。
 *
 * @param option 关键帧动画参数。
 * @param value 持续时间。单位为毫秒。
 * @param index 状态索引值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_KeyframeAnimateOption_SetDuration(ArkUI_KeyframeAnimateOption* option, int32_t value, int32_t index);

/**
 * @brief 设置关键帧动画某段关键帧使用的动画曲线。
 *
 * @note 由于springMotion、responsiveSpringMotion、interpolatingSpring曲线时长不生效，故不支持这三种曲线。
 * @param option 关键帧动画参数。
 * @param value 该关键帧使用的动画曲线。默认值：EASE_IN_OUT。
 * @param index 状态索引值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_KeyframeAnimateOption_SetCurve(
    ArkUI_KeyframeAnimateOption* option, ArkUI_CurveHandle value, int32_t index);

/**
 * @brief 设置关键帧时刻状态的闭包函数，即在该关键帧时刻要达到的状态。
 *
 * @param option 关键帧动画参数。
 * @param event 闭包函数。
 * @param userData 用户定义对象指针。
 * @param index 状态索引值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */    
int32_t OH_ArkUI_KeyframeAnimateOption_RegisterOnEventCallback(
    ArkUI_KeyframeAnimateOption* option, void* userData, void (*event)(void* userData), int32_t index);

/**
 * @brief 获取关键帧整体延时时间。
 *
 * @param option 关键帧动画参数。
 * @return 整体延时时间。
 * @since 12
 */ 
int32_t OH_ArkUI_KeyframeAnimateOption_GetDelay(ArkUI_KeyframeAnimateOption* option);

/**
 * @brief 获取关键帧动画播放次数。
 *
 * @param option 关键帧动画参数。
 * @return 动画播放次数。
 * @since 12
 */ 
int32_t OH_ArkUI_KeyframeAnimateOption_GetIterations(ArkUI_KeyframeAnimateOption* option);

/**
 * @brief 获取关键帧动画参数的期望帧率。
 *
 * @param option 关键帧动画参数。
 * @return 关键帧动画参数的期望帧率。
 * @since 18
 */
ArkUI_ExpectedFrameRateRange* OH_ArkUI_KeyframeAnimateOption_GetExpectedFrameRate(ArkUI_KeyframeAnimateOption* option);

/**
 * @brief 获取关键帧动画某段状态持续时间。
 *
 * @param option 关键帧动画参数。
 * @param index 状态索引值。
 * @return 持续时间。单位为毫秒。
 * @since 12
 */ 
int32_t OH_ArkUI_KeyframeAnimateOption_GetDuration(ArkUI_KeyframeAnimateOption* option, int32_t index);

/**
 * @brief 获取关键帧动画某段状态动画曲线。
 *
 * @param option 关键帧动画参数。
 * @param index 状态索引值。
 * @return 动画曲线。
 * @since 12
 */ 
ArkUI_CurveHandle OH_ArkUI_KeyframeAnimateOption_GetCurve(ArkUI_KeyframeAnimateOption* option, int32_t index);

/**
 * @brief 创建animator动画对象参数。
 *
 * @note keyframeSize大于0时，动画插值起点默认是0，动画插值终点模式值是1。不支持设置。
 * @param keyframeSize 关键帧个数。
 * @return animator动画对象参数指针。
 * @since 12
 */ 
ArkUI_AnimatorOption* OH_ArkUI_AnimatorOption_Create(int32_t keyframeSize);

/**
 * @brief 销毁animator动画对象参数。
 *
 * @since 12
 */ 
void OH_ArkUI_AnimatorOption_Dispose(ArkUI_AnimatorOption* option);

/**
 * @brief 设置animator动画播放的时长，单位毫秒。
 *
 * @param option animator动画对象参数。
 * @param value 播放的时长，单位毫秒。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetDuration(ArkUI_AnimatorOption* option, int32_t value);

/**
 * @brief 设置animator动画延时播放时长，单位毫秒。
 *
 * @param option animator动画对象参数。
 * @param value 延时播放时长，单位毫秒。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetDelay(ArkUI_AnimatorOption* option, int32_t value);

/**
 * @brief 设置animator动画播放次数。设置为0时不播放，设置为-1时无限次播放。
 *
 * @note 设置为除-1外其他负数视为无效取值，无效取值动画默认播放1次。
 * @param option animator动画对象参数。
 * @param value 动画播放次数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetIterations(ArkUI_AnimatorOption* option, int32_t value);

/**
 * @brief 设置animator动画执行后是否恢复到初始状态。
 *
 * @param option animator动画对象参数。
 * @param value 动画执行后是否恢复到初始状态。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetFill(ArkUI_AnimatorOption* option, ArkUI_AnimationFill value);

/**
 * @brief 设置animator动画播放方向。
 *
 * @param option animator动画对象参数。
 * @param value 动画播放方向。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetDirection(ArkUI_AnimatorOption* option, ArkUI_AnimationDirection value);

/**
 * @brief 设置animator动画插值曲线。
 *
 * @note 不支持springCurve，springMotion，responsiveSpringMotion，interpolatingSpring
 * customCurve动画曲线。
 *
 * @param option animator动画对象参数。
 * @param value 动画插值曲线。默认值：ARKUI_CURVE_LINEAR。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetCurve(ArkUI_AnimatorOption* option, ArkUI_CurveHandle value);

/**
 * @brief 设置animator动画插值起点。
 * @note 当Animator动画为keyframe动画时，此方法不生效。
 *
 * @param option animator动画对象参数。
 * @param value 动画插值起点。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetBegin(ArkUI_AnimatorOption* option, float value);

/**
 * @brief 设置animator动画插值终点。
 * @note 当Animator动画为keyframe动画时，此方法不生效。
 *
 * @param option animator动画对象参数。
 * @param value 动画插值终点。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetEnd(ArkUI_AnimatorOption* option, float value);

/**
 * @brief 设置animator动画期望的帧率范围。
 *
 * @param option animator动画对象参数。
 * @param value 期望的帧率范围对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetExpectedFrameRateRange(
    ArkUI_AnimatorOption* option, ArkUI_ExpectedFrameRateRange* value);

/**
 * @brief 设置animator动画关键帧参数。
 *
 * @param option animator动画对象参数。
 * @param time 关键帧时间。取值范围：[0, 1], 必须是递增。
 * @param value 关键帧数值。
 * @param index 关键帧的索引值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetKeyframe(
    ArkUI_AnimatorOption* option, float time, float value, int32_t index);

/**
 * @brief 设置animator动画关键帧曲线类型。
 *
 * @note 不支持springCurve，springMotion，responsiveSpringMotion，interpolatingSpring
 * customCurve动画曲线
 *
 * @param option animator动画对象参数。
 * @param value 动画插值曲线。
 * @param index 关键帧的索引值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */ 
int32_t OH_ArkUI_AnimatorOption_SetKeyframeCurve(ArkUI_AnimatorOption* option, ArkUI_CurveHandle value, int32_t index);
/**
 * @brief 获取animator动画播放的时长。
 *
 * @param option animator动画参数。
 * @return 动画播放的时长，单位毫秒。
 * @since 12
 */ 
int32_t OH_ArkUI_AnimatorOption_GetDuration(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画延时播放时长。
 *
 * @param option animator动画参数。
 * @return 动画延时播放时长，单位毫秒。
 * @since 12
 */
int32_t OH_ArkUI_AnimatorOption_GetDelay(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画播放次数。
 *
 * @param option animator动画动画参数。
 * @return 动画播放次数。
 * @since 12
 */
int32_t OH_ArkUI_AnimatorOption_GetIterations(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画执行后是否恢复到初始状态。
 *
 * @param option animator动画参数。
 * @return 执行后是否恢复到初始状态。
 * @since 12
 */
ArkUI_AnimationFill OH_ArkUI_AnimatorOption_GetFill(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画播放方向。
 *
 * @param option animator动画参数。
 * @return 动画播放方向。
 * @since 12
 */
ArkUI_AnimationDirection OH_ArkUI_AnimatorOption_GetDirection(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画插值曲线。
 *
 * @param option animator动画参数。
 * @return 动画插值曲线。
 * @since 12
 */
ArkUI_CurveHandle OH_ArkUI_AnimatorOption_GetCurve(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画插值起点。
 *
 * @param option animator动画参数。
 * @return 动画插值起点。
 * @since 12
 */
float OH_ArkUI_AnimatorOption_GetBegin(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画插值终点。
 *
 * @param option animator动画参数。
 * @return 动画插值终点。
 * @since 12
 */
float OH_ArkUI_AnimatorOption_GetEnd(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画期望的帧率范围。
 *
 * @param option animator动画参数。
 * @return 期望的帧率范围对象指针。
 * @since 12
 */
ArkUI_ExpectedFrameRateRange* OH_ArkUI_AnimatorOption_GetExpectedFrameRateRange(ArkUI_AnimatorOption* option);

/**
 * @brief 获取animator动画关键帧时间。
 *
 * @param option animator动画对象参数。
 * @param index 关键帧的索引值。
 * @return 关键帧时间。
 * @since 12
 */ 
float OH_ArkUI_AnimatorOption_GetKeyframeTime(ArkUI_AnimatorOption* option, int32_t index);

/**
 * @brief 获取animator动画关键帧数值。
 *
 * @param option animator动画对象参数。
 * @param index 关键帧的索引值。
 * @return 关键帧数值。
 * @since 12
 */ 
float OH_ArkUI_AnimatorOption_GetKeyframeValue(ArkUI_AnimatorOption* option, int32_t index);

/**
 * @brief 获取animator动画关键帧动画插值曲线。
 *
 * @param option animator动画对象参数。
 * @param index 关键帧的索引值。
 * @return 动画插值曲线。
 * @since 12
 */ 
ArkUI_CurveHandle OH_ArkUI_AnimatorOption_GetKeyframeCurve(ArkUI_AnimatorOption* option, int32_t index);

/**
 * @brief 获取动画事件对象中的用户自定义对象。
 *
 * @param event 动画事件对象。
 * @return 用户自定义对象。
 * @since 12
 */ 
void* OH_ArkUI_AnimatorEvent_GetUserData(ArkUI_AnimatorEvent* event);

/**
 * @brief 获取动画事件对象中的用户自定义对象。
 *
 * @param event 动画事件对象。
 * @return 用户自定义对象。
 * @since 12
 */ 
void* OH_ArkUI_AnimatorOnFrameEvent_GetUserData(ArkUI_AnimatorOnFrameEvent* event);

/**
 * @brief 获取动画事件对象中的当前进度。
 *
 * @param event 动画事件对象。
 * @return 动画进度。
 * @since 12
 */ 
float OH_ArkUI_AnimatorOnFrameEvent_GetValue(ArkUI_AnimatorOnFrameEvent* event);

/**
 * @brief 设置animator动画接收到帧时回调。
 *
 * @param option animator动画参数。
 * @param userData 用户自定义参数。
 * @param callback 回调函数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_AnimatorOption_RegisterOnFrameCallback(
    ArkUI_AnimatorOption* option, void* userData, void (*callback)(ArkUI_AnimatorOnFrameEvent* event));

/**
 * @brief 设置animator动画完成时回调。
 *
 * @param option animator动画参数。
 * @param userData 用户自定义参数。
 * @param callback 回调函数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_AnimatorOption_RegisterOnFinishCallback(
    ArkUI_AnimatorOption* option, void* userData, void (*callback)(ArkUI_AnimatorEvent* event));

/**
 * @brief 设置animator动画被取消时回调。
 *
 * @param option animator动画参数。
 * @param userData 用户自定义参数。
 * @param callback 回调函数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_AnimatorOption_RegisterOnCancelCallback(
    ArkUI_AnimatorOption* option, void* userData, void (*callback)(ArkUI_AnimatorEvent* event));

/**
 * @brief 设置animator动画重复时回调。
 *
 * @param option animator动画参数。
 * @param userData 用户自定义参数。
 * @param callback 回调函数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_AnimatorOption_RegisterOnRepeatCallback(
    ArkUI_AnimatorOption* option, void* userData, void (*callback)(ArkUI_AnimatorEvent* event));

/**
 * @brief 更新animator动画。
 *
 * @param animator animator动画对象。
 * @param option animator动画参数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_ResetAnimatorOption(
    ArkUI_AnimatorHandle animator, ArkUI_AnimatorOption* option);

/**
 * @brief 启动animator动画。
 *
 * @param animator animator动画对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_Play(ArkUI_AnimatorHandle animator);

/**
 * @brief 结束animator动画。
 *
 * @param animator animator动画对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_Finish(ArkUI_AnimatorHandle animator);

/**
 * @brief 暂停animator动画。
 *
 * @param animator animator动画对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_Pause(ArkUI_AnimatorHandle animator);

/**
 * @brief 取消animator动画。
 *
 * @param animator animator动画对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_Cancel(ArkUI_AnimatorHandle animator);

/**
 * @brief 以相反的顺序播放animator动画。
 *
 * @param animator animator动画对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 */
int32_t OH_ArkUI_Animator_Reverse(ArkUI_AnimatorHandle animator);

/**
 * @brief 插值曲线的初始化函数，可以根据入参创建一个插值曲线对象。
 *
 * @param curve 曲线类型。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateCurveByType(ArkUI_AnimationCurve curve);

/**
 * @brief 构造阶梯曲线对象。
 *
 * @param count 阶梯的数量，需要为正整数，取值范围：[1, +∞)。
 * @param end 在每个间隔的起点或是终点发生阶跃变化。
 * true：在终点发生阶跃变化。false：在起点发生阶跃变化。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateStepsCurve(int32_t count, bool end);

/**
 * @brief 构造三阶贝塞尔曲线对象。
 *
 *
 * @param x1 确定贝塞尔曲线第一点横坐标，取值范围：[0, 1]。
 * 设置的值小于0时，按0处理；设置的值大于1时，按1处理。
 * @param y1 确定贝塞尔曲线第一点纵坐标。
 * @param x2 确定贝塞尔曲线第二点横坐标，取值范围：[0, 1]。
 * 设置的值小于0时，按0处理；设置的值大于1时，按1处理。
 * @param y2 确定贝塞尔曲线第二点纵坐标。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateCubicBezierCurve(float x1, float y1, float x2, float y2);

/**
 * @brief 构造弹簧曲线对象，曲线形状由弹簧参数决定，动画时长受animation、animateTo中的duration参数控制。
 *
 * @param velocity 初始速度。是由外部因素对弹性动效产生的影响参数，
 * 其目的是保证对象从之前的运动状态平滑的过渡到弹性动效。该速度是归一化速度，其值等于动画开始时的实际速度除以动画属性改变值。
 * @param mass 质量。弹性系统的受力对象，会对弹性系统产生惯性影响。质量越大，震荡的幅度越大，恢复到平衡位置的速度越慢。
 * @param stiffness 刚度。是物体抵抗施加的力而形变的程度。在弹性系统中，刚度越大，抵抗变形的能力越强，恢复到平衡位置的速度就越快。
 * @param damping 阻尼。用于描述系统在受到扰动后震荡及衰减的情形。阻尼越大，弹性运动的震荡次数越少、震荡幅度越小。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateSpringCurve(float velocity, float mass, float stiffness, float damping);

/**
 * @brief 构造弹性动画曲线对象。如果对同一对象的同一属性进行多个弹性动画，每个动画会替换掉前一个动画，并继承之前的速度。
 * @note 动画时间由曲线参数决定，不受animation、animateTo中的duration参数控制。
 *
 * @param response 弹簧自然振动周期，决定弹簧复位的速度。
 * @param dampingFraction 阻尼系数。
 * 大于0小于1的值为欠阻尼，运动过程中会超出目标值；
 * 等于1为临界阻尼；
 * 大于1为过阻尼，运动过程中逐渐趋于目标值。
 * @param overlapDuration 弹性动画衔接时长。发生动画继承时，如果前后两个弹性动画response不一致，
 * response参数会在overlapDuration时间内平滑过渡。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateSpringMotion(float response, float dampingFraction, float overlapDuration);

/**
 * @brief 构造弹性跟手动画曲线对象，是springMotion的一种特例，仅默认参数不同，可与springMotion混合使用。
 * @note 动画时间由曲线参数决定，不受animation、animateTo中的duration参数控制。
 *
 * @param response 弹簧自然振动周期，决定弹簧复位的速度。
 * @param dampingFraction 阻尼系数。
 * 大于0小于1的值为欠阻尼，运动过程中会超出目标值；
 * 等于1为临界阻尼；
 * 大于1为过阻尼，运动过程中逐渐趋于目标值。
 * @param overlapDuration 弹性动画衔接时长。发生动画继承时，如果前后两个弹性动画response不一致，
 * response参数会在overlapDuration时间内平滑过渡。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateResponsiveSpringMotion(
    float response, float dampingFraction, float overlapDuration);

/**
 * @brief 构造插值器弹簧曲线对象，生成一条从0到1的动画曲线，实际动画值根据曲线进行插值计算。
 * @note 动画时间由曲线参数决定，不受animation、animateTo中的duration参数控制。
 *
 *
 * @param velocity 初始速度。外部因素对弹性动效产生的影响参数，
 * 目的是保证对象从之前的运动状态平滑地过渡到弹性动效。该速度是归一化速度，
 * 其值等于动画开始时的实际速度除以动画属性改变值。
 * @param mass 质量。弹性系统的受力对象，会对弹性系统产生惯性影响。
 * 质量越大，震荡的幅度越大，恢复到平衡位置的速度越慢。
 * @param stiffness 刚度。表示物体抵抗施加的力而形变的程度。
 * 刚度越大，抵抗变形的能力越强，恢复到平衡位置的速度越快。
 * @param damping 阻尼。用于描述系统在受到扰动后震荡及衰减的情形。
 * 阻尼越大，弹性运动的震荡次数越少、震荡幅度越小。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateInterpolatingSpring(float velocity, float mass, float stiffness, float damping);

/**
 * @brief 构造自定义曲线对象。
 *
 * @param userData 用户自定义数据。
 * @param interpolate 用户自定义的插值回调函数。fraction为动画开始时的插值输入x值。取值范围：[0,1]
 * 返回值为曲线的y值。取值范围：[0,1]。
 * fraction等于0时，返回值为0对应动画起点，返回不为0，动画在起点处有跳变效果。
 * fraction等于1时，返回值为1对应动画终点，返回值不为1将导致动画的终值不是状态变量的值，
 * 出现大于或者小于状态变量值，再跳变到状态变量值的效果。
 * @return 曲线的插值对象指针。如果参数异常返回NULL。
 */
ArkUI_CurveHandle OH_ArkUI_Curve_CreateCustomCurve(
    void* userData, float (*interpolate)(float fraction, void* userdata));

/**
 * @brief 销毁自定义曲线对象。
 *
 * @param curve 曲线的插值对象指针。
 */
void OH_ArkUI_Curve_DisposeCurve(ArkUI_CurveHandle curveHandle);


/**
 * @brief 创建组件转场时的透明度效果对象。
 *
 * @note 设置小于0的非法值按0处理，大于1的非法值按1处理。
 * @param opacity 透明度，取值范围： [0, 1]。
 * @return 组件转场时的透明度效果对象。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateOpacityTransitionEffect(float opacity);

/**
 * @brief 创建组件转场时的平移效果对象。
 *
 * @param translate 组件转场时的平移参数对象。
 * @return 组件转场时的平移效果对象。如果参数异常返回NULL。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateTranslationTransitionEffect(ArkUI_TranslationOptions* translate);

/**
 * @brief 创建组件转场时的缩放效果对象。
 *
 * @param scale 组件转场时的缩放参数对象。
 * @return 组件转场时的缩放效果对象。如果参数异常返回NULL。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateScaleTransitionEffect(ArkUI_ScaleOptions* scale);

/**
 * @brief 创建组件转场时的旋转效果对象。
 *
 * @param rotate 组件转场时的旋转参数对象。
 * @return 组件转场时的旋转效果对象。如果参数异常返回NULL。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateRotationTransitionEffect(ArkUI_RotationOptions* rotate);

/**
 * @brief 创建组件平移效果对象。
 *
 * @param move 平移类型。
 * @return 组件转场时的平移效果对象。如果参数异常返回NULL。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateMovementTransitionEffect(ArkUI_TransitionEdge move);

/**
 * @brief 创建非对称的转场效果对象。
 *
 * @note 如不通过asymmetric函数构造TransitionEffect，则表明该效果在组件出现和消失时均生效。
 * @param appear 出现的转场效果。
 * @param disappear 消失的转场效果。
 * @return 非对称的转场效果对象。如果参数异常返回NULL。
 * @since 12
 */
ArkUI_TransitionEffect* OH_ArkUI_CreateAsymmetricTransitionEffect(
    ArkUI_TransitionEffect* appear, ArkUI_TransitionEffect* disappear);

/**
 * @brief 销毁转场效果对象。
 *
 * @param option 转场效果对象。
 * @since 12
 */
void OH_ArkUI_TransitionEffect_Dispose(ArkUI_TransitionEffect* option);

/**
 * @brief 设置转场效果链式组合，以形成包含多种转场效果的TransitionEffect。
 *
 * @param option 转场效果。
 * @param combine 需要链式转场效果。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_TransitionEffect_Combine(
    ArkUI_TransitionEffect* option, ArkUI_TransitionEffect* combine);

/**
 * @brief 设置转场效果动画参数。
 *
 * @note 如果通过combine进行转场效果的组合，前一转场效果的动画参数也可用于后一转场效果。
 * @param option 转场效果。
 * @param animation 属性显示动画效果相关参数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_TransitionEffect_SetAnimation(
    ArkUI_TransitionEffect* option, ArkUI_AnimateOption* animation);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_ANIMATE_H
/** @} */